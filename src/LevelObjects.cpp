//
//  LevelObjects.h
//  MinigolfRendTest
//
//  Created by Chase Bradbury on 4/21/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#include "include/LevelObjects.h"

using namespace std;

/*
Vector3f::Vector3f(float X, float Y, float Z){
	x = X;
	y = Y;
	z = Z;
} */

// Print the Vector3f (Formatted)
void printVector3f(Vector3f& v){
	cout << "(" << v.x << "," << v.y << "," << v.z << ")";
}

// Print the tile information (Formatted: ID, Edges, Vertices, Neighbors)
void printTile(Tile* t){
	cout << "Tile: [ ID:" << t->ID << " , E:" << t->numEdges << " , V:";
	for(int i = 0; i < t->numEdges; i++){
		cout << t->vertices[i] << " ";
	}
	cout << ", N:";
	for(int i = 0; i < t->numEdges; i++){
		cout << t->neighbors[i] << " ";
	}
	cout << "]";
}

// Print the cup information (Formatted)
void printCup(Cup* c){
	cout << "Cup: [ ID: " << c->tileID << ", Position: ";
	cout << c->position << " ]";
}
	
// Print the tee information (Formatted)
void printTee(Tee* t){
	cout << "Tee: [ ID: " << t->tileID << ", Position: ";
	cout << t->position << " ]";
}
