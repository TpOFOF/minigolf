//
//  Vector3f.cpp
//  CMPS160-Lab3
//
//  Created by Chase Bradbury on 11/11/13.
//  Copyright (c) 2013 Chase Bradbury. All rights reserved.
//

#include "include/Vector3f.h"

/*** Constructors ***/

Vector3f::Vector3f() {
    this->x = 0;
    this->y = 0;
    this->z = 0;
}

Vector3f::Vector3f(float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;
}



/*** Class functions ***/

void Vector3f::normalize() {
	float length = sqrt(x * x + y * y + z * z);
	if(length == 0){
		x = 0;
		y = 0;
		z = 0;
		return;
	}	
		
	float tempx = x / length;
    float tempy = y / length;
    float tempz = z / length;
    x = tempx;
    y = tempy;
    z = tempz;
}

void Vector3f::reflect(Vector3f normal) {
    *this = *this - normal * 2 * dot(*this, normal);
}

bool Vector3f::isNormal() {
    return (this->length() == 1);
}

float Vector3f::length() {
	float sum = x * x + y * y + z * z;
	if(sum == 0) return(0);
	float length = sqrt(sum);
    return(length); 
}

bool Vector3f::isZero(){
	return(x == 0 && y == 0 && z == 0);
}



/*** Operator overloads ***/

// Vector addition
Vector3f Vector3f::operator +(Vector3f other) {
    Vector3f newVector;
    newVector.x = x + other.x;
    newVector.y = y + other.y;
    newVector.z = z + other.z;
    return newVector;
}
// Vector subtraction
Vector3f Vector3f::operator -(Vector3f other) {
    Vector3f newVector;
    newVector.x = x - other.x;
    newVector.y = y - other.y;
    newVector.z = z - other.z;
    return newVector;
}
// Scalar float multiplication
Vector3f Vector3f::operator *(float scale) {
    Vector3f newVector;
    newVector.x = x * scale;
    newVector.y = y * scale;
    newVector.z = z * scale;
    return newVector;
}
// Scalar float division
Vector3f Vector3f::operator /(float scale) {
    Vector3f newVector;
    newVector.x = x / scale;
    newVector.y = y / scale;
    newVector.z = z / scale;
    return newVector;
}
// Cross multiplication
Vector3f Vector3f::operator *(Vector3f other) {
    Vector3f newVector;
    newVector.x = y * other.z - z * other.y;
    newVector.y = z * other.x - x * other.z;
    newVector.z = x * other.y - y * other.x;
    return newVector;
}
// Integer incrementation
Vector3f Vector3f::operator ++() {
    Vector3f newVector;
    newVector.x = ++x;
    newVector.y = ++y;
    newVector.z = ++z;
    return newVector;
}
// Integer decrementation
Vector3f Vector3f::operator --() {
    Vector3f newVector;
    newVector.x = --x;
    newVector.y = --y;
    newVector.z = --z;
    return newVector;
}
// Vector addition
Vector3f Vector3f::operator +=(Vector3f other) {
    x = x + other.x;
    y = y + other.y;
    z = z + other.z;
    return *this;
}
// Vector subtraction
Vector3f Vector3f::operator -=(Vector3f other) {
    x = x - other.x;
    y = y - other.y;
    z = z - other.z;
    return *this;
}
// Scalar float multiplication
Vector3f Vector3f::operator *=(float scale) {
    x = x * scale;
    y = y * scale;
    z = z * scale;
    return *this;
}
// Scalar float division
Vector3f Vector3f::operator /=(float scale) {
    x = x / scale;
    y = y / scale;
    z = z / scale;
    return *this;
}
// Cross multiplication
Vector3f Vector3f::operator *=(Vector3f other) {
    x = y * other.z - z * other.y;
    y = z * other.x - x * other.z;
    z = x * other.y - y * other.x;
    return *this;
}


/*** Comparison operators ***/
bool Vector3f::operator==(Vector3f v){
	bool equal = true;
	if(x != v.x) equal = false;
	if(y != v.y) equal = false;
	if(z != v.z) equal = false;
	return(equal);
}

bool Vector3f::operator!=(Vector3f v){
	if(x != v.x) return(true);
	if(y != v.y) return(true);
	if(z != v.z) return(true);
	return(false);
}

bool Vector3f::operator<(Vector3f v){
	return(length() < v.length());
}

bool Vector3f::operator>(Vector3f v){
	return(length() > v.length());
}

bool Vector3f::operator<=(Vector3f v){
	return(length() <= v.length());
}

bool Vector3f::operator>=(Vector3f v){
	return(length() >= v.length());
}








/*** Utility functions ***/

ostream& operator <<(ostream& stream, const Vector3f& vector) {
    stream << "(" << vector.x << "," << vector.y << "," << vector.z << ")";
    return stream;
}

Vector3f normalize(Vector3f vector) {
	float mag = vector.length();
	if(mag == 0) return(Vector3f(0,0,0));
    Vector3f newVector = vector / mag;
	///*
    newVector.x = vector.x / sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
    newVector.y = vector.y / sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
    newVector.z = vector.z / sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
    //*/
	return newVector;
}

Vector3f reflect(Vector3f input, Vector3f normal) {
    return(input - normal * 2 * dot(input, normal));
}

// Return the vector projection of vector b onto vector a
Vector3f project(Vector3f b, Vector3f a){
	return(normalize(a) * (dot(a, b) / a.length()) ); 
}

float dot(Vector3f v1, Vector3f v2) {
	return(v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}

