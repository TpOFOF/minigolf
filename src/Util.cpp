//
//  Util.cpp
//  Minigolf3
//
//  Created by Chase Bradbury on 4/29/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#include "include/Util.h"

// 
void draw_axis() {
    glLineWidth(2.5);
    
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_LINES);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(15, 0, 0);
    glEnd();
    
    glColor3f(0.0, 1.0, 0.0);
    glBegin(GL_LINES);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0, 15, 0);
    glEnd();
    
    glColor3f(0.0, 0.0, 1.0);
    glBegin(GL_LINES);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0, 0, 15);
    glEnd();
}


// Return the position a ray intersects with a plane
Vector3f rayPlaneCollisionPoint(Vector3f rs, Vector3f rv, Vector3f ps, Vector3f pn){
	// rs - ray start point, rv - ray direction, ps - point on plane, pn - normal
	Vector3f point = Vector3f(0,0,0);
	// point = d * rv + rs
	float d = rayPlaneCollisionDist(rs, rv, ps, pn);
	
	// If the ray is parallel to the plane, return a very far away point
	if(d == -1000000000) 
		return(Vector3f(1000000000, 1000000000, 1000000000));
	
	rv.normalize();
	point = rs + (rv * d);
	return(point);
}


// Return the distance from the start of a ray to a plane
float rayPlaneCollisionDist(Vector3f rs, Vector3f rv, Vector3f ps, Vector3f pn){
	// rs - ray start point, rv - ray direction, ps - point on plane, pn - normal
	float d = -1000000000; // Collision distance starts as infinite
	
	// If the ray is parallel to the plane, return "infinity"
	if( dot(rv, pn) == 0) return(d);
	
	// To ensure d represents distance, normalize both direction vectors
	rv.normalize();
	pn.normalize();
	
	// d =  ((ps - rs) dot pn ) / ( rv dot pn )
	d = dot((ps - rs), pn) / dot(rv, pn);
	return(d);
}


// Return the distance from a point to a plane
float pointPlaneDist(Vector3f pt, Vector3f ps, Vector3f pn){
	// pt - point, ps - point on plane, pn - normal
	// Distance = magnitude( n dot w ) / magnitude(n)   
	//     where w is any vector from plane to point
	return( dot(pn, (ps - pt)) / pn.length() );
}


// Return the y coordinate of a point on a plane defined by p and n, at position (x,y,z)
float planeYPos(Vector3f p, Vector3f n, float x, float z){
	// y = -( d + ax + cz ) / b
	float d = -( (n.x)*(p.x) + (n.y)*(p.y) + (n.z)*(p.z) );
	return( -(d + (n.x)*(x) + (n.z)*(z)) / (n.y));
}

void drawText(float x, float y, void *font, int fontSize, const char* string, Vector3f color, SPS_ALIGNMENT alignment)
{
    glPushMatrix();
    //glScalef(.2, .2, 1);
    
    switch(alignment) {
        case SPS_ALIGNMENT_TOPLEFT:
            glTranslatef(0, .1, 0);
            break;
    }
    glColor3f(color.x, color.y, color.z);
    glRasterPos2f((x * 2) - 1, 1 - (y * 2));
    
    for (const char* c = string; *c != '\0'; ++c) {
        glutBitmapCharacter(font, *c);
    }
    
    glPopMatrix();
}

char* concat(char* string1, char* string2) {
    char* destination = (char*)malloc( strlen( string1 ) + strlen( string2 ) + 1 );
    strcpy( destination, string1 );
    strcat( destination, string2 );
    return destination;
}

int spsCharToInt(char input) {
    if (input < 65) return input - 48;
    else return input - 55;
}

char spsIntToChar(int input) {
    if (input < 10) return input + 48;
    else return input + 55;
}