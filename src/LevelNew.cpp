//
//  Level.cpp
//  MinigolfRendTest
//
//  Created by Chase Bradbury on 4/18/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#include "include/LevelNew.h"
//#include "include/GL/glut.h"


// Display list descriptor for this level
int DL = -1;
bool isList = false;


Level::Level(LevelData* LD) {
	data = LD;
	
	ball = new SphereCollider(data, data->getTee()->position, data->getTile(data->getTee()->tileID));
    ball->setRadius(0.03);
    ball->setMass(10);
	
	Vector3f levelCenter = data->getCenter();
	overheadCamPos.x = levelCenter.x;
    overheadCamPos.y = levelCenter.y + data->getSize()/1.5;
    overheadCamPos.z = levelCenter.z;
    
}


Tile* Level::getTile(int ID){
	return(data->getTile(ID));
}


Vector3f Level::getBallPosition()
{
    return (ball->getPosition());
}

void Level::setBallPosition(Vector3f pos){
	ball->setPosition(pos);
}

Vector3f Level::getBallVelocity(){
	return(ball->getVelocity());
}

void Level::setBallVelocity(Vector3f vel){
	ball->setVelocity(vel);
}

bool Level::hasWon()
{
    return (ball->isInCup());
}

void Level::restartBall()
{
    ball->restart();
    isList = false;
}

void Level::addForceToBall(Vector3f force)
{
    ball->addForce(force);
}


void Level::updateBall(){
	ball->update();
}


// Draw the tiles, walls, and objects in the level
void Level::drawLevel()
{
	// Handle the display list if the level has been drawn
	if(!isList){
		if(!debugTile){
			DL = glGenLists(1);
			glNewList(DL, GL_COMPILE);
		}
	
		//glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		GLfloat tile[] = {0.0f, 0.6f, 0.1f};
		glMaterialfv(GL_FRONT, GL_DIFFUSE, tile);
		for (int i = 1; i < getNumTiles() + 1; ++i) {
			if( !debugTile || ball->getTile() != i )
				glColor3f(0.0, 0.6, 0.1);
			else 
				glColor3f(0.8, 0.6, 0.3);
			drawTile(i);
		}
		
		//glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		GLfloat wall[] = {0.5f, 0.3f, 0.0f};
		glMaterialfv(GL_FRONT, GL_DIFFUSE, wall);
		for (int i = 1; i < getNumTiles() + 1; ++i) {
			glColor3f(0.5, 0.3, 0.0);
			drawTileBorders(i);
		}
		
		data->drawCup();
		data->drawTee();
	
		if(!debugTile){
			glEndList();
			if(DL > 0)
				isList = true;
			else {
				cout << "WARNING: Level: drawLevel(): Display list is invalid" << endl;
				exit(1);
			}
		}
	}
	
	if(isList && !debugTile)
		glCallList(DL);
	
	// Draw dynamic elements in the level
	drawBall();
}

void Level::drawBall()
{
	ball->draw();
}


void Level::drawTile(int ID)
{
    Tile tile = *data->getTile(ID);
    
    glBegin(GL_TRIANGLE_FAN);
        
    for (int i = 0; i < tile.numEdges; ++i) {
        //glTexCoord2f((float)i / width, (float)j / depth);
        glNormal3f(tile.normal.x, tile.normal.y, tile.normal.z);
        glVertex3f(tile.vertices[i].x, tile.vertices[i].y, tile.vertices[i].z);
    }
    glEnd();
}



void Level::drawTileBorders(int ID)
{
    Tile tile = *data->getTile(ID);
    
    for (int i = 0; i < tile.numEdges; ++i) {
        if (!tile.neighbors[i]) {
            glBegin(GL_TRIANGLE_FAN);
            glNormal3f(tile.edgeNormals[i].x, tile.edgeNormals[i].y, tile.edgeNormals[i].z);
            glVertex3f(tile.vertices[i].x, tile.vertices[i].y, tile.vertices[i].z);
            glVertex3f(tile.vertices[i].x, tile.vertices[i].y + 0.1, tile.vertices[i].z);
            if (i < tile.numEdges - 1)
            {
                glVertex3f(tile.vertices[i + 1].x, tile.vertices[i + 1].y + 0.1, tile.vertices[i + 1].z);
                glVertex3f(tile.vertices[i + 1].x, tile.vertices[i + 1].y, tile.vertices[i + 1].z);
            } else
            {
                glVertex3f(tile.vertices[0].x, tile.vertices[0].y + 0.1, tile.vertices[0].z);
                glVertex3f(tile.vertices[0].x, tile.vertices[0].y, tile.vertices[0].z);
            }
            
            glEnd();
        }
    }
}


void Level::drawLight(float lx, float ly, float lz){
	// Draw a cube to represent the position of the light (which is itself unlit)

	glDisable(GL_LIGHTING);

	glColor3f(1,1,1);
	glBegin(GL_QUADS);

	glNormal3f(0,0,lz+1);
	glVertex3f(lx-.05,-.05+ly,-.05+lz);
	glVertex3f(lx-.05,.05+ly,-.05+lz);
	glVertex3f(lx+.05,.05+ly,-.05+lz);
	glVertex3f(lx+.05,-.05+ly,-.05+lz);

	glNormal3f(0,0,lz-1);
	glVertex3f(lx-.05,-.05+ly,.05+lz);
	glVertex3f(lx-.05,.05+ly,.05+lz);
	glVertex3f(lx+.05,.05+ly,.05+lz);
	glVertex3f(lx+.05,-.05+ly,.05+lz);

	glNormal3f(1,0,0);
	glVertex3f(lx-.05,-.05+ly,-.05+lz);
	glVertex3f(lx-.05,.05+ly,-.05+lz);
	glVertex3f(lx-.05,.05+ly,.05+lz);
	glVertex3f(lx-.05,-.05+ly,.05+lz);

	glNormal3f(-1,0,0);
	glVertex3f(lx+.05,-.05+ly,-.05+lz);
	glVertex3f(lx+.05,.05+ly,-.05+lz);
	glVertex3f(lx+.05,.05+ly,.05+lz);
	glVertex3f(lx+.05,-.05+ly,.05+lz);

	glNormal3f(0,1,0);
	glVertex3f(lx-.05,-.05+ly,-.05+lz);
	glVertex3f(lx+.05,-.05+ly,-.05+lz);
	glVertex3f(lx+.05,-.05+ly,.05+lz);
	glVertex3f(lx-.05,-.05+ly,.05+lz);

	glNormal3f(0,-1,0);
	glVertex3f(lx-.05,.05+ly,-.05+lz);
	glVertex3f(lx+.05,.05+ly,-.05+lz);
	glVertex3f(lx+.05,.05+ly,.05+lz);
	glVertex3f(lx-.05,.05+ly,.05+lz);

	glEnd();
};



void Level::drawBox(float l, float w, float h, float wStr, float hStr, float lSk, float wSk, float hSk, int ends){
	// Arguments in order:
	// length, width, height, width Stretch, height Stretch, length Skew, width Skew, height Skew, end faces

	// This function draws a box with between 4 and 6 quads, allowing for skewing and stretching of the faces

//	if(debug)
//		glColor3f(0,0,.9);
	
	glBegin(GL_QUADS);
	glNormal3f(0,0,1);
	glVertex3f(0,h/2,w/2);
	glVertex3f(0,-h/2,w/2);
	glVertex3f(l+lSk, -(h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
	glVertex3f(l, (h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
	
//	if(debug)
//		glColor3f(0,.9,0);

	glNormal3f(0,1,0);
	glVertex3f(l, (h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
	glVertex3f(l, (h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
	glVertex3f(0,h/2,-w/2);
	glVertex3f(0,h/2,w/2);
	
//	if(debug)
//		glColor3f(0,0,.3);

	glNormal3f(0,-1,0);
	glVertex3f(0,h/2,-w/2);
	glVertex3f(l, (h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
	glVertex3f(l+lSk, -(h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
	glVertex3f(0,-h/2,-w/2);
	
//	if(debug)
//		glColor3f(0,.3,0);

	glNormal3f(0,0,-1);
	glVertex3f(l+lSk, -(h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
	glVertex3f(l+lSk, -(h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
	glVertex3f(0,-h/2,w/2);
	glVertex3f(0,-h/2,-w/2);

	glEnd();

	// If selected, draw the nearest end face
	if(ends == 1 || ends == 3){
//		if(debug)
//			glColor3f(.3,0,0);

		glBegin(GL_QUADS);
		glNormal3f(-1,0,0);
		glVertex3f(0,-h/2,w/2);
		glVertex3f(0,h/2,w/2);
		glVertex3f(0,h/2,-w/2);
		glVertex3f(0,-h/2,-w/2);
		glEnd();
	}

	// If selected, draw the farthest end face
	if(ends == 2 || ends == 3){
//		if(debug)
//			glColor3f(.9,0,0);

		glBegin(GL_QUADS);
		glNormal3f(1,0,0);
		glVertex3f(l+lSk,-(h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
		glVertex3f(l+lSk,-(h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
		glVertex3f(l,(h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
		glVertex3f(l,(h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
		glEnd();
	}

}



