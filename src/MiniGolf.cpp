//
// MiniGolf.cpp
//   - The MiniGolf game itself (uses Game engine)
//

//#include "include/glut.fwd.h"
#include <cstdlib>
#include <string>
#include <cstring>

#include "include/game.h"
#include "include/Vector3f.h"
#include "include/Util.h"

using namespace std;

#define PI 3.14159265

// Declare variables at a global scope for callback access
Game* game;
int counter = 60;
bool startButtonClicked = false;

int camUnlock = 0;
Vector3f prevCamRot = Vector3f(45,0,0);
float prevCamZoom = 2;

float clubCharge = 0;
bool clubCharging = false;
int strokes = 0;
int score = 0;
bool alreadyGambled = false;

bool ballMoving = false;
int arrowID = -1;

char courseName[256];
char levelName[256];
int par = 0;



//// Other functions (not callback) ////

// When camera movement buttons are held down, exponentially
//     increase the delay before the camera re-locks based on 
//     how long buttons were held down
void unlockCamera(){
	if(camUnlock == 0)
		camUnlock = 4;
	camUnlock *= 2;
	if(camUnlock > 60)
		camUnlock = 60; // Maximum frames before camera re-snaps
}


// Change the course and level names to their current names
void changeNames(){
	for(int i = 0; i < 256; i++){
		courseName[i] = '\0';
		levelName[i] = '\0';
	}
	
	strcpy(courseName, game->courseName.c_str());
	strcpy(levelName, game->currentLevelName.c_str());
	
	par = game->currentLevelPar;
}


// Go to the next level, and change the corresponding local name variables
void goToNextLevel(){
	// If the level has been skipped, add the level's par + 4 strokes to the score
	if(!(game->hasWonLevel()))
		score += (par + 4);

	strokes = 0;
	game->nextLevel();
	changeNames();
	alreadyGambled = false;
};




//// Action functions ////



void doNothing(){
	// Function to do nothing
};


void winCheck(){
	if(game->hasWonLevel()){
		//cout << "You finished level " << game->getLevel() << "!" << endl;
		score += (strokes - par);
		strokes = 0;
		goToNextLevel();
	}
};


// Move the camera back to the ball's position
void snapCamera(){
	//float speed = game->getBallVelocity().length();
	Vector3f vel = game->getBallVelocity();
	float speed = sqrt(pow(vel.x, 2) + pow(vel.z, 2));
	if(speed < .0003 && vel.y < .0035){
		ballMoving = false;
		if(!(game->getCameraOverhead()))
			game->showElement(arrowID);
		else
			game->hideElement(arrowID);
		game->setBallVelocity(Vector3f(0,0,0));
	} else {
		ballMoving = true;
		game->hideElement(arrowID);
	}
		
	if(camUnlock == 0){
	
		game->trackCameraToPosition(game->getBallPosition(), (1.0f/7.0f));
	
		// If the mouse is clicked and the camera is not being rotated, set the previous angle
		if(game->isClicked(SPS_MOUSE_LEFT)){
			if(!(game->getCameraOverhead()))
				prevCamRot = game->getCameraRotation();
		} else {
			// Otherwise, track the camera's rotation
			game->trackCameraToRotation(prevCamRot, (1.0f/4.0f));
		}
		
		if(!(game->getCameraOverhead()) && !(game->isPressed('x') || game->isPressed('z')))
			game->trackCameraToZoom(prevCamZoom, (1.0f/7.0f));
	
	} else camUnlock --;
	if(camUnlock < 0) camUnlock = 0;
};


// Apply the club's charge level to affect the acceleration of the ball
void swingClub(){
	float power = clubCharge;
	Vector3f camR = game->getCameraRotation();
	Vector3f swingDir = Vector3f(cos(PI*camR.y/180), 0, sin(PI*camR.y/180));
	//cout << "camR.y: " << (int)(camR.y)%360 << "    Power: " << power << endl;
	swingDir *= power;
	clubCharge = 0;
	clubCharging = false;
	
	if(!(game->getCameraOverhead()))
		game->addForceToBall(swingDir);
	else
		return;
	
	
	// Allow the player to swing the ball while it's moving, but at the cost of
	//     an extra stroke added to their score
	if(ballMoving)
		strokes += 2;
	else
		strokes += 1;
	
	ballMoving = true;
}


// Apply a force in a random direction (once per hole), and reward the player 
//    for gambling by subtracting a stroke from their score
void swingClubRandomly(){
	if(alreadyGambled) return;
	
	float power = (float)(rand()%5 + 8) / 10.0f;
	float theta = (float)(rand()%360);
	Vector3f swingDir = Vector3f(cos(PI*theta/180), 0, sin(PI*theta/180));
	swingDir *= power;
	game->addForceToBall(swingDir);
	clubCharge = 0;
	clubCharging = false;
	alreadyGambled = true;
	
	// Allow the player to swing the club randomly, and reward them by deducting
	//     a stroke from their score (may only be done once per level)
	strokes -= 1;
}


// Stop the ball from moving
void stopBall(){
	if(!ballMoving) return;
	game->setBallVelocity(Vector3f(0,0,0));
	ballMoving = false;
	// At the expense of a stroke, the player can stop the ball's motion
	strokes += 1;
}


// Charge the club until it reaches the maximum value (arbitrarily 5)
void chargeClub(){
	clubCharging = true;
	if(clubCharge < 1.4)
		clubCharge += .04;
	else clubCharge = 1.4;
}


void clickStartButton(){
	startButtonClicked = true;
}


void quit(){
	exit(0);
};





//// Callback functions ////

// Camera
void holdW(){ unlockCamera(); game->moveCameraHorizontallyRelative(0, .1f); }
void holdS(){ unlockCamera(); game->moveCameraHorizontallyRelative(0, -.1f); }
void holdA(){ unlockCamera(); game->moveCameraHorizontallyRelative(-.1f, 0); }
void holdD(){ unlockCamera(); game->moveCameraHorizontallyRelative(.1f, 0); }
void holdX(){ if(camUnlock > 0) unlockCamera(); if(game->getCameraZoom() > .3) game->zoomCamera(-.1f); prevCamZoom = game->getCameraZoom(); }
void holdZ(){ if(camUnlock > 0) unlockCamera(); if(game->getCameraZoom() < 5) game->zoomCamera(.1f); prevCamZoom = game->getCameraZoom(); }
void pressE(){ if(game->isLevelScene()) game->setCameraOverhead(true, true); }
void holdE(){ if(game->isLevelScene()) camUnlock = 1; }
void releaseE(){ game->setCameraOverhead(false, false); }
// Light
void holdI(){ game->moveLightHorizontallyRelative(0, .1f); }
void holdK(){ game->moveLightHorizontallyRelative(0, -.1f); }
void holdJ(){ game->moveLightHorizontallyRelative(-.1f, 0); }
void holdL(){ game->moveLightHorizontallyRelative(.1f, 0); }
void holdU(){ game->moveLight(Vector3f(0,-.1f,0)); }
void holdO(){ game->moveLight(Vector3f(0,.1f,0)); }
// Level actions
void pressQ(){ quit(); }
void holdC(){ chargeClub(); }
void releaseC(){ if(clubCharging) swingClub(); else clubCharge = 0; }




//// Conditional functions ////

bool readyToStart(){
	if(startButtonClicked){
		startButtonClicked = false;
		return(true);
	}
	return(false);
}

bool alarm(){
	if(counter <= 0)
		return(true);
	return(false);
};

bool returnTrue(){
	return(true);
};

bool hasWonLevel(){
	return(game->hasWonLevel());
}







int main(int argc, char** argv){
	string title = "____MiniGolf!____";
	//cout << title << endl;
	
	game = new Game(title, argv[1]);
	game->addFrameLogic(winCheck);

	int titleScene = game->addScene(doNothing, false);
	int levelScene = game->addScene(snapCamera, true);
	

	game->addSceneEnterAction(titleScene, doNothing);
	game->addTransition(titleScene, levelScene, readyToStart, doNothing);
	
	
	
	///////////////////////////
	////// Bind controls //////
	///////////////////////////
	
	
	// Bind controls to the appropriate scenes
	game->addSceneControl(levelScene, 'q', SPS_KEYSTATE_DOWN, quit);
	// Camera
	game->addSceneControl(levelScene, 'w', SPS_KEYSTATE_HELD, holdW);
	game->addSceneControl(levelScene, 'a', SPS_KEYSTATE_HELD, holdA);
	game->addSceneControl(levelScene, 's', SPS_KEYSTATE_HELD, holdS);
	game->addSceneControl(levelScene, 'd', SPS_KEYSTATE_HELD, holdD);
	game->addSceneControl(levelScene, 'x', SPS_KEYSTATE_HELD, holdX);
	game->addSceneControl(levelScene, 'z', SPS_KEYSTATE_HELD, holdZ);
	game->addSceneControl(levelScene, 'e', SPS_KEYSTATE_DOWN, pressE);
	game->addSceneControl(levelScene, 'e', SPS_KEYSTATE_HELD, holdE);
	game->addSceneControl(levelScene, 'e', SPS_KEYSTATE_UP, releaseE);
	// Light
	game->addSceneControl(levelScene, 'i', SPS_KEYSTATE_HELD, holdI);
	game->addSceneControl(levelScene, 'j', SPS_KEYSTATE_HELD, holdJ);
	game->addSceneControl(levelScene, 'k', SPS_KEYSTATE_HELD, holdK);
	game->addSceneControl(levelScene, 'l', SPS_KEYSTATE_HELD, holdL);
	game->addSceneControl(levelScene, 'u', SPS_KEYSTATE_HELD, holdU);
	game->addSceneControl(levelScene, 'o', SPS_KEYSTATE_HELD, holdO);
	// Club
	game->addSceneControl(levelScene, 'c', SPS_KEYSTATE_HELD, holdC);
	game->addSceneControl(levelScene, 'c', SPS_KEYSTATE_UP, releaseC);
	game->addSceneControl(levelScene, 'v', SPS_KEYSTATE_DOWN, stopBall);
	game->addSceneControl(levelScene, 'b', SPS_KEYSTATE_DOWN, swingClubRandomly);
	
	// Next level (for debugging/ skipping levels)
	game->addSceneControl(levelScene, 'm', SPS_KEYSTATE_DOWN, goToNextLevel);
	
	// (Redundantly) Set the game to recognize the default camera controls
	game->defaultCamera = true;
	
	
	
	// Get the course name, level name, and par before trying to draw them
	changeNames();
	
	
	
	///////////////////////
	////// Set up UI //////
	///////////////////////
	
	// Title scene
    Image* titleScreen = new Image((char*)"./assets/MiniGolfBG.spi");
    
    Image* startButtonIdle = new Image((char*)"./assets/StartButton.spi");
    Image* startButtonHover = new Image((char*)"./assets/StartButtonHover.spi");
    Image* startButtonClick = new Image((char*)"./assets/StartButtonClick.spi");
    
    Button* startButton = new Button(startButtonIdle, startButtonHover, startButtonClick);
    startButton->setClickFunction(clickStartButton);
    
    game->addElement(titleScene, startButton, 0.5, 0.5, .1, SPS_ALIGNMENT_TOP);
	game->addElement(titleScene, titleScreen, 0.5, 0, 1, SPS_ALIGNMENT_TOP);
    
    
	
	// Level scene
    Image* powerBarUncharged = new Image((char*)"./assets/PowerBarUncharged.spi");
    Image* powerBarCharged = new Image((char*)"./assets/PowerBarCharged.spi");
    Image* arrow = new Image((char*)"./assets/Arrow.spi");
	
	game->addElement(levelScene, powerBarCharged, 0, 1, .3, SPS_ALIGNMENT_BOTTOMLEFT, &SPS_GLOBAL_1, &clubCharge, 1, 1.4, SPS_ALIGNMENT_BOTTOMLEFT);
    game->addElement(levelScene, powerBarUncharged, 0, 1, .3, SPS_ALIGNMENT_BOTTOMLEFT);
    arrowID = game->addElement(levelScene, arrow, .5, .5, .1, SPS_ALIGNMENT_TOP);
    
	// Textual information for the level scene
    game->addElement(levelScene, (char*)"", courseName, 0, 0.1, SPS_ALIGNMENT_TOPLEFT);
	game->addElement(levelScene, (char*)"Score: ", &score, .85, 0.1, SPS_ALIGNMENT_TOPLEFT);
    game->addElement(levelScene, (char*)"", levelName, 0.17, 0.93, SPS_ALIGNMENT_BOTTOMLEFT);
    game->addElement(levelScene, (char*)"Par: ", &par, 0.13, 0.98, SPS_ALIGNMENT_BOTTOMLEFT);
    game->addElement(levelScene, (char*)"Strokes: ", &strokes, 0.35, 0.98, SPS_ALIGNMENT_BOTTOMLEFT);
    
	
	// Set the font
    game->setFont(levelScene, GLUT_BITMAP_TIMES_ROMAN_24, 24, Vector3f(1, 1, 1));
	
	// Bind the game object to the class
	bindGameObject(game);
	
	// Pass the window dimensions, argc, and argv (needed by GLUT), and start the game
	game->start(argc, argv, 600, 600);
	
	// This library of GLUT can't leave the main display loop, 
	//     so don't put anything after game->start()
	
	return(0);
};

