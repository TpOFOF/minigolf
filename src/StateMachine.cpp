//
//  StateMachine.cpp
//  EngineTest
//
//  Created by Chase Bradbury on 11/24/13.
//  Copyright (c) 2013 Chase Bradbury. All rights reserved.
//

#include <cstdlib>
#include "include/StateMachine.h"


void state_null_action() {
    return;
}

stateref make_state(IAction* action, IAction* entryAction, IAction* exitAction, int id, bool isLevel) {
    stateref state = (stateref)malloc(sizeof(state_t));
	
//	state->action = action;

    state->stateAction = action;
	state->entryAction = entryAction;
    state->exitAction = exitAction;
	
	state->transitions = new vector<transitionref>();
	state->transitions->clear();
	
    state->numTransitions = 0;
	state->stateID = id;
	state->inputManager = new InputManager();
	
	state->isLevelState = isLevel;
	state->UI = new UILayer(90);
	
    return state;
}

void make_transition(stateref fromState, IAction* action, ICondition* condition, stateref toState) {
    transitionref transition = (transitionref)malloc(sizeof(transition_t));
    transition->action = action;
    transition->condition = condition;
    transition->target = toState;
	
	fromState->transitions->push_back(transition);
	fromState->numTransitions ++;
}

void StateMachine::update()
{
    transitionref triggeredTransition = NULL;
	    
    for (int i = 0; i < currentState->numTransitions; ++i)
    {	
		if (currentState->transitions->at(i)->condition->test())
        {
            triggeredTransition = currentState->transitions->at(i);
            break;
        }
    }

    int actionCount = 0;
    if (triggeredTransition != NULL)
    {
		
        stateref targetState = triggeredTransition->target;
        IAction* currAction = currentState->exitAction;
        currAction->doAction();
        
        currAction = triggeredTransition->action;
        currAction->doAction();
        
        currentState = targetState;
		currentID = targetState->stateID;
		
        currAction = currentState->entryAction;
		currAction->doAction();
		
        currentState->logicAction->doAction();
		currentState->stateAction->doAction();
		if(currentState->isLevelState){
			currentState->physicsAction->doAction();
			currentState->renderAction->doAction();
		}
		currentState->hudAction->doAction();
		
		delete(currAction);
    } else {
		
		currentState->logicAction->doAction();
		currentState->stateAction->doAction();
		if(currentState->isLevelState){
			currentState->physicsAction->doAction();
			currentState->renderAction->doAction();
		}
		currentState->hudAction->doAction();
	}
	
}