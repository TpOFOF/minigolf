//
//  LevelData.cpp
//  MinigolfRendTest
//
//  Created by Chase Bradbury on 4/21/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#include "include/LevelData.h"

LevelData::LevelData()
{
	cup = NULL;
	tee = NULL;
	hasCup = false;
	hasTee = false;
};


LevelData::~LevelData(){
	int numTiles = (int)tiles.size();
	list<Tile*>::iterator iter = tiles.begin();
	for(int i = 0; i < numTiles; i++){
		free(*iter);
		if(i < numTiles-1)
			advance(iter, 1);
	}
	free(tee);
	free(cup);
};


// Add a new tile to the LevelData object
void LevelData::addTile(int ID, int numEdges, Vector3f* verts, int* neighbors, Vector3f normal, Vector3f* edgeNormals)
{
	Tile* newTile = (Tile*)malloc(sizeof(Tile));
	newTile->ID = ID;
	newTile->numEdges = numEdges;
	newTile->vertices = verts;           ////
	newTile->neighbors = neighbors;
	newTile->normal = normal;            ////
	newTile->edgeNormals = edgeNormals;  ////
	
	// Create a floor object based on the arguments
	newTile->floor.normal = normal;
	newTile->floor.points = verts;
	newTile->floor.density = 1;
	
	// Create an array of wall objects from the arguments
	newTile->walls = (Wall*)malloc(sizeof(Wall) * numEdges);
	Wall* ws = newTile->walls;
	for(int i = 0; i < numEdges; i++){
		ws[i].normal = edgeNormals[i];
		ws[i].points = (Vector3f*)malloc(sizeof(Vector3f) * 4);
		ws[i].solid = (neighbors[i] == 0)? true : false; // Wall is solid if no neighbor
		int neighbor = neighbors[i];
		ws[i].density = 1;
		Vector3f* wp = ws[i].points;
		//asdfasdf
		wp[0] = verts[i];
		wp[1] = verts[i] + Vector3f(0, .1, 0);
		if (i < numEdges - 1) {
			wp[2] = verts[i+1] + Vector3f(0, .1, 0);
			wp[3] = verts[i+1];
		} else {
			wp[2] = verts[0] + Vector3f(0, .1, 0);
			wp[3] = verts[0];
		}
	}
	
	
	tiles.push_back(newTile);
};


// Add a tee to the LevelData object if there isn't one already
void LevelData::addTee(int tileID, Vector3f vertex)
{
	if(hasTee){
		cout << "WARNING: LevelData: Level already has tee" << endl;
		return;
	}
	tee = (Tee*)malloc(sizeof(Tee));
	tee->tileID = tileID;
	tee->position = vertex;
	hasTee = true;
};


// Add a cup to the LevelData object if there isn't one already
void LevelData::addCup(int tileID, Vector3f vertex)
{
	if(hasCup){
		cout << "WARNING: LevelData: Level already has cup" << endl;
		return;
	}
	cup = (Cup*)malloc(sizeof(Cup));
	cup->tileID = tileID;
	cup->position = vertex;
	hasCup = true;
};

void LevelData::addSizeAndCenter(float inSize, Vector3f inCenter)
{
	size = inSize;
    center = inCenter;
};

// Return the tile of the specified ID
Tile* LevelData::getTile(int ID)
{
	for (list<Tile*>::iterator iter = tiles.begin(); iter != tiles.end(); iter++)
		if ((*iter)->ID == ID)
			return (*iter);
			
	return NULL;
};


// Return the pointer to the cup
Cup* LevelData::getCup(){
	return(cup);
}


// Return the pointer to the tee
Tee* LevelData::getTee(){
	return(tee);
}

Vector3f LevelData::getCenter(){
	return(center);
}

float LevelData::getSize(){
	return(size);
}


// Return the number of tiles contained in this LevelData object
int LevelData::getNumTiles()
{
	return tiles.size();
};


// Print the LevelData object's contents (formatted)
void LevelData::printData(){
	cout << "Total tiles: " << getNumTiles() << endl;
	cout << "___ Tiles ___" << endl;
	list<Tile*>::iterator iter = tiles.begin();
	int listSize = tiles.size();
	for(int i = 0; i < listSize; i++){
		printTile(*iter);
		cout << endl;
		if(i < listSize-1)
			advance(iter, 1);
	}
	printTee(tee);
	cout << endl;
	printCup(cup);
	cout << endl;
}


// Draw the cup (assumes the matrix is at the origin)
void LevelData::drawCup(){

	Cup* c = getCup();
	if(c == NULL){
		cout << "ERROR: LevelData: drawCup(): Cup position is null" << endl;
		exit(1);
	}
	
	glPushMatrix();
	
	// First translate the matrix to the correct position
	glTranslatef(c->position.x, c->position.y, c->position.z);
	
	// Next find the normal of the tile it's on and rotate the cup to align
	Vector3f N = (getTile(c->tileID))->normal;
	
	Vector3f UP = Vector3f(0,1,0);
	// Get the angle between the UP and N vectors
	float angle = acos( dot(N, UP) / N.length() );
	// Get the axis to rotate about
	Vector3f axis = normalize(UP * N);
	if(axis.isZero()){
		axis = UP;
		angle = 0;
	}

	glRotatef(angle*180/PI, axis.x, axis.y, axis.z);

	
	glTranslatef(0,.001,0);
	
	
	// Next draw the cylinder
	int sides = 8;
	float radius = .06;
	float height = .005;
	
	float dTh = 2.0f * PI / ((float)sides); // Change in angle per side
	
	glColor3f(0.1, 0.1, 0.1);
	float theta = 0;
	// Draw the ring
	glBegin(GL_QUAD_STRIP);
		for(int i = 0; i <= sides; i++){
			theta = (float)(i) * dTh;
			glNormal3f(cos(theta), 0, sin(theta));
			glVertex3f(radius*cos(theta), 0, radius*sin(theta));
			glVertex3f(radius*cos(theta), height, radius*sin(theta));
			
		}
	glEnd();
	
	// Draw the top
	glBegin(GL_TRIANGLE_FAN);
		glNormal3f(0, 1, 0);
		glVertex3f(0, height, 0);
		for(int i = 0; i <= sides; i++){
			theta = (float)(i) * dTh;
			glNormal3f(0, 1, 0);
			glVertex3f(radius*cos(theta), height, -radius*sin(theta));
		}
	glEnd();
	
	// Draw the bottom (optional, but might help debugging)
	glBegin(GL_TRIANGLE_FAN);
		glNormal3f(0, -1, 0);
		glVertex3f(0, 0, 0);
		for(int i = 0; i <= sides; i++){
			theta = (float)(i) * dTh;
			glNormal3f(0, -1, 0);
			glVertex3f(radius*cos(theta), 0, radius*sin(theta));
		}
	glEnd();
	
	glPopMatrix();
}

// Draw the tee (assumes the matrix is at the origin)
void LevelData::drawTee(){
	Tee* t = getTee();
	if(t == NULL){
		cout << "ERROR: LevelData: drawTee(): Tee position is null" << endl;
		exit(1);
	}
	
	glPushMatrix();
	
	// First translate the matrix to the correct position
	glTranslatef(t->position.x, t->position.y, t->position.z);
	
	// Next find the normal of the tile it's on and rotate the cup to align
	Vector3f N = (getTile(t->tileID))->normal;
	Vector3f UP = Vector3f(0,1,0);
	// Get the angle between the UP and N vectors
	float angle = acos( dot(N, UP) / N.length() );
	// Get the axis to rotate about
	Vector3f axis = normalize(UP * N);
	if(axis.isZero()){
		axis = UP;
		angle = 0;
	}
	glRotatef(angle*180/PI, axis.x, axis.y, axis.z);
	
	
	glTranslatef(0,.001,0);
	
	
	
	// Next draw the rectangles
	float length = .3f;
	float height = length / 15;
	
	glColor3f(0.2, 0.4, 7.0);
	glBegin(GL_QUADS);
		// Top side
		glNormal3f(0, 1, 0);
		glVertex3f(length/2, height, length/2);
		glVertex3f(-length/2, height, length/2);
		glVertex3f(-length/2, height, -length/2);
		glVertex3f(length/2, height, -length/2);
		
		glNormal3f(0, 0, 1);
		glVertex3f(-length/2, height, length/2);
		glVertex3f(length/2, height, length/2);
		glVertex3f(length/2, 0, length/2);
		glVertex3f(-length/2, 0, length/2);
		
		glNormal3f(-1, 0, 0);
		glVertex3f(-length/2, height, -length/2);
		glVertex3f(-length/2, height, length/2);
		glVertex3f(-length/2, 0, length/2);
		glVertex3f(-length/2, 0, -length/2);
		
		glNormal3f(0, 0, -1);
		glVertex3f(length/2, height, -length/2);
		glVertex3f(-length/2, height, -length/2);
		glVertex3f(-length/2, 0, -length/2);
		glVertex3f(length/2, 0, -length/2);
		
		glNormal3f(1, 0, 0);
		glVertex3f(length/2, height, length/2);
		glVertex3f(length/2, height, -length/2);
		glVertex3f(length/2, 0, -length/2);
		glVertex3f(length/2, 0, length/2);
		
		// Bottom side (Optional)
		glNormal3f(0, -1, 0);
		glVertex3f(length/2, 0, -length/2);
		glVertex3f(-length/2, 0, -length/2);
		glVertex3f(-length/2, 0, length/2);
		glVertex3f(length/2, 0, length/2);
	glEnd();
	
	glPopMatrix();
}
