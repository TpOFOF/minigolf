//
//  Button.cpp
//  Minigolf5
//
//  Created by Chase Bradbury on 5/22/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#include "include/Button.h"

Button::Button() {
    onHover = NULL;
    onClick = NULL;
    hovering = false;
    clicked = false;
    position = Vector3f(0, 0, 0);
}

Button::Button(Image* image) {
    idleImage = hoverImage = clickImage = image;
    onHover = NULL;
    onClick = NULL;
    hovering = false;
    clicked = false;
    position = Vector3f(0, 0, -1);
    size = Vector3f(idleImage->getWidth(), idleImage->getHeight(), 0);
    aspect = size.x/size.y;
}

Button::Button(Image* image, Image* imageHover, Image* imageClick) {
    idleImage = image;
    hoverImage = imageHover;
    clickImage = imageClick;
    onHover = NULL;
    onClick = NULL;
    hovering = false;
    clicked = false;
    position = Vector3f(0, 0, -1);
    size = Vector3f(idleImage->getWidth(), idleImage->getHeight(), 0);
    aspect = size.x/size.y;
}

void Button::setPosition(float x, float y, float depth) {
    position = Vector3f(x, y, 0);
}

void Button::setPosition(Vector3f pos) {
    position = pos;
}

void Button::setSize(float width, float height) {
    size = Vector3f(width, height, 0);
}

void Button::setSize(Vector3f sz) {
    size = sz;
}

void Button::setHoverFunction(void (*function)()) {
    onHover = function;
}

void Button::setClickFunction(void (*function)()) {
    onClick = function;
}

// Returns true if the mouse is over the button and sets isOver to the return value.
bool Button::mouseOver(float mouseX, float mouseY, float screenWidth, float screenHeight, float xOffset, float yOffset, float scale, SPS_ALIGNMENT alignment) {
    // Set button bounds in relation to screen.
    
    float positionX = xOffset*screenWidth/2 + screenWidth/2;
    float positionY = yOffset*screenHeight/2 + screenHeight/2;
    float sizeX = (screenWidth/2)*scale*(size.x/size.y);
    float sizeY = (screenHeight/2)*scale;
    
    if (screenHeight > screenWidth) {
        sizeX = (screenWidth/2)*scale*(size.x/size.y);
        sizeY = (screenHeight/2)*scale;
    } else {
        sizeX = (screenWidth/2)*scale*(size.x/size.y);
        sizeY = (screenHeight/2)*scale;
    }
    
    switch (alignment) {
        case SPS_ALIGNMENT_TOPLEFT:
            hovering = ((positionX < mouseX) && (positionX + sizeX > mouseX) && (positionY < mouseY) && (positionY + sizeY > mouseY));
            break;
        case SPS_ALIGNMENT_TOP:
            hovering = ((positionX - sizeX/2 < mouseX) && (positionX + sizeX/2 > mouseX) && (positionY < mouseY) && (positionY + sizeY > mouseY));
            break;
        case SPS_ALIGNMENT_TOPRIGHT:
            hovering = ((positionX - sizeX < mouseX) && (positionX > mouseX) && (positionY < mouseY) && (positionY + sizeY > mouseY));
            break;
        case SPS_ALIGNMENT_LEFT:
            hovering = ((positionX < mouseX) && (positionX + sizeX > mouseX) && (positionY - sizeY/2 < mouseY) && (positionY + sizeY/2 > mouseY));
            break;
        case SPS_ALIGNMENT_CENTER:
            hovering = ((positionX - sizeX/2 < mouseX) && (positionX + sizeX/2 > mouseX) && (positionY - sizeY/2 < mouseY) && (positionY + sizeY/2 > mouseY));
            break;
        case SPS_ALIGNMENT_RIGHT:
            hovering = ((positionX - sizeX < mouseX) && (positionX > mouseX) && (positionY - sizeY/2 < mouseY) && (positionY + sizeY/2 > mouseY));
            break;
        case SPS_ALIGNMENT_BOTTOMLEFT:
            hovering = ((positionX < mouseX) && (positionX + sizeX > mouseX) && (positionY - sizeY < mouseY) && (positionY > mouseY));
            break;
        case SPS_ALIGNMENT_BOTTOM:
            hovering = ((positionX - sizeX/2 < mouseX) && (positionX + sizeX/2 > mouseX) && (positionY - sizeY < mouseY) && (positionY > mouseY));
            break;
        case SPS_ALIGNMENT_BOTTOMRIGHT:
            hovering = ((positionX - sizeX < mouseX) && (positionX > mouseX) && (positionY - sizeY < mouseY) && (positionY > mouseY));
            break;
        default:
            break;
    }
    
    // Set variable 'hovering' to if mouse within position/size bounds.
    
    if (!hovering) clicked = false;
    return hovering;
}

void Button::click() {
    if (clicked && onClick != NULL) onClick();
    clicked = !clicked;
}

// Returns the depth (position.z). Helps to determine which Button to draw first if multiple overlap.
float priority();

void Button::runOnHover() {
    if (onHover != NULL) onHover();
}

void Button::runOnClick() {
    clicked = true;
    if (onClick != NULL) onClick();
}

void Button::draw() {
    
    glEnable(GL_TEXTURE_2D);
    
    if (clicked)
        clickImage->bindImage();
    else if (hovering)
        hoverImage->bindImage();
    else
        idleImage->bindImage();
    
    glBegin(GL_QUADS);
    // Bottom left
    glTexCoord2f(0.0f, 1.0f);
    glVertex3i(position.x, position.y - size.y, position.z);
    
    // Top left
    glTexCoord2f(0.0f, 0.0f);
    glVertex3i(position.x, position.y, position.z);
    
    // Top right
    glTexCoord2f(1.0f, 0.0f);
    glVertex3i(position.x + size.x, position.y, position.z);
    
    // Bottom right
    glTexCoord2f(1.0f, 1.0f);
    glVertex3i(position.x + size.x, position.y - size.y, position.z);
    glEnd();
    
    glDisable(GL_TEXTURE_2D);
}

void Button::drawToScreen(float screenWidth, float screenHeight, float xOffset, float yOffset, float scale, SPS_ALIGNMENT alignment) {
    
    float left;
    float right;
    float top;
    float bottom;
    
    float scaleX;
    float scaleY;
    
    if (screenHeight > screenWidth) {
        scaleX = 1;
        scaleY = screenWidth/screenHeight;
    } else {
        scaleX = screenHeight/screenWidth;
        scaleY = 1;
    }
    
    
    left = (position.x) + xOffset;
    right = (position.x + size.x*scale/screenWidth) + xOffset;
    top = (position.y - size.y*scale/screenHeight) - yOffset;
    bottom = (position.y) - yOffset;
    
    glEnable(GL_TEXTURE_2D);
    
    
    if (clicked)
        clickImage->bindImage();
    else if (hovering)
        hoverImage->bindImage();
    else
        idleImage->bindImage();
    
    switch(alignment) {
        case SPS_ALIGNMENT_FREE:
            left = (position.x)*scale/size.y + xOffset;
            right = (position.x + size.x)*scale/size.y + xOffset;
            top = (position.y - size.y)*scale/size.y - yOffset;
            bottom = (position.y)*scale/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_TOPLEFT:
            left = (position.x)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_TOP:
            left = (position.x - size.x/2)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x/2)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_TOPRIGHT:
            left = (position.x - size.x)*scale*scaleX/size.y + xOffset;
            right = (position.x)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_LEFT:
            left = (position.x)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y/2)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y/2)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_CENTER:
            left = (position.x - size.x/2)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x/2)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y/2)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y/2)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_RIGHT:
            left = (position.x - size.x)*scale*scaleX/size.y + xOffset;
            right = (position.x)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y/2)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y/2)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_BOTTOMLEFT:
            left = (position.x)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x)*scale*scaleX/size.y + xOffset;
            top = (position.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_BOTTOM:
            left = (position.x - size.x/2)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x/2)*scale*scaleX/size.y + xOffset;
            top = (position.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_BOTTOMRIGHT:
            left = (position.x - size.x)*scale*scaleX/size.y + xOffset;
            right = (position.x)*scale*scaleX/size.y + xOffset;
            top = (position.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y)*scale*scaleY/size.y - yOffset;
            break;
    }
    
    glBegin(GL_QUADS);
    // Bottom left
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(left, top, position.z);
    
    // Top left
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(left, bottom, position.z);
    
    // Top right
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(right, bottom, position.z);
    
    // Bottom right
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(right, top, position.z);
    glEnd();
    
    glDisable(GL_TEXTURE_2D);
}

void Button::drawToScreenWithRotation(float screenWidth, float screenHeight, float xOffset, float yOffset, float scale, SPS_ALIGNMENT alignment, float angle) {
    glPushMatrix();
    glRotatef(angle, 0, 0, 1);
    drawToScreen(screenWidth, screenHeight, xOffset, yOffset, scale, alignment);
    glPopMatrix();
}

void Button::drawToScreenUV(float screenWidth, float screenHeight, float xOffset, float yOffset, float scale, SPS_ALIGNMENT alignment, float scaleU, float scaleV, SPS_ALIGNMENT alignmentUV) {
    float left;
    float right;
    float top;
    float bottom;
    
    float scaleX;
    float scaleY;
    
    float uleft;
    float uright;
    float vtop;
    float vbottom;
    
    if (screenHeight > screenWidth) {
        scaleX = 1;
        scaleY = screenWidth/screenHeight;
    } else {
        scaleX = screenHeight/screenWidth;
        scaleY = 1;
    }
    
    
    left = (position.x) + xOffset;
    right = (position.x + size.x*scale/screenWidth) + xOffset;
    top = (position.y - size.y*scale/screenHeight) - yOffset;
    bottom = (position.y) - yOffset;
    
    glEnable(GL_TEXTURE_2D);
    
    
    if (clicked)
        clickImage->bindImage();
    else if (hovering)
        hoverImage->bindImage();
    else
        idleImage->bindImage();
    
    switch(alignment) {
        case SPS_ALIGNMENT_FREE:
            left = (position.x)*scale/size.y + xOffset;
            right = (position.x + size.x)*scale/size.y + xOffset;
            top = (position.y - size.y)*scale/size.y - yOffset;
            bottom = (position.y)*scale/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_TOPLEFT:
            left = (position.x)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_TOP:
            left = (position.x - size.x/2)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x/2)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_TOPRIGHT:
            left = (position.x - size.x)*scale*scaleX/size.y + xOffset;
            right = (position.x)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_LEFT:
            left = (position.x)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y/2)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y/2)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_CENTER:
            left = (position.x - size.x/2)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x/2)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y/2)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y/2)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_RIGHT:
            left = (position.x - size.x)*scale*scaleX/size.y + xOffset;
            right = (position.x)*scale*scaleX/size.y + xOffset;
            top = (position.y - size.y/2)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y/2)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_BOTTOMLEFT:
            left = (position.x)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x)*scale*scaleX/size.y + xOffset;
            top = (position.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_BOTTOM:
            left = (position.x - size.x/2)*scale*scaleX/size.y + xOffset;
            right = (position.x + size.x/2)*scale*scaleX/size.y + xOffset;
            top = (position.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y)*scale*scaleY/size.y - yOffset;
            break;
        case SPS_ALIGNMENT_BOTTOMRIGHT:
            left = (position.x - size.x)*scale*scaleX/size.y + xOffset;
            right = (position.x)*scale*scaleX/size.y + xOffset;
            top = (position.y)*scale*scaleY/size.y - yOffset;
            bottom = (position.y + size.y)*scale*scaleY/size.y - yOffset;
            break;
    }
    
    uleft = 0;
    uright = 1;
    vtop = 0;
    vbottom = 1;
    
    switch(alignmentUV) {
        case SPS_ALIGNMENT_FIXED:
            break;
        case SPS_ALIGNMENT_TOPLEFT:
            uright = scaleU;
            right = left + scaleU*scale*scaleX*aspect;
            vbottom = scaleV;
            top = bottom - scaleV*scale*scaleY;
            break;
        case SPS_ALIGNMENT_TOP:
            break;
        case SPS_ALIGNMENT_TOPRIGHT:
            break;
        case SPS_ALIGNMENT_LEFT:
            break;
        case SPS_ALIGNMENT_CENTER:
            break;
        case SPS_ALIGNMENT_RIGHT:
            break;
        case SPS_ALIGNMENT_BOTTOMLEFT:
            uright = scaleU;
            right = left + scaleU*scale*scaleX*aspect;
            vtop = 1 - scaleV;
            bottom = top + scaleV*scale*scaleY;
            break;
        case SPS_ALIGNMENT_BOTTOM:
            break;
        case SPS_ALIGNMENT_BOTTOMRIGHT:
            break;
    }
    
    glBegin(GL_QUADS);
    // Bottom left
    glTexCoord2f(uleft, vbottom);
    glVertex3f(left, top, position.z);
    
    // Top left
    glTexCoord2f(uleft, vtop);
    glVertex3f(left, bottom, position.z);
    
    // Top right
    glTexCoord2f(uright, vtop);
    glVertex3f(right, bottom, position.z);
    
    // Bottom right
    glTexCoord2f(uright, vbottom);
    glVertex3f(right, top, position.z);
    glEnd();
    
    glDisable(GL_TEXTURE_2D);
}