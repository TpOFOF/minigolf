//
//  UILayer.cpp
//  Minigolf5
//
//  Created by Chase Bradbury on 5/29/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#include "include/UILayer.h"

int UILayer::windowWidth = 0;

int UILayer::windowHeight = 0;

float UILayer::fov = 90.0;


UILayer::UILayer(float fovAngle) {
    fov = fovAngle;
    numElems = 0;
    numText = 0;
}

void UILayer::setWindowSize(int w, int h) {
    // Do stuff.
	windowWidth = w;
	windowHeight = h;
}

int UILayer::getWindowWidth() {
    return windowWidth;
}

int UILayer::getWindowHeight() {
    return windowHeight;
}

void UILayer::setWidth(int width) {
    windowWidth = width;
}

void UILayer::setHeight(int height) {
    windowHeight = height;
}

void UILayer::setFOV(float angle) {
    fov = angle;
}

void UILayer::reshape(int w, int h) {
    windowWidth = w;
    windowHeight = h;
	
	/* // Not sure this is necessary
    float aspect = windowWidth/windowHeight;
	glViewport(0, 0, windowWidth, windowHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fov, aspect, 0.01, 10000); // necessary to preserve aspect ratio
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	*/
}

void UILayer::setFont(void* font, int size, Vector3f color) {
    textFont = font;
    fontSize = size;
    fontColor = color;
}

int UILayer::addElement(Button* element, float x, float y, float scale, SPS_ALIGNMENT alignment) {
	UIElement tempElem;
	tempElem.button = element;
	tempElem.x = (x * 2) - 1;
	tempElem.y = (y * 2) - 1;
	tempElem.scale = scale * 2;
	tempElem.alignment = alignment;
    tempElem.u = &SPS_GLOBAL_1;
    tempElem.v = &SPS_GLOBAL_1;
    tempElem.alignmentUV = SPS_ALIGNMENT_FIXED;
	tempElem.visible = true;
	
	elements.push_back(tempElem);
	return(elements.size() - 1);
}

int UILayer::addElement(Button* element, float x, float y, float scale, SPS_ALIGNMENT alignment, float* refU, float* refV, float maxU, float maxV, SPS_ALIGNMENT alignmentUV) {
    //if (numAdded < numElems) {
    //UIElement* tempElem = new UIElement();
    UIElement tempElem;// = (UIElement*)malloc(sizeof(UIElement));
    tempElem.button = element;
    tempElem.x = (x * 2) - 1;
    tempElem.y = (y * 2) - 1;
    tempElem.scale = scale * 2;
    tempElem.alignment = alignment;
    tempElem.u = refU;
    tempElem.v = refV;
    tempElem.maxU = maxU;
    tempElem.maxV = maxV;
    tempElem.alignmentUV = alignmentUV;
    //tempElem.verticalAlignment = vAlign;
    /*
     switch(tempElem.horizontalAlignment){
     case UI_FIX_LEFT: tempElem.x = x; break;
     case UI_FIX_CENTER: tempElem.x = (windowWidth / 2) + x; break;
     case UI_FIX_RIGHT: tempElem.x = windowWidth - x; break;
     }
     switch(tempElem.verticalAlignment){
     case UI_FIX_BOTTOM: tempElem.y = y; break;
     case UI_FIX_CENTER: tempElem.y = (windowHeight / 2) + y; break;
     case UI_FIX_TOP: tempElem.y = windowHeight - y; break;
     }*/
	 
    tempElem.visible = true;
    
    elements.push_back(tempElem);
    //elements[numAdded++] = tempElem;
    //} else {
    //cout << "Cannot add more than " << numElems << " UIElements." << endl;
    //}
	return(elements.size() - 1);
}

int UILayer::addElement(Image* element, float x, float y, float scale, SPS_ALIGNMENT alignment) {
	UIElement tempElem;
	tempElem.button = new Button(element, element, element);
	tempElem.x = (x * 2) - 1;
	tempElem.y = (y * 2) - 1;
	tempElem.scale = scale * 2;
	tempElem.alignment = alignment;
    tempElem.u = &SPS_GLOBAL_1;
    tempElem.v = &SPS_GLOBAL_1;
    tempElem.alignmentUV = SPS_ALIGNMENT_FREE;
		//tempElem.verticalAlignment = vAlign;
        /*
		switch(tempElem.horizontalAlignment){
			case UI_FIX_LEFT: tempElem.x = x; break;
			case UI_FIX_CENTER: tempElem.x = (windowWidth / 2) + x; break;
			case UI_FIX_RIGHT: tempElem.x = windowWidth - x; break;
		}
		switch(tempElem.verticalAlignment){
			case UI_FIX_BOTTOM: tempElem.y = y; break;
			case UI_FIX_CENTER: tempElem.y = (windowHeight / 2) + y; break;
			case UI_FIX_TOP: tempElem.y = windowHeight - y; break;
		}*/
    
    tempElem.visible = true;
	
    elements.push_back(tempElem);
		//elements[numAdded++] = tempElem;
    //} else {
        //cout << "Cannot add more than " << numElems << " UIElements." << endl;
    //}
	return(elements.size() - 1);
}

int UILayer::addElement(Image* element, float x, float y, float scale, SPS_ALIGNMENT alignment, float* refU, float* refV, float maxU, float maxV, SPS_ALIGNMENT alignmentUV) {
    UIElement tempElem;
    tempElem.button = new Button(element, element, element);
    tempElem.x = (x * 2) - 1;
    tempElem.y = (y * 2) - 1;
    tempElem.scale = scale * 2;
    tempElem.alignment = alignment;
    tempElem.u = refU;
    tempElem.v = refV;
    tempElem.maxU = maxU;
    tempElem.maxV = maxV;
    tempElem.alignmentUV = alignmentUV;
    //tempElem.verticalAlignment = vAlign;
    /*
     switch(tempElem.horizontalAlignment){
     case UI_FIX_LEFT: tempElem.x = x; break;
     case UI_FIX_CENTER: tempElem.x = (windowWidth / 2) + x; break;
     case UI_FIX_RIGHT: tempElem.x = windowWidth - x; break;
     }
     switch(tempElem.verticalAlignment){
     case UI_FIX_BOTTOM: tempElem.y = y; break;
     case UI_FIX_CENTER: tempElem.y = (windowHeight / 2) + y; break;
     case UI_FIX_TOP: tempElem.y = windowHeight - y; break;
     }*/
    
    tempElem.visible = true;
	
    elements.push_back(tempElem);
    //elements[numAdded++] = tempElem;
    //} else {
    //cout << "Cannot add more than " << numElems << " UIElements." << endl;
    //}
	return(elements.size() - 1);
}

int UILayer::addElement(UIElement element) {
    /*if (numAdded < numElems) {
        elements[numAdded++] = element;
    } else {
        cout << "Cannot add more than " << numElems << " UIElements." << endl;
    }*/
    
    elements.push_back(element);
	return(elements.size() - 1);
}

int UILayer::addElement(char* text1, char* text2, float screenPosX, float screenPosY, SPS_ALIGNMENT alignment) {
    UIText tempText;
    //tempText.text = concat(text1, text2);
	tempText.text = text1;
    tempText.var = NULL;
	tempText.varText = text2;
    tempText.x = screenPosX;
    tempText.y = screenPosY;
    tempText.alignment = alignment;
    tempText.visible = true;
	
    texts.push_back(tempText);
	return(texts.size() - 1);
}

int UILayer::addElement(char* text, int* var, float  screenPosX, float screenPosY, SPS_ALIGNMENT alignment) {
    UIText tempText;
    tempText.text = text;
    tempText.var = var;
	tempText.varText = NULL;
    tempText.x = screenPosX;
    tempText.y = screenPosY;
    tempText.alignment = alignment;
    tempText.visible = true;
	
    texts.push_back(tempText);
	return(texts.size() - 1);
}

void UILayer::click(int x, int y) {
    for (int i = 0; i < elements.size(); ++i) {
        if(elements[i].button->mouseOver(x, y, windowWidth, windowHeight, elements[i].x, elements[i].y, elements[i].scale, elements[i].alignment)) {
            if(elements[i].visible)
				elements[i].button->click();
        }
    }
}

void UILayer::update(int x, int y) {
    for (int i = 0; i < elements.size(); ++i) {
        elements[i].button->mouseOver(x, y, windowWidth, windowHeight, elements[i].x, elements[i].y, elements[i].scale, elements[i].alignment);
    }
}


// Switch the viewport to 2d
void setUp2D(){
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //glOrtho(0.0, screenWidth, 0.0, screenHeight, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glDisable(GL_CULL_FACE);
    
    glClear(GL_DEPTH_BUFFER_BIT);
}


// Set up the 3d viewport
void setUp3D() {
	//UILayer* UI = currentState->UI;
	int w = UILayer::getWindowWidth();
	int h = UILayer::getWindowHeight();
	float aspect = ((float)(w)) / ((float)(h));
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90.0, aspect, 0.01, 10000); // necessary to preserve aspect ratio
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}



void UILayer::draw() {
	setUp2D();
    for (int i = 0; i < texts.size(); ++i) {
        if(!texts[i].visible) continue;
		
        char* tmpString;
        if (texts[i].var == NULL) {
			if(texts[i].varText != NULL){
				tmpString = concat(texts[i].text, texts[i].varText);
			} else {
				tmpString = texts[i].text;
			}
		} else {
            char floatConv[10];
            snprintf(floatConv, sizeof(floatConv), "%d", *texts[i].var);
            tmpString = concat(texts[i].text, floatConv);
        }
        drawText(texts[i].x, texts[i].y, textFont, fontSize, tmpString, fontColor, texts[i].alignment);
    }
	setUp3D();
    for (int i = 0; i < elements.size(); ++i) {
		if(elements[i].visible)
			elements[i].button->drawToScreenUV(windowWidth, windowHeight, elements[i].x, elements[i].y, elements[i].scale, elements[i].alignment, *elements[i].u/elements[i].maxU, *elements[i].v/elements[i].maxV, elements[i].alignmentUV);
    }
}

void UILayer::moveElements(){
	// Reposition all of the UIElements based on window dimensions
	
	
	/* If possible/ time allows, also adjust the scale to maintain the same proportion */
	
	
	for(int i = 0; i < numElems; i++){
		UIElement tempElem = elements[i];
		float x = tempElem.x;
		float y = tempElem.y;
		/*switch(tempElem.horizontalAlignment){
			case UI_FIX_LEFT: tempElem.x = x; break;
			case UI_FIX_CENTER: tempElem.x = (windowWidth / 2) + x; break;
			case UI_FIX_RIGHT: tempElem.x = windowWidth - x; break;
		}
		switch(tempElem.verticalAlignment){
			case UI_FIX_BOTTOM: tempElem.y = y; break;
			case UI_FIX_CENTER: tempElem.y = (windowHeight / 2) + y; break;
			case UI_FIX_TOP: tempElem.y = windowHeight - y; break;
		}*/
	}
}


void UILayer::hideElement(int element){
	if(element < elements.size() && element >= 0)
		elements[element].visible = false;
	else
		cout << "WARNING: UILayer::hideElement(): Element does not exist" << endl;
}

void UILayer::showElement(int element){
	if(element < elements.size() && element >= 0)
		elements[element].visible = true;
	else
		cout << "WARNING: UILayer::showElement(): Element does not exist" << endl;
}

void UILayer::hideText(int text){
	if(text < texts.size() && text >= 0)
		texts[text].visible = false;
	else
		cout << "WARNING: UILayer::hideText(): Text does not exist" << endl;
}

void UILayer::showText(int text){
	if(text < texts.size() && text >= 0)
		texts[text].visible = true;
	else
		cout << "WARNING: UILayer::showText(): Text does not exist" << endl;
}

