//
//  Vector3f.h
//  CMPS160-Lab3
//
//  Created by Chase Bradbury on 11/11/13.
//  Copyright (c) 2013 Chase Bradbury. All rights reserved.
//

#ifndef __CMPS160_Lab3___Vector3f_h_
#define __CMPS160_Lab3___Vector3f_h_

#include <stdlib.h>
#include <stdio.h>
//#include <GLUT/glut.h>
//#include "./GL/glut.h"
#include "./glut.fwd.h"
#include <math.h>
#include <iostream>
using namespace std;

/*** Vector class ***/

class Vector3f {
public:
    float x;
    float y;
    float z;
    
    Vector3f();
    Vector3f(float x, float y, float z);
    
    void normalize();
    void reflect(Vector3f normal);
    bool isNormal();
    float length();
	bool isZero();
    
    Vector3f operator +(Vector3f);
    Vector3f operator -(Vector3f);
    Vector3f operator *(float); // Scalar multiplication
	Vector3f operator /(float); // Scalar division
    Vector3f operator *(Vector3f); // Cross product
    Vector3f operator ++();
    Vector3f operator --();
    Vector3f operator +=(Vector3f);
    Vector3f operator -=(Vector3f);
    Vector3f operator *=(float); // Scalar multiplication
	Vector3f operator /=(float); // Scalar division
    Vector3f operator *=(Vector3f); // Cross product
    friend ostream& operator <<(ostream& stream, const Vector3f& vector);
	
	// Comparison operators
	bool operator==(Vector3f v);
	bool operator!=(Vector3f v);
	bool operator<(Vector3f v);
	bool operator>(Vector3f v);
	bool operator<=(Vector3f v);
	bool operator>=(Vector3f v);
};

ostream& operator <<(ostream& stream, const Vector3f& vector);

Vector3f normalize(Vector3f);
Vector3f reflect(Vector3f input, Vector3f normal);
Vector3f project(Vector3f a, Vector3f b);
float dot(Vector3f v1, Vector3f v2);

#endif /* defined(__CMPS160_Lab3___dVector__) */
