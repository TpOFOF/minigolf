#ifndef __GAME_HEADER__
#define __GAME_HEADER__

#include <cstdlib> 
#include <cstring>
#include <string>
#include <vector>
#include "./Vector3f.h"
#include "./LevelNew.h"
#include "./StateMachine.h"
#include "./UILayer.h"

using namespace std;

inline void nullFunction(){};

class Game{

private:
	static int numStates;
	static vector<stateref> states;
	static StateMachine* FSM;
	static Level** levels;
	static Level* currentLevel;
	static int totalLevels;
	static int currentLevelNum;
	static string windowTitle;
	static UILayer* UI;
	
	static Game* gameObject;
	static bool gameObjectBound;

//public:
	
	float lightPosition[4];
	
	static Vector3f camP;
	static Vector3f camR;
	
	
	static Vector3f overheadPos;
	static float camZoom;
	static bool overheadCam;
	static bool smoothOverheadMove;
	
	bool drawLight = true;
	
//private:
	static int screenWidth;
	static int screenHeight;
	
	static IAction* logicAction;
	static IAction* physicsAction;
	static IAction* renderAction;
	
	static void drawUI();
	void drawLightObject();
	
	static void cb_display();
	static void cb_idle();
	static void cb_reshape(int w, int h);
	static void cb_mouseclick(int button, int state, int x, int y);
	static void cb_mousemove(int x, int y);
	static void cb_mousepassive(int x, int y);
	static void cb_keyboard(unsigned char key, int x, int y);
	static void cb_keyboardUp(unsigned char key, int x, int y);
	
	friend void doPhysics();
	friend void doRender();
	
	
public:
	/* ********* Engine constructor functions ********* */
	
	// _____________ ENGINE USAGE: ____________
	// In order to properly use the engine, call the following in order:
	//
	//      /* Globally-scoped variables used in callback functions */
	//
	//      Game* g;    // Also globally scoped
	//
	//      /* Callback functions themselves */
	//		void doNothing(){};     // Function that does nothing (you'll need one.)
	//
	// 		g = new Game("nameOfGame", argv[1]);    // actual names can vary
	//
	//		/* Add scenes, transitions, controls, etc. here */
	//
	//		bindGameObject(g);
	//		g->start(argc, argv, 500, 500);  // The screen width and height may vary
	//                                       // but argc and argv must be unmodified
	//
	// _________________________________________
	
	// Create a new instance of the Game class 
	//    (pass in the title of the window and the file path of the course file)
	Game(string gameName, char* courseFile);
	
	// Destructor
	~Game();
	
	// Bind the game object to the class (NECESSARY FOR FUNCTIONALITY)
	friend void bindGameObject(Game* G);

	// Begin the game, passing in unmodified argc and argv arguments, 
	//     screen width and height. No code after the call to start() will be reached
	void start(int argc, char** argv, int sWidth, int sHeight);
	
	// End the game. (Must be called from a callback function)
	void end();
	
	
	
	/* ********* Scene functions ********* */
	////  For those familiar with Finite State Machines, scenes are states
	////  with the specific task of handling the game's update loop functions.
	////     
	////	 NOTE: Scene functions take in function pointers as arguments
	////     Usage follows this format:    
	////                  
	////				  void someFunction() { 
	////				      /* some actions */ 
	////				  };
	////
	////                  addFrameLogic(someFunction);   // someFunction = (void (*function)())
	////
	
	// Add a function to be called every update step (just after the input updates)
	void addFrameLogic(void (*logicFunction)());
	
	// Add a new scene (requires frame logic to be defined), and return an int representing it
	int addScene(void (*stateFunction)(), bool isLevelState);
	
	//     addTransition takes a pointer to a conditional function of this format:
	//     
	//      bool someCondition(){
	//			if( /*condition expression*/ ){
	//				return(true);
	//			} else {
	//				return(false);
	//			}
	//	    }
	//
	// Add a transition between scenes (use the int return values from addScene)
	void addTransition(int scene1, int scene2, bool (*condFunction)(), void (*actFunction)());

	// Add an action to be performed upon entering a scene (excluding game start)
	void addSceneEnterAction(int scene, void (*entryFunction)());
	
	// Add an action to be performed when a scene is exited
	void addSceneExitAction(int scene, void (*exitFunction)());
	
	// Add a keyboard control to a scene
	void addSceneControl( int sceneID, SPS_KEY key, SPS_KEYSTATE state, void (*keyFunction)() );
	
	
	// Return the int representing the current scene (same id as returned by addScene)
	int getCurrentScene();
	
	
	// Set the current scene to a specific scene 
	//    (this will bypass transition, exit, and enter actions)
	void setCurrentScene(int s);
	
	// Return whether the current scene is set to a level scene
	bool isLevelScene();
	
	
	// Get the index of the current level
	int getLevel();
	
	// Set the level to a specific index
	void setLevel(int l);
	
	// Move on to the next level
	void nextLevel();
	
	
	
	/* ********* Ball functions ********* */
	
	
	// Get the position of the ball
	Vector3f getBallPosition();
	
	// Set the position of the ball
	//     (This can potentially conflict with ordinary physics updates)
	void setBallPosition(Vector3f pos);
	
	// Get the velocity of the ball
	Vector3f getBallVelocity();
	
	// Set the velocity of the ball directly
	void setBallVelocity(Vector3f vel);
	
	// Add a force vector to the ball's velocity
	void addForceToBall(Vector3f f);
	
	// Return whether the ball has reached the cup yet
	bool hasWonLevel();
	
	
	
	
	/* ********* Camera functions ********* */
	
	// Reset the camera's position
	void initializeCamera();
	
	// Track the camera towards a point by a ratio of ratio
	void trackCameraToPosition(Vector3f pos, float ratio); // Good ratio: 1/7
	
	// Move the camera in a certain direction
	void moveCamera(Vector3f dir);
	
	// Move the camera relative to its orientation (horizontally only, Z axis is forward)
	void moveCameraHorizontallyRelative(float localX, float localZ);
	
	// Get the position of the camera
	Vector3f getCameraPosition();
	
	
	// Rotate the camera
	void rotateCamera(float xRot, float yRot, float zRot);
	
	// Rotate the camera to a specific orientation
	void rotateCameraTo(float xRot, float yRot, float zRot);
	
	// Adjust the rotation of the camera by a certain ratio
	//     NOTE: Default camera rotation is Vector3f(45, 0, 0)
	//           If you use this function at all, you'll want to store
	//			 the vector to return to afterwards
	void trackCameraToRotation(Vector3f rot, float ratio);
	
	// Get the rotation of the camera
	//     NOTE: The Vector3f represents the rotation of the camera about the
	//           x, y, and z axes corresponding to the Vector3f's components.
	//           (It is _NOT_ the vector about which the camera is rotated.)
	Vector3f getCameraRotation();
	
	
	// Zoom the camera to a certain amount (minimum 0)
	void setCameraZoom(float zoom);
	
	// Zoom the camera relative to a certain amount (minimum 0)
	void zoomCamera(float zoomAmount);
	
	// Adjust the zoom of the camera by a certain ratio (default is 2)
	void trackCameraToZoom(float zoom, float ratio);
	
	// Get the zoom level of the camera
	float getCameraZoom();
	
	// Set the camera to a top-down perspective on the level (with optional smooth transition)
	void setCameraOverhead(bool isOverhead, bool smoothTransition);
	
	// Get whether the camera is overhead
	bool getCameraOverhead();
	
	
	
	/* ********* Light functions ********* */
	
	// Reset the light's position
	void initializeLight();
	
	// Set the light position
	void setLightPosition(Vector3f pos);
	
	// Move the light in a direction
	void moveLight(Vector3f dir);
	
	// Move the light relative to the camera's orientation
	void moveLightHorizontallyRelative(float localX, float localZ);
	
	// Get the position of the light
	Vector3f getLightPosition();
	
	
	
	
	
	/* ********* Screen functions ********* */
	
	// Get the width of the screen
	static int getScreenWidth();
	
	// Get the height of the screen
	static int getScreenHeight();

	
	// Get the x position of the mouse on the screen
	static int getMouseX();
	
	// Get the y position of the mouse on the screen
	static int getMouseY();
	
	// Returns whether a certain mouse button is down
	static bool isClicked(SPS_MOUSE mouseButton);
	
	// Returns the last key pressed
    static SPS_KEY getLastKeyPressed();
    
    // Returns whether a certain key is pressed
    static bool isPressed(SPS_KEY key);
	
	
	
	
	
	/* ********* UI functions ********* */
    
    // Set the font for a UILayer text
    void setFont(int sceneID, void* font, int size, Vector3f color);
    
    // Add a Button element with no UI variance
	int addElement(int sceneID, Button* element, float screenPosX, float screenPosY, float scale, SPS_ALIGNMENT alignment);
    
    // Add a Button element with UI variance
    int addElement(int sceneID, Button* element, float x, float y, float scale, SPS_ALIGNMENT alignment, float* refU, float* refV, float maxU, float maxV, SPS_ALIGNMENT alignmentUV);
    
    // Add an Image element with no UI variance
    int addElement(int sceneID, Image* element, float screenPosX, float screenPosY, float scale, SPS_ALIGNMENT alignment);
    
    // Add an Image element with no UI variance
    int addElement(int sceneID, Image* element, float x, float y, float scale, SPS_ALIGNMENT alignment, float* refU, float* refV, float maxU, float maxV, SPS_ALIGNMENT alignmentUV);
    
    // Add a text element
    int addElement(int sceneID, char* text1, char* text2, float screenPosX, float screenPosY, SPS_ALIGNMENT alignment);
    
    // Add a text element with an int variable
    int addElement(int sceneID, char* text, int* var, float screenPosX, float screenPosY, SPS_ALIGNMENT alignment);
	
	// Hide a UI element
	void hideElement(int element);

	// Show a UI element
	void showElement(int element);

	// Hide a UI text element
	void hideText(int text);

	// Show a UI text element
	void showText(int text);
	
	
	static string courseName;
	static string currentLevelName;
	static int currentLevelPar;
	
	// Control whether or not to use the default camera controls in the level
	static bool defaultCamera;
	
	
	/* ********* Pointless functions ********* */
	
	// The user does not ever need this, as they already have a pointer
	//     to the game object at the global scope of their program
	static Game* getGameObject(); 
	
};



#endif
