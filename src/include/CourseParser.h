#ifndef COURSEPARSER_H
#define COURSEPARSER_H

#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>
#include "./LevelNew.h" //// FIX
#include "./LevelData.h"

// Parser object to parse file information and pass a LevelData object
//    to the Level. Can either call Parser(), then loadLevel(char* s)
//    or Parser(char* s), then loadLevel()
class CourseParser{

public:
	CourseParser(char* s);
	~CourseParser();
	
	//LevelData* loadLevel();
	//LevelData* loadLevel(char* s);
	
	Level** getLevels(); // Just return a Level*, not a Level**
	int getNumLevels();
	string courseName;
	
	//LevelData** loadLevelsFromFile(int* numLevels);
	//LevelData*** loadAllLevels(char* s, int* numLevels);
	
private:
	char* fn;
	int numLevels;
	vector<Level*> levels;
};

#endif
