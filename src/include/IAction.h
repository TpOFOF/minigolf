//
//  IAction.h
//  EngineTest
//
//  Created by Chase Bradbury on 11/24/13.
//  Copyright (c) 2013 Chase Bradbury. All rights reserved.
//

#ifndef EngineTest_IAction_h
#define EngineTest_IAction_h

#include "./Game.fwd.h"
#include "./Level.fwd.h"
//#include "./Level.h"
//#include "./Game.h"


class IAction {
private:
    void (*function)();
    void (*functionWithArgs)(Level*);
    void (*functionWithGameArg)(Game*);
	
public:
    void setAction(void (*f)()) {
        function = f;
    }
	
	void setActionWithArgs(void (*f)(Level*)) {
        functionWithArgs = f;
    }
	
	void setActionWithGameArg(void (*f)(Game*)) {
        functionWithGameArg = f;
    }
    
    
	void doAction() {
        return function();
    };
	
	void doAction(Level* curr) {
        return functionWithArgs(curr);
    };
	
	void doAction(Game* curr) {
        return functionWithGameArg(curr);
    };
};

#endif
