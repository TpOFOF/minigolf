//
//  Ball.h
//  Minigolf3
//
//  Created by Chase Bradbury on 4/30/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#ifndef __Minigolf3__Ball__
#define __Minigolf3__Ball__

#include <iostream>
#include "./Vector3f.h"
#include "./LevelData.h"
#include "./LevelObjects.h"
#include "./Util.h"

class SphereCollider {
private:
    int tileID;
    Vector3f position;
    Vector3f velocity;
    Tile* tile;
    float mass;
    Vector3f force;
    float radius;
	LevelData* LD;
	float gravity = .05;
    bool inCup = false;
    
public:
    bool testgrav = true;
    
    SphereCollider(LevelData* data);
    
    SphereCollider(LevelData* data, Vector3f initPosition);
    
    SphereCollider(LevelData* data, Vector3f initPosition, Tile* initTile);
    
    void restart();
    
    void setTile(Tile* newTile);
	
	void setTile(int newTile);
	
	int getTile();
    
    void setRadius(float newRadius);
    
    void setMass(float newMass);
    
    Vector3f getPosition(){ return position; };
	
	void setPosition(Vector3f pos){ position = pos; };
	
	Vector3f getVelocity(){ return velocity; };
	
	void setVelocity(Vector3f vel){ velocity = vel; };
    
    // Returns the ID of the tile
    int collideAndReturnNewTileID();
    
    //returns true if sphere will collide with a plane in the next frame, false if it won't.
    //point:
    bool willCollide(Vector3f ray, Vector3f normal);
    
    void addForce(Vector3f newForce);
    
    Vector3f getAcceleration();
    
    bool isInCup();
    
    void update();
    
    void draw();
};


#endif /* defined(__Minigolf3__Ball__) */
