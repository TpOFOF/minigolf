//
//  ICondition.h
//  EngineTest
//
//  Created by Chase Bradbury on 11/24/13.
//  Copyright (c) 2013 Chase Bradbury. All rights reserved.
//

#ifndef EngineTest_ICondition_h
#define EngineTest_ICondition_h


class ICondition {
private:
    bool (*function)();
    
    
public:
    void setTest(bool (*f)()) {
        function = f;
    }
    
    
	bool test() {
        return function();
    };
	
};

#endif
