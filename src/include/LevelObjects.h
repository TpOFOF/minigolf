//
//  LevelObjects.h
//  MinigolfRendTest
//
//  Created by Chase Bradbury on 4/21/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#ifndef MinigolfRendTest_LevelObjects_h
#define MinigolfRendTest_LevelObjects_h

#include <cstdlib>
#include <cstdio>
#include <iostream>
//#include "./GL/glut.h"
//#include "./GL/gl.h"
//#include "./GL/glu.h"
#include "./Vector3f.h"

#define PI 3.14159265

using namespace std;


struct Floor {
	Vector3f normal;
	Vector3f* points;
	float density; // For optional part of reflection calculation
};

struct Wall {
	Vector3f normal; 
	Vector3f* points;
	bool solid;
	int neighbor;
	float density;
};

struct Tile {
    int ID;
    int numEdges;
    Vector3f* vertices; // See floor
    int* neighbors;
	Vector3f normal; // See floor
	Vector3f* edgeNormals; // See walls
	Floor floor;
	Wall* walls;
};
	
struct Ball {
	int currentID;
    Vector3f position;
	Vector3f velocity;
	Vector3f acceleration;
	float radius;
	float mass;
};

struct Cup {
	int tileID;
    Vector3f position;
};

struct Tee {
	int tileID;
    Vector3f position;
};


void printVector3f(Vector3f& v);
void printTile(Tile* t);
void printTee(Tee* t);
void printCup(Cup* c);

#endif
