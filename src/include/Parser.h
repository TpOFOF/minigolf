#ifndef PARSER_H
#define PARSER_H

#include <cstdlib>
#include "./LevelData.h"

// Parser object to parse file information and pass a LevelData object
//    to the Level. Can either call Parser(), then loadLevel(char* s)
//    or Parser(char* s), then loadLevel()
class Parser{

public:
	Parser();
	Parser(char* s);
	~Parser();
	LevelData* loadLevel();
	LevelData* loadLevel(char* s);
	
	//LevelData** loadLevelsFromFile(int* numLevels);
	//LevelData*** loadAllLevels(char* s, int* numLevels);
	
private:
	char* fn;
};

#endif
