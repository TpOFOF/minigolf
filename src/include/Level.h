//
//  Level.h
//  MinigolfRendTest
//
//  Created by Chase Bradbury on 4/18/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#ifndef __MinigolfRendTest__Level__
#define __MinigolfRendTest__Level__

#include <iostream>
#include "./glut.fwd.h"

#include "./LevelData.h"
#include "./LevelObjects.h"
#include "SphereCollider.h"

class Level {
private:
    char* filename;
    LevelData* data;
    SphereCollider* ball;
    int tileID;
    
	bool debugTile = false;
	
public:
	void testgrav() { ball->testgrav = !ball->testgrav;}

    Level(char* file, Vector3f* overheadCamPos);
	
	Tile* getTile(int ID);
    
	Vector3f getBallPosition();
    
	void setBallPosition(Vector3f pos);
	
    bool hasWon();
    
    void restartBall();
	
	void addForceToBall(Vector3f force);
	
	void updateBall();
	
    void drawLevel();
    
    void drawTile(int ID);
    
    void drawTileBorders(int ID);
	
	void drawLight(float lx, float ly, float lz);
    
    void drawBall();
	
	/*
	// Test function for ball movement
	void applyAVP();
	
	void applyForce(Vector3f force);
	
	void applyPhysics();
	*/
	
    //int run(){while(true){}; return 0;};
    
    int getNumTiles(){ return data->getNumTiles(); };
    
    void drawBox(float l, float w, float h, float wStr, float hStr, float lSk, float wSk, float hSk, int ends);
    
};


inline void drawLevelPtr(Level* L){
	L->drawLevel();
}

inline void updateBallPtr(Level* L){
	L->updateBall();
}


#endif /* defined(__MinigolfRendTest__Level__) */
