//
//  Button.h
//  Minigolf5
//
//  Created by Chase Bradbury on 5/22/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#ifndef __Minigolf5__Button__
#define __Minigolf5__Button__

#include <iostream>
//#include <GLUT/GLUT.h>
//#include "GL/glut.h"
#include "./glut.fwd.h"
#include "Vector3f.h"
#include "Image.h"
#include "SPSMacros.h"

class Button {
private:
    void (*onHover)();
    void (*onClick)();
    
    Image* idleImage;
    Image* hoverImage;
    Image* clickImage;
    
    Vector3f position;
    Vector3f size;
    
    bool hovering;
    bool clicked;
    
    int alignment;
    float aspect;
    
public:
    // Default constructor.
    Button();
    
    Button(Image* image);
    
    Button(Image* image, Image* imageHover, Image* imageClick);
    
    void setPosition(float x, float y, float depth);
    
    void setPosition(Vector3f pos);
    
    void setSize(float width, float height);
    
    void setSize(Vector3f sz);
    
    void setHoverFunction(void (*function)());
    
    void setClickFunction(void (*function)());
    
    // Returns true if the mouse is over the button and sets isOver to the return value.
    bool mouseOver(float mouseX, float mouseY, float screenWidth, float screenHeight, float xOffset, float yOffset, float scale, SPS_ALIGNMENT alignment);
    
    void click();
    
    // Returns the depth (position.z). Helps to determine which Button to draw first if multiple overlap.
    float priority();
    
    void runOnHover();
    
    void runOnClick();
    
    void draw();
    
    void drawToScreen(float screenWidth, float screenHeight, float xOffset, float yOffset, float scale, SPS_ALIGNMENT alignment);
    
    void drawToScreenWithRotation(float screenWidth, float screenHeight, float xOffset, float yOffset, float scale, SPS_ALIGNMENT alignment, float angle);
    
    void drawToScreenUV(float screenWidth, float screenHeight, float xOffset, float yOffset, float scale, SPS_ALIGNMENT alignment, float scaleU, float scaleV, SPS_ALIGNMENT alignmentUV);
};

#endif /* defined(__Minigolf5__Button__) */


// Font and drawing text, new parser, get networking in