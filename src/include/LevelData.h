//
//  LevelData.h
//  MinigolfRendTest
//
//  Created by Chase Bradbury on 4/21/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#ifndef __MinigolfRendTest__LevelData__
#define __MinigolfRendTest__LevelData__

#include <cstdlib>
#include <iostream>
#include <list>
#include <cmath>

#include "./LevelObjects.h"

using namespace std;

class LevelData {
private:
	bool hasCup;
	bool hasTee;
	
    list<Tile*> tiles;
    Cup* cup;
    Tee* tee;
    
    float size;
    Vector3f center;
    
public:
    LevelData();
	~LevelData();
    
	// Add a new tile to the LevelData object
    void addTile(int ID, int numEdges, Vector3f* verts, int* neighbors, Vector3f normal, Vector3f* edgeNormals);
	
	// Add a tee to the LevelData object if there isn't one already
	void addTee(int ID, Vector3f vertex);
	
	// Add a cup to the LevelData object if there isn't one already
	void addCup(int ID, Vector3f vertex);
    
	// Add the size and center of the level geometry to the LevelData object
    void addSizeAndCenter(float inSize, Vector3f inCenter);
	
    
	// Return the tile of the specified ID
    Tile* getTile(int ID);
    
	// Return the pointer to the cup
	Cup* getCup();

	// Return the pointer to the tee
	Tee* getTee();
    
    // Return the size
    float getSize();
    
    // Return the center position
    Vector3f getCenter();
	
	// Return the number of tiles contained in this LevelData object
    int getNumTiles();
    
	// Print the LevelData object's contents (formatted)
	void printData();
	
	// Draw the cup (assumes the matrix is at the origin)
	void drawCup();
	
	// Draw the tee (assumes the matrix is at the origin)
	void drawTee();
};

#endif /* defined(__MinigolfRendTest__LevelData__) */
