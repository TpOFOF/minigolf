//
//  StateMachine.h
//  EngineTest
//
//  Created by Chase Bradbury on 11/24/13.
//  Copyright (c) 2013 Chase Bradbury. All rights reserved.
//

#ifndef __EngineTest__StateMachine__
#define __EngineTest__StateMachine__

#include <iostream>
#include <vector>

#include "IAction.h"
#include "ICondition.h"
#include "./InputManager.h"
#include "./UILayer.h"

using namespace std;

struct transition_t;

typedef transition_t* transitionref;

typedef struct state_t {

    //IAction* action;
    //IAction* updateAction;
	
	IAction* logicAction;
	IAction* stateAction;
	IAction* physicsAction;
	IAction* renderAction;
	IAction* hudAction;
	
	IAction* entryAction;
    IAction* exitAction;
    int numTransitions;
    
	vector<transitionref> *transitions;
	
	int stateID;
	InputManager* inputManager;
	UILayer* UI;
	
	bool isLevelState;
} state_t;

typedef state_t* stateref;



typedef struct transition_t {
    stateref target;
    IAction* action;
    ICondition* condition;
} transition_t;

stateref make_state(IAction* action, IAction* entryAction, IAction* exitAction, int id, bool isLevel);

void make_transition(stateref fromState, IAction* action, ICondition* condition, stateref toState);

class StateMachine {
public:
    stateref currentState;
	
	int currentID = -1;
    
    StateMachine() {};
    
    void setState(stateref newState) { currentState = newState; currentID = currentState->stateID;};
    
	void update();
};

#endif /* defined(__EngineTest__StateMachine__) */
