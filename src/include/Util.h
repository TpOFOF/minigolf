//
//  Util.h
//  Minigolf3
//
//  Created by Chase Bradbury on 4/29/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#ifndef __Minigolf3__Util__
#define __Minigolf3__Util__

#include <iostream>
#include <cstring>
#include <string>
//#include <GLUT/glut.h>
//#include "./GL/glut.h"
#include "./glut.fwd.h"
#include "./Vector3f.h"
#include "SPSMacros.h"

void draw_axis();

Vector3f rayPlaneCollisionPoint(Vector3f ls, Vector3f lv, Vector3f ps, Vector3f pn);
float rayPlaneCollisionDist(Vector3f ls, Vector3f lv, Vector3f ps, Vector3f pn);
float pointPlaneDist(Vector3f pt, Vector3f ps, Vector3f pn);
float planeYPos(Vector3f p, Vector3f n, float x, float z);

void drawText(float x, float y, void *font, int fontSize, const char* string, Vector3f color, SPS_ALIGNMENT alignment);

char* concat(char* string1, char* string2);

int spsCharToInt(char input);

char spsIntToChar(int input);

void saveHighscore(char* filename, char* profile, int score);

void displayHighscores(char* filename, int numScores);

#endif /* defined(__Minigolf3__Util__) */
