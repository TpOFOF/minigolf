//
//  SPSMacros.h
//  Minigolf5
//
//  Created by Chase Bradbury on 6/3/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#ifndef Minigolf5_SPSMacros_h
#define Minigolf5_SPSMacros_h

#define SPS_ALIGNMENT_FIXED 0
#define SPS_ALIGNMENT_TOPLEFT 1
#define SPS_ALIGNMENT_TOP 2
#define SPS_ALIGNMENT_TOPRIGHT 3
#define SPS_ALIGNMENT_LEFT 4
#define SPS_ALIGNMENT_CENTER 5
#define SPS_ALIGNMENT_RIGHT 6
#define SPS_ALIGNMENT_BOTTOMLEFT 7
#define SPS_ALIGNMENT_BOTTOM 8
#define SPS_ALIGNMENT_BOTTOMRIGHT 9
#define SPS_ALIGNMENT_FREE 10

typedef int SPS_ALIGNMENT;

static float SPS_GLOBAL_0 = 0;
static float SPS_GLOBAL_1 = 1;


#define SPS_KEY_NULL '\0'
#define SPS_KEY_0 '0'
#define SPS_KEY_1 '1'
#define SPS_KEY_2 '2'
#define SPS_KEY_3 '3'
#define SPS_KEY_4 '4'
#define SPS_KEY_5 '5'
#define SPS_KEY_6 '6'
#define SPS_KEY_7 '7'
#define SPS_KEY_8 '8'
#define SPS_KEY_9 '9'

#define SPS_KEY_A 'A'
#define SPS_KEY_B 'B'
#define SPS_KEY_C 'C'
#define SPS_KEY_D 'D'
#define SPS_KEY_E 'E'
#define SPS_KEY_F 'F'
#define SPS_KEY_G 'G'
#define SPS_KEY_H 'H'
#define SPS_KEY_I 'I'
#define SPS_KEY_J 'J'
#define SPS_KEY_K 'K'
#define SPS_KEY_L 'L'
#define SPS_KEY_M 'M'
#define SPS_KEY_N 'N'
#define SPS_KEY_O 'O'
#define SPS_KEY_P 'P'
#define SPS_KEY_Q 'Q'
#define SPS_KEY_R 'R'
#define SPS_KEY_S 'S'
#define SPS_KEY_T 'T'
#define SPS_KEY_U 'U'
#define SPS_KEY_V 'V'
#define SPS_KEY_W 'W'
#define SPS_KEY_X 'X'
#define SPS_KEY_Y 'Y'
#define SPS_KEY_Z 'Z'

typedef unsigned char SPS_KEY;

#define SPS_KEYSTATE_DOWN 0
#define SPS_KEYSTATE_UP 1
#define SPS_KEYSTATE_HELD 2

typedef int SPS_KEYSTATE;

#define SPS_MOUSE_LEFT 0
#define SPS_MOUSE_MIDDLE 1
#define SPS_MOUSE_RIGHT 2

typedef int SPS_MOUSE;

#endif
