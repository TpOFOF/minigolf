//
//  InputManager.h
//  Minigolf5
//
//  Created by Chase Bradbury on 5/29/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#ifndef __Minigolf5__InputManager__
#define __Minigolf5__InputManager__

#include <iostream>
#include "SPSMacros.h"
#include "glut.fwd.h"
#include "Util.h"

class InputManager {
private:
	static void doNothing(){return;};
    
    void (*function_0)() = doNothing;
    void (*function_1)() = doNothing;
    void (*function_2)() = doNothing;
    void (*function_3)() = doNothing;
    void (*function_4)() = doNothing;
    void (*function_5)() = doNothing;
    void (*function_6)() = doNothing;
    void (*function_7)() = doNothing;
    void (*function_8)() = doNothing;
    void (*function_9)() = doNothing;
    
    void (*function_A)() = doNothing;
    void (*function_B)() = doNothing;
    void (*function_C)() = doNothing;
    void (*function_D)() = doNothing;
    void (*function_E)() = doNothing;
    void (*function_F)() = doNothing;
    void (*function_G)() = doNothing;
    void (*function_H)() = doNothing;
    void (*function_I)() = doNothing;
    void (*function_J)() = doNothing;
    void (*function_K)() = doNothing;
    void (*function_L)() = doNothing;
    void (*function_M)() = doNothing;
    void (*function_N)() = doNothing;
    void (*function_O)() = doNothing;
    void (*function_P)() = doNothing;
    void (*function_Q)() = doNothing;
    void (*function_R)() = doNothing;
    void (*function_S)() = doNothing;
    void (*function_T)() = doNothing;
    void (*function_U)() = doNothing;
    void (*function_V)() = doNothing;
    void (*function_W)() = doNothing;
    void (*function_X)() = doNothing;
    void (*function_Y)() = doNothing;
    void (*function_Z)() = doNothing;
    
    void (*function_down0)() = doNothing;
    void (*function_down1)() = doNothing;
    void (*function_down2)() = doNothing;
    void (*function_down3)() = doNothing;
    void (*function_down4)() = doNothing;
    void (*function_down5)() = doNothing;
    void (*function_down6)() = doNothing;
    void (*function_down7)() = doNothing;
    void (*function_down8)() = doNothing;
    void (*function_down9)() = doNothing;
    
    void (*function_downA)() = doNothing;
    void (*function_downB)() = doNothing;
    void (*function_downC)() = doNothing;
    void (*function_downD)() = doNothing;
    void (*function_downE)() = doNothing;
    void (*function_downF)() = doNothing;
    void (*function_downG)() = doNothing;
    void (*function_downH)() = doNothing;
    void (*function_downI)() = doNothing;
    void (*function_downJ)() = doNothing;
    void (*function_downK)() = doNothing;
    void (*function_downL)() = doNothing;
    void (*function_downM)() = doNothing;
    void (*function_downN)() = doNothing;
    void (*function_downO)() = doNothing;
    void (*function_downP)() = doNothing;
    void (*function_downQ)() = doNothing;
    void (*function_downR)() = doNothing;
    void (*function_downS)() = doNothing;
    void (*function_downT)() = doNothing;
    void (*function_downU)() = doNothing;
    void (*function_downV)() = doNothing;
    void (*function_downW)() = doNothing;
    void (*function_downX)() = doNothing;
    void (*function_downY)() = doNothing;
    void (*function_downZ)() = doNothing;
    
    void (*function_up0)() = doNothing;
    void (*function_up1)() = doNothing;
    void (*function_up2)() = doNothing;
    void (*function_up3)() = doNothing;
    void (*function_up4)() = doNothing;
    void (*function_up5)() = doNothing;
    void (*function_up6)() = doNothing;
    void (*function_up7)() = doNothing;
    void (*function_up8)() = doNothing;
    void (*function_up9)() = doNothing;
    
    void (*function_upA)() = doNothing;
    void (*function_upB)() = doNothing;
    void (*function_upC)() = doNothing;
    void (*function_upD)() = doNothing;
    void (*function_upE)() = doNothing;
    void (*function_upF)() = doNothing;
    void (*function_upG)() = doNothing;
    void (*function_upH)() = doNothing;
    void (*function_upI)() = doNothing;
    void (*function_upJ)() = doNothing;
    void (*function_upK)() = doNothing;
    void (*function_upL)() = doNothing;
    void (*function_upM)() = doNothing;
    void (*function_upN)() = doNothing;
    void (*function_upO)() = doNothing;
    void (*function_upP)() = doNothing;
    void (*function_upQ)() = doNothing;
    void (*function_upR)() = doNothing;
    void (*function_upS)() = doNothing;
    void (*function_upT)() = doNothing;
    void (*function_upU)() = doNothing;
    void (*function_upV)() = doNothing;
    void (*function_upW)() = doNothing;
    void (*function_upX)() = doNothing;
    void (*function_upY)() = doNothing;
    void (*function_upZ)() = doNothing;
    
    
    void (*function_mouseMove)(int x, int y);
    void (*function_mousePassive)(int x, int y);
    
    static bool keyPressed[36];
    static bool keyHeld[36];
    static bool keyReleased[36];
    static int keysNum;
    
    
    static char lastKeyPressed;
    
    static int mouseX;
    static int mouseY;
    
    static int prevMouseX;
    static int prevMouseY;
    
    static bool mousePressed;
    static SPS_MOUSE mouseButton;
	
	static bool mouseButtonPressed[3];
    
public:
    
	
	/* ***** Access functions ***** */
	
	
	// Returns whether a certain mouse button is down
	static bool isClicked(SPS_MOUSE mouseButton);
	
	// Returns the mouse's X position
	static int getMouseX();
	
	// Returns the mouse's Y position
	static int getMouseY();
	
	// Return the mouse's X position in the previous frame
	static int getMouseXPrev();
	
	// Return the mouse's Y position in the previous frame
	static int getMouseYPrev();
	
	// Returns the last key pressed
    static SPS_KEY getLastKeyPressed();
    
    // Returns whether a certain key is pressed
	static bool isPressed(SPS_KEY key);
	
	
	
	
	
	/* ***** Manager/ callback functions ***** */
    
	
    // Sets the key function of the given key to the function pointer.
    void setKeyFunction(SPS_KEY key, SPS_KEYSTATE state, void (*function)());
    
    // Runs the key function of the given key.
    void doKeyFunction(SPS_KEY key, SPS_KEYSTATE state);
    
    // Calls the non-static member functions associated with the pressed buttons
    void callInputUpdates();
    
    // Callback function for key pressed
    static void cb_keyDown(unsigned char key, int x, int y);
    
    // Callback function for key released
    static void cb_keyUp(unsigned char key, int x, int y);
    
    // Callback function for mouse movement while pressed
    static void cb_mouseMove(int x, int y);
    
    // Callback function for mouse movement while not pressed
    static void cb_mousePassive(int x, int y);
    
    // Callback function for mouse click
    static void cb_mouseClick(int button, int state, int x, int y);
};

#endif /* defined(__Minigolf5__InputManager__) */
