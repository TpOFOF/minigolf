//
//  UILayer.h
//  Minigolf5
//
//  Created by Chase Bradbury on 5/29/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#ifndef __Minigolf5__UILayer__
#define __Minigolf5__UILayer__

#include <iostream>
#include <vector>
#include "Button.h"
#include "Util.h"

// Alignment constants (for use in addElement())
#define UI_FIX_LEFT 0
#define UI_FIX_RIGHT 2
#define UI_FIX_CENTER 1
#define UI_FIX_BOTTOM 0
#define UI_FIX_TOP 2

typedef int UI_ALIGNMENT;


struct UIElement {
    Button* button;
    float x;
    float y;
    float scale;
	SPS_ALIGNMENT alignment;
    float* u;
    float* v;
    float maxU;
    float maxV;
    SPS_ALIGNMENT alignmentUV;
	bool visible;
};

struct UIText {
    char* text;
    float x;
    float y;
    int* var;
	char* varText;
	SPS_ALIGNMENT alignment;
	bool visible;
};

void setUp2D();
void setUp3D();

class UILayer {
private:
    static int windowWidth;
    static int windowHeight;
    static float fov;
    
    vector<UIElement> elements;
    int numElems;
    
    vector<UIText> texts;
    int numText;
    
    void* textFont;
    int fontSize;
    Vector3f fontColor;
    
public:
    //UILayer();
    
    UILayer(float fovAngle);
    
    void setWindowSize(int w, int h);
    
    static int getWindowWidth();
    
    static int getWindowHeight();
    
    static void setWidth(int width);
    
    static void setHeight(int height);
    
    static void setFOV(float angle);
    
    static void reshape(int w, int h);
    
    void setFont(void* font, int size, Vector3f color);
    
    int addElement(Button* element, float screenPosX, float screenPosY, float scale, SPS_ALIGNMENT alignment);
    
    int addElement(Button* element, float x, float y, float scale, SPS_ALIGNMENT alignment, float* refU, float* refV, float maxU, float maxV, SPS_ALIGNMENT alignmentUV);
    
    int addElement(Image* element, float screenPosX, float screenPosY, float scale, SPS_ALIGNMENT alignment);
    
    int addElement(Image* element, float x, float y, float scale, SPS_ALIGNMENT alignment, float* refU, float* refV, float maxU, float maxV, SPS_ALIGNMENT alignmentUV);
    
    int addElement(UIElement element);
    
    int addElement(char* text1, char* text2, float screenPosX, float screenPosY, SPS_ALIGNMENT alignment);
    
    int addElement(char* text, int* var, float screenPosX, float screenPosY, SPS_ALIGNMENT alignment);
    
    void (*getClickFunction(int x, int y))();
    
    void click(int x, int y);
    
    void update(int x, int y);
    
    void draw();
	
	void moveElements();
	
	void hideText(int text);
	
	void showText(int text);
	
	void hideElement(int element);
	
	void showElement(int element);
};

#endif /* defined(__Minigolf5__UILayer__) */
