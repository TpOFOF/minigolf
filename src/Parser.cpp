#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include "include/Parser.h"

using namespace std;


// Constructor (without string)
Parser::Parser(){
	fn = NULL;
};


// Constructor (with string)
Parser::Parser(char* s){
	fn = s;
};


// Destructor
Parser::~Parser(){
};


// Parse the level data and return a LevelData object
LevelData* Parser::loadLevel(){
	bool isWarning = false;
	bool catchWarnings = true;
	
	if(strcmp(fn, "") == 0){
		cout << "ERROR: Parser.cpp: loadLevel(): Filename is blank" << endl;
		exit(1);
		//return(NULL);
	}
	
	cout << "Loading level from file: " << fn << "... " << endl;
	
	// Instantiate the LevelData object
	int totalLines = -1;
	int validLines = 0;
	LevelData* LD = new LevelData();
	bool isTee = false;
	bool isCup = false;
    float sizeX = 0;
    float sizeZ = 0;
    float minX = 0;
    float minY = 0;
    float minZ = 0;
    float maxX = 0;
    float maxY = 0;
    float maxZ = 0;
    Vector3f center;
	
	string str;
	// Open the file containing level data
	ifstream infile;
	infile.open(fn);
    if(infile.fail()){
        cout << "ERROR: Parser: loadLevel(): Could not open level file." << endl;
        exit(1);
    }
	
	// Populate the LevelData object with tiles
	while(!infile.eof()){
		getline(infile, str);
		
		int id = -1;
		Vector3f* verts = NULL;
		totalLines ++;
		
/*		
tile tile-ID n v1,x v1,y v1,z ... vn,x vn,y vn,z neighbour1 ... neighbourn
*/
		// If the stream reaches eof, break
		if(str.compare("") == '\0') break;
		
		string objType = "";
		
		// Get the type of the object and id from the string
		istringstream iss(str);
		if (!(iss >> objType >> id)){
			isWarning = true;
			cout << "WARNING: Parser: loadLevel(): Invalid object type or id on line " << totalLines << " [Skipping line]" << endl;
			continue; 
		}
		
		if(id < 1){
			isWarning = true;
			cout << "WARNING: Parser: loadLevel(): Invalid tile id on line " << totalLines << " [Skipping tile]" << endl;
			continue; 
		}
		
		int type = -1;
		if(objType.compare("tile") == 0) type = 0;
		else if(objType.compare("tee") == 0) type = 1;
		else if(objType.compare("cup") == 0) type = 2;
		else{
			isWarning = true;
			cout << "WARNING: Parser: loadLevel(): Invalid object type on line " << totalLines << " [Skipping line]" << endl;
			continue;
			//return(NULL);
		}
		
		// Depending on the object type, parse the line and add to the LevelData object
		switch(type){
			case 0:
			{
				int edges = 0;
				if (!(iss >> edges)){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Invalid/ missing edge count on line " << totalLines << " [Skipping tile]" << endl;
					continue;
					//exit(1); 
				}
				if(edges < 3){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Invalid edge count on line " << totalLines << " [Skipping tile]" << endl;
					continue;
					//exit(1);
				}
				
				// Initialize vertices' structs
				verts = (Vector3f*)malloc(sizeof(Vector3f)*edges);
				for(int i = 0; i < edges; i++){
					verts[i].x = 0;
					verts[i].y = 0;
					verts[i].z = 0;
				}
				
				// Fill the vertices' structs with appropriate data
				bool validVerts = true;
				for(int i = 0; i < edges; i++){
					if(!(iss >> verts[i].x >> verts[i].y >> verts[i].z)){
						isWarning = true;
						cout << "WARNING: Parser: loadLevel(): Invalid set of vertices on line " << totalLines << " [Skipping tile]" << endl;
						free(verts);
						validVerts = false;
						break;
						//exit(1);
					}
                    // Keep track minimum and maximum x and z values of points
                    if (verts[i].x > maxX) maxX = verts[i].x;
                    if (verts[i].x < minX) minX = verts[i].x;
                    if (verts[i].y > maxY) maxY = verts[i].y;
                    if (verts[i].y < minY) minY = verts[i].y;
                    if (verts[i].z > maxZ) maxZ = verts[i].z;
                    if (verts[i].z < minZ) minZ = verts[i].z;
				}
				if(!validVerts) continue;
				
				
				 // Calculate normal
                Vector3f normal = normalize((verts[1] - verts[0]) * (verts[2] - verts[0]));
                
				
				// Initialize the neighbors list
				bool validNeighbors = true;
				int* neighbors = (int*)malloc(sizeof(int)*edges);
				for(int i = 0; i < edges; i++)
					neighbors[i] = 0;
				
				// Fill the neighbors list
				for(int i = 0; i < edges; i++){
					if(!(iss >> neighbors[i])){
						isWarning = true;
						cout << "WARNING: Parser: loadLevel(): Invalid set of neighbors on line " << totalLines << " [Skipping tile]" << endl;
						free(verts);
						free(neighbors);
						validNeighbors = false;
						break;
						//exit(1);
					}
				}
				if(!validNeighbors) continue;
				
				// Initialize edge normals
                Vector3f* edgeNormals = (Vector3f*)malloc(sizeof(Vector3f)*edges);
                for(int i = 0; i < edges; i++)
					edgeNormals[i] = Vector3f(0, 0, 0);
                
                // Calculate edge normals (inward-facing if CCW)
                for(int i = 0; i < edges; i++){
                    Vector3f top = verts[i];
                    top.y += 0.1;
                    if (i < edges - 1) {
                        edgeNormals[i] = normalize(((top - verts[i]) * (verts[i + 1] - verts[i])));
                    } else{
                        edgeNormals[i] = normalize((top - verts[i]) * (verts[0] - verts[i]));
                    }
                }
				
				// Assuming all of the above information was valid, add the tile to the LevelData
				LD->addTile(id, edges, verts, neighbors, normal, edgeNormals);
				break;
			}
			case 1:
			{
				// Ensure this isn't a duplicate tee
				if(isTee){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Duplicate tee. [Ignoring]" << endl;
					continue;
				}
				verts = (Vector3f*)malloc(sizeof(Vector3f)); // malloc a single Vector3f
				verts[0].x = 0;
				verts[0].y = 0;
				verts[0].z = 0;
				if(!(iss >> verts[0].x >> verts[0].y >> verts[0].z)){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Invalid set of vertices on line " << totalLines << " [Skipping tee]" << endl;
					free(verts);
					continue;
					//exit(1);
				}
				LD->addTee(id, verts[0]); // Only pass an id and Vector3f
				isTee = true;
				break;
			}
			case 2: 
			{
				if(isCup){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Duplicate cup. [Ignoring]" << endl;
					continue;
				}
				verts = (Vector3f*)malloc(sizeof(Vector3f)); // malloc a single Vector3f
				verts[0].x = 0;
				verts[0].y = 0;
				verts[0].z = 0;
				if(!(iss >> verts[0].x >> verts[0].y >> verts[0].z)){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Invalid set of vertices on line " << totalLines << " [Skipping cup]" << endl;
					free(verts);
					continue;
					//exit(1);
				}
				LD->addCup(id, verts[0]); // Only pass an id and Vector3f
				isCup = true;
				break;
			}
		}
		
		validLines ++;
	}
	
    // Calculate size of level and centerpoint of level. Store in LevelData
    center.x = (minX + maxX)/2;
    center.y = maxY;
    center.z = (minZ + maxZ)/2;
    sizeX = maxX - minX;
    sizeZ = maxZ - minZ;
    if (sizeX > sizeZ) LD->addSizeAndCenter(sizeX, center);
    else LD->addSizeAndCenter(sizeZ, center);
    
	// If conditions have been met, return the LevelData object
	if(validLines >= 3 && isCup && isTee && (!isWarning || !catchWarnings)){
		return(LD);
	} else {
		if(validLines < 3) cout << "ERROR: Parser: loadLevel(): Level file has too few valid lines" << endl;
		if(!isCup) cout << "ERROR: Parser: loadLevel(): Level missing a cup" << endl;
		if(!isTee) cout << "ERROR: Parser: loadLevel(): Level missing a tee" << endl;
		if(isWarning) cout << "ERROR: All warnings must be cleared before level is valid" << endl;
		delete(LD);
		exit(1);
		//return(NULL);
	}
};


// Parse the level data from a string and return a LevelData object
LevelData* Parser::loadLevel(char* s){
	// Set the filename and return the value of loadLevel()
	fn = s;
	return(loadLevel());
};












/* ****************************************************************** */
// For refactoring:
// Functionally, the below functions only differ by the method of parsing and the return



/*

// Parse the level data and return a LevelData object
LevelData** Parser::loadLevelsFromFile(int* numLevels){
	bool isWarning = false;
	bool catchWarnings = true;
	
	if(strcmp(fn, "") == 0){
		cout << "ERROR: Parser.cpp: loadLevel(): Filename is blank" << endl;
		exit(1);
		//return(NULL);
	}
	
	cout << "Loading all levels from file: " << fn << "... " << endl;
	
	// Instantiate the LevelData object
	int totalLines = -1;
	int validLines = 0;
	LevelData* LD = new LevelData();
	bool isTee = false;
	bool isCup = false;
    float sizeX = 0;
    float sizeZ = 0;
    float minX = 0;
    float minY = 0;
    float minZ = 0;
    float maxX = 0;
    float maxY = 0;
    float maxZ = 0;
    Vector3f center;
	
	string str;
	// Open the file containing level data
	ifstream infile;
	infile.open(fn);
    if(infile.fail()){
        cout << "ERROR: Parser: loadLevel(): Could not open level file." << endl;
        exit(1);
    }
	
	// Populate the LevelData object with tiles
	while(!infile.eof()){
		getline(infile, str);
		
		int id = -1;
		Vector3f* verts = NULL;
		totalLines ++;
		
///*		
//tile tile-ID n v1,x v1,y v1,z ... vn,x vn,y vn,z neighbour1 ... neighbourn
//* /
		// If the stream reaches a new line (hopefully indicating eof), break
		if(str.compare("") == '\0') break;
		
		string objType = "";
		
		// Get the type of the object and id from the string
		istringstream iss(str);
		if (!(iss >> objType >> id)){
			isWarning = true;
			cout << "WARNING: Parser: loadLevel(): Invalid object type or id on line " << totalLines << " [Skipping line]" << endl;
			continue; 
		}
		
		if(id < 1){
			isWarning = true;
			cout << "WARNING: Parser: loadLevel(): Invalid tile id on line " << totalLines << " [Skipping tile]" << endl;
			continue; 
		}
		
		int type = -1;
		if(objType.compare("tile") == 0) type = 0;
		else if(objType.compare("tee") == 0) type = 1;
		else if(objType.compare("cup") == 0) type = 2;
		else{
			isWarning = true;
			cout << "WARNING: Parser: loadLevel(): Invalid object type on line " << totalLines << " [Skipping line]" << endl;
			continue;
			//return(NULL);
		}
		
		// Depending on the object type, parse the line and add to the LevelData object
		switch(type){
			case 0:
			{
				int edges = 0;
				if (!(iss >> edges)){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Invalid/ missing edge count on line " << totalLines << " [Skipping tile]" << endl;
					continue;
					//exit(1); 
				}
				if(edges < 3){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Invalid edge count on line " << totalLines << " [Skipping tile]" << endl;
					continue;
					//exit(1);
				}
				
				// Initialize vertices' structs
				verts = (Vector3f*)malloc(sizeof(Vector3f)*edges);
				for(int i = 0; i < edges; i++){
					verts[i].x = 0;
					verts[i].y = 0;
					verts[i].z = 0;
				}
				
				// Fill the vertices' structs with appropriate data
				bool validVerts = true;
				for(int i = 0; i < edges; i++){
					if(!(iss >> verts[i].x >> verts[i].y >> verts[i].z)){
						isWarning = true;
						cout << "WARNING: Parser: loadLevel(): Invalid set of vertices on line " << totalLines << " [Skipping tile]" << endl;
						free(verts);
						validVerts = false;
						break;
						//exit(1);
					}
                    // Keep track minimum and maximum x and z values of points
                    if (verts[i].x > maxX) maxX = verts[i].x;
                    if (verts[i].x < minX) minX = verts[i].x;
                    if (verts[i].y > maxY) maxY = verts[i].y;
                    if (verts[i].y < minY) minY = verts[i].y;
                    if (verts[i].z > maxZ) maxZ = verts[i].z;
                    if (verts[i].z < minZ) minZ = verts[i].z;
				}
				if(!validVerts) continue;
				
				
				 // Calculate normal
                Vector3f normal = normalize((verts[1] - verts[0]) * (verts[2] - verts[0]));
                
				
				// Initialize the neighbors list
				bool validNeighbors = true;
				int* neighbors = (int*)malloc(sizeof(int)*edges);
				for(int i = 0; i < edges; i++)
					neighbors[i] = 0;
				
				// Fill the neighbors list
				for(int i = 0; i < edges; i++){
					if(!(iss >> neighbors[i])){
						isWarning = true;
						cout << "WARNING: Parser: loadLevel(): Invalid set of neighbors on line " << totalLines << " [Skipping tile]" << endl;
						free(verts);
						free(neighbors);
						validNeighbors = false;
						break;
						//exit(1);
					}
				}
				if(!validNeighbors) continue;
				
				// Initialize edge normals
                Vector3f* edgeNormals = (Vector3f*)malloc(sizeof(Vector3f)*edges);
                for(int i = 0; i < edges; i++)
					edgeNormals[i] = Vector3f(0, 0, 0);
                
                // Calculate edge normals (inward-facing if CCW)
                for(int i = 0; i < edges; i++){
                    Vector3f top = verts[i];
                    top.y += 0.1;
                    if (i < edges - 1) {
                        edgeNormals[i] = normalize(((top - verts[i]) * (verts[i + 1] - verts[i])));
                    } else{
                        edgeNormals[i] = normalize((top - verts[i]) * (verts[0] - verts[i]));
                    }
                }
				
				// Assuming all of the above information was valid, add the tile to the LevelData
				LD->addTile(id, edges, verts, neighbors, normal, edgeNormals);
				break;
			}
			case 1:
			{
				// Ensure this isn't a duplicate tee
				if(isTee){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Duplicate tee. [Ignoring]" << endl;
					continue;
				}
				verts = (Vector3f*)malloc(sizeof(Vector3f)); // malloc a single Vector3f
				verts[0].x = 0;
				verts[0].y = 0;
				verts[0].z = 0;
				if(!(iss >> verts[0].x >> verts[0].y >> verts[0].z)){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Invalid set of vertices on line " << totalLines << " [Skipping tee]" << endl;
					free(verts);
					continue;
					//exit(1);
				}
				LD->addTee(id, verts[0]); // Only pass an id and Vector3f
				isTee = true;
				break;
			}
			case 2: 
			{
				if(isCup){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Duplicate cup. [Ignoring]" << endl;
					continue;
				}
				verts = (Vector3f*)malloc(sizeof(Vector3f)); // malloc a single Vector3f
				verts[0].x = 0;
				verts[0].y = 0;
				verts[0].z = 0;
				if(!(iss >> verts[0].x >> verts[0].y >> verts[0].z)){
					isWarning = true;
					cout << "WARNING: Parser: loadLevel(): Invalid set of vertices on line " << totalLines << " [Skipping cup]" << endl;
					free(verts);
					continue;
					//exit(1);
				}
				LD->addCup(id, verts[0]); // Only pass an id and Vector3f
				isCup = true;
				break;
			}
		}
		
		validLines ++;
	}
	
    // Calculate size of level and centerpoint of level. Store in LevelData
    center.x = (minX + maxX)/2;
    center.y = maxY;
    center.z = (minZ + maxZ)/2;
    sizeX = maxX - minX;
    sizeZ = maxZ - minZ;
    if (sizeX > sizeZ) LD->addSizeAndCenter(sizeX, center);
    else LD->addSizeAndCenter(sizeZ, center);
    
	// If conditions have been met, return the LevelData object
	if(validLines >= 3 && isCup && isTee && (!isWarning || !catchWarnings)){
		return(LD);
	} else {
		if(validLines < 3) cout << "ERROR: Parser: loadLevel(): Level file has too few valid lines" << endl;
		if(!isCup) cout << "ERROR: Parser: loadLevel(): Level missing a cup" << endl;
		if(!isTee) cout << "ERROR: Parser: loadLevel(): Level missing a tee" << endl;
		if(isWarning) cout << "ERROR: All warnings must be cleared before level is valid" << endl;
		delete(LD);
		exit(1);
		//return(NULL);
	}
};




// Double pointer all the way across the code
LevelData** Parser::loadAllLevels(char* s, int* numLevels){
	fn = s;
	return(loadLevelsFromFile(numLevels));
};

*/

