#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include "include/CourseParser.h"

using namespace std;


// Constructor (with string)
CourseParser::CourseParser(char* s){
	fn = s;
	numLevels = 0;
	levels = vector<Level*>();
};


// Destructor
CourseParser::~CourseParser(){
};


// Parse the level data and return an array of Level objects
Level** CourseParser::getLevels(){
	bool catchWarnings = true;
	
	if(strcmp(fn, "") == 0){
		cout << "ERROR: CourseParser.cpp: getLevels(): Filename is blank" << endl;
		exit(1);
		//return(NULL);
	}
	
	
	// Open the file containing levels' data
	ifstream infile;
	infile.open(fn);
    if(infile.fail()){
        cout << "ERROR: CourseParser: loadLevel(): Could not open course file." << endl;
        exit(1);
    }
	
	//cout << "Loading course from file: " << fn << "... " << endl;
	
	if(infile.eof()){
		cout << "ERROR: CourseParser::getLevels(): could not load from course file." << endl;
		exit(1);
	}
	
	string str;
	string courseBuf; // Read the first word (which should be "course")
	//string courseName;
	int numHoles = 0;
	
	bool readingLevel = false;
	
	getline(infile, str);
	istringstream fileIss(str);
	if (!(fileIss >> courseBuf >> courseName >> numHoles)){
		cout << "ERROR: CourseParser: getLevels(): Invalid course declaration." << endl;
		exit(1);
	}
	
	if(courseBuf.compare("course") != 0){
		cout << "ERROR: CourseParser::getLevels(): Invalid course declaration." << endl;
		exit(1);
	}
	
	if(courseName.compare("") == 0){
		cout << "WARNING: CourseParser::getLevels(): Course name is blank." << endl;
		courseName = "Untitled_Course";
	}
	
	// Set the number of levels
	numLevels = numHoles;
	
	// Create a new quote-delimited string stream to get the course name from
	stringstream qStream(str);
	string courseNameBuf;
	if(!getline(qStream, courseNameBuf, '"')){
		cout << "ERROR: CourseParser: getLevels(): Problem loading course name." << endl;
		courseName = "Untitled_Course";
	}
	if(!(getline(qStream, courseName, '"'))){
		cout << "ERROR: CourseParser::getLevels(): Problem loading course name." << endl;
		courseName = "Untitled_Course";
	}
	
	
	
	// For each hole, read until EOF or (ideally) "end_hole", create a level for it, and add to levels
	for(int j = 0; j < numHoles; j++){
		bool isWarning = false;
		//cout << "Iterating... " << j << endl;
	
		// Instantiate a LevelData object
		int totalLines = -1;
		int validLines = 0;
		LevelData* LD = new LevelData();
		bool isTee = false;
		bool isCup = false;
		float sizeX = 0;
		float sizeZ = 0;
		float minX = 0;
		float minY = 0;
		float minZ = 0;
		float maxX = 0;
		float maxY = -100;
		float maxZ = 0;
		Vector3f center;
		string levelName;
		int par = 3;
		
		// Populate the LevelData object with tiles
		while(!infile.eof()){
			getline(infile, str);
			// While the line is not "end_hole", treat it as part of the course
	
	
			// If the stream reaches eof or "end_hole", break
			if(str.compare("\0") == 0){
				//cout << "Skipping line." << endl;
				continue;
			}
			
			if(str.compare("") == '\0' || str.compare("end_hole") == 0){
				readingLevel = false;
				//cout << " ...Ending read" << endl;
				break;
			}
			
			if(str.compare("begin_hole") == 0){
				if(readingLevel){
					cout << "WARNING: Hole " << j << " was not properly ended." << endl;
					continue;
				}
				readingLevel = true;
				//cout << "Beginning level read..." << endl;
				continue;
			}
			
			// If between an "end_hole" and a "continue_hole", skip the line
			if(!readingLevel){
				//cout << "Skipping line." << endl;
				continue;
			}
			
			
			
			// Otherwise, parse as a normal LevelData would be parsed
			
			int id = -1;
			Vector3f* verts = NULL;
			totalLines ++;
			string objType = "";
			
			// Get the type of the object and id from the string
			istringstream iss(str);
			if (!(iss >> objType)){
				isWarning = true;
				cout << "WARNING: CourseParser::getLevels(): Invalid object type on line " << totalLines << " [Skipping line]" << endl;
				continue; 
			}
			
			if(objType.compare("name") == 0){
				// Create a new quote-delimited string stream to get the level name from
				stringstream quoteStream(str);
				string nameBuf;
				if(!getline(quoteStream, nameBuf, '"')){
					cout << "ERROR: CourseParser: getLevels(): Problem loading hole name." << endl;
					levelName = "Untitled_Hole";
					continue;
				}
				if(!(getline(quoteStream, levelName, '"'))){
					cout << "ERROR: CourseParser::getLevels(): Problem loading hole name." << endl;
					levelName = "Untitled_Hole";
					continue;
				}
				continue;
			}
			
			if(objType.compare("par") == 0){
				if(!(iss >> par)){
					cout << "WARNING: CourseParser::getLevels(): Invalid par" << endl;
					par = 3;
				}
				continue;
			}
			
			
			if(!(iss >> id)){
				isWarning = true;
				cout << "WARNING: CourseParser::getLevels(): Missing tile id on line " << totalLines << " [Skipping tile]" << endl;
				continue;
			}
			
			if(id < 1){
				isWarning = true;
				cout << "WARNING: CourseParser::getLevels(): Invalid tile id on line " << totalLines << " [Skipping tile]" << endl;
				continue; 
			}
			
			int type = -1;
			if(objType.compare("tile") == 0) type = 0;
			else if(objType.compare("tee") == 0) type = 1;
			else if(objType.compare("cup") == 0) type = 2;
			else{
				isWarning = true;
				cout << "WARNING: Parser: loadLevel(): Invalid object type on line " << totalLines << " [Skipping line]" << endl;
				continue;
			}
			
			// Depending on the object type, parse the line and add to the LevelData object
			switch(type){
				case 0:
				{
					int edges = 0;
					if (!(iss >> edges)){
						isWarning = true;
						cout << "WARNING: Parser: loadLevel(): Invalid/ missing edge count on line " << totalLines << " [Skipping tile]" << endl;
						continue;
					}
					if(edges < 3){
						isWarning = true;
						cout << "WARNING: Parser: loadLevel(): Invalid edge count on line " << totalLines << " [Skipping tile]" << endl;
						continue;
					}
					
					// Initialize vertices' structs
					verts = (Vector3f*)malloc(sizeof(Vector3f)*edges);
					for(int i = 0; i < edges; i++){
						verts[i].x = 0;
						verts[i].y = 0;
						verts[i].z = 0;
					}
					
					// Fill the vertices' structs with appropriate data
					bool validVerts = true;
					for(int i = 0; i < edges; i++){
						if(!(iss >> verts[i].x >> verts[i].y >> verts[i].z)){
							isWarning = true;
							cout << "WARNING: Parser: loadLevel(): Invalid set of vertices on line " << totalLines << " [Skipping tile]" << endl;
							free(verts);
							validVerts = false;
							break;
						}
						// Keep track minimum and maximum x and z values of points
						if (verts[i].x > maxX) maxX = verts[i].x;
						if (verts[i].x < minX) minX = verts[i].x;
						if (verts[i].y > maxY) maxY = verts[i].y;
						if (verts[i].y < minY) minY = verts[i].y;
						if (verts[i].z > maxZ) maxZ = verts[i].z;
						if (verts[i].z < minZ) minZ = verts[i].z;
					}
					if(!validVerts) continue;
					
					
					 // Calculate normal
					Vector3f normal = normalize((verts[1] - verts[0]) * (verts[2] - verts[0]));
					
					
					// Initialize the neighbors list
					bool validNeighbors = true;
					int* neighbors = (int*)malloc(sizeof(int)*edges);
					for(int i = 0; i < edges; i++)
						neighbors[i] = 0;
					
					// Fill the neighbors list
					for(int i = 0; i < edges; i++){
						if(!(iss >> neighbors[i])){
							isWarning = true;
							cout << "WARNING: Parser: loadLevel(): Invalid set of neighbors on line " << totalLines << " [Skipping tile]" << endl;
							free(verts);
							free(neighbors);
							validNeighbors = false;
							break;
							//exit(1);
						}
					}
					if(!validNeighbors) continue;
					
					// Initialize edge normals
					Vector3f* edgeNormals = (Vector3f*)malloc(sizeof(Vector3f)*edges);
					for(int i = 0; i < edges; i++)
						edgeNormals[i] = Vector3f(0, 0, 0);
					
					// Calculate edge normals (inward-facing if CCW)
					for(int i = 0; i < edges; i++){
						Vector3f top = verts[i];
						top.y += 0.1;
						if (i < edges - 1) {
							edgeNormals[i] = normalize(((top - verts[i]) * (verts[i + 1] - verts[i])));
						} else{
							edgeNormals[i] = normalize((top - verts[i]) * (verts[0] - verts[i]));
						}
					}
					
					// Assuming all of the above information was valid, add the tile to the LevelData
					LD->addTile(id, edges, verts, neighbors, normal, edgeNormals);
					break;
				}
				case 1:
				{
					// Ensure this isn't a duplicate tee
					if(isTee){
						isWarning = true;
						cout << "WARNING: Parser: loadLevel(): Duplicate tee. [Ignoring]" << endl;
						continue;
					}
					verts = (Vector3f*)malloc(sizeof(Vector3f)); // malloc a single Vector3f
					verts[0].x = 0;
					verts[0].y = 0;
					verts[0].z = 0;
					if(!(iss >> verts[0].x >> verts[0].y >> verts[0].z)){
						isWarning = true;
						cout << "WARNING: Parser: loadLevel(): Invalid set of vertices on line " << totalLines << " [Skipping tee]" << endl;
						free(verts);
						continue;
						//exit(1);
					}
					LD->addTee(id, verts[0]); // Only pass an id and Vector3f
					isTee = true;
					break;
				}
				case 2: 
				{
					if(isCup){
						isWarning = true;
						cout << "WARNING: Parser: loadLevel(): Duplicate cup. [Ignoring]" << endl;
						continue;
					}
					verts = (Vector3f*)malloc(sizeof(Vector3f)); // malloc a single Vector3f
					verts[0].x = 0;
					verts[0].y = 0;
					verts[0].z = 0;
					if(!(iss >> verts[0].x >> verts[0].y >> verts[0].z)){
						isWarning = true;
						cout << "WARNING: Parser: loadLevel(): Invalid set of vertices on line " << totalLines << " [Skipping cup]" << endl;
						free(verts);
						continue;
						//exit(1);
					}
					LD->addCup(id, verts[0]); // Only pass an id and Vector3f
					isCup = true;
					break;
				}
			}
			
			validLines ++;
		}
		
		// Calculate size of level and centerpoint of level. Store in LevelData
		center.x = (minX + maxX)/2;
		center.y = maxY;
		center.z = (minZ + maxZ)/2;
		sizeX = maxX - minX;
		sizeZ = maxZ - minZ;
		if (sizeX > sizeZ) LD->addSizeAndCenter(sizeX, center);
		else LD->addSizeAndCenter(sizeZ, center);
		
		// If conditions have been met, return the LevelData object
		if(validLines >= 3 && isCup && isTee && (!isWarning || !catchWarnings)){
			
			// Add Level to levels vector
			Level* L = new Level(LD);
			L->levelName = levelName;
			L->par = par;
			levels.push_back(L);
		
		} else {
			if(validLines < 3) cout << "WARNING: Parser: loadLevel(): Hole " << j << " has too few valid lines" << endl;
			if(!isCup) cout << "WARNING: Parser: loadLevel(): Hole " << j << " missing a cup" << endl;
			if(!isTee) cout << "WARNING: Parser: loadLevel(): Hole " << j << " missing a tee" << endl;
			if(isWarning) cout << "WARNING: All warnings must be cleared before hole " << j << " is valid" << endl;
			delete(LD);
			
			// Decrease the total number of levels and don't add the level to the vector
			numLevels --;
			continue;
		}
	
	
	
	}
	
	//cout << "Correctly loaded " << numLevels  << "/" << numHoles << " holes." << endl;
	
	// After iterating through every hole, return the array of levels
	if(numHoles > 0){
		Level** levelsArray = (Level**)malloc(numLevels * sizeof(Level*));
		for(int i = 0; i < numLevels; i++){
			levelsArray[i] = levels[i];
		}
		return(levelsArray);
	} else {
		cout << "ERROR: CourseParser::getLevels(): No levels could be loaded." << endl;
		exit(1);
	}
};


// Return the number of levels in the course (should always be called after getLevels)
int CourseParser::getNumLevels(){
	return(numLevels);
}
