//
//  main.cpp
//  MinigolfRendTest
//
//  Created by Chase Bradbury on 4/18/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#include <cstdio>
#include <ctime>
#include <iostream>
#include <math.h>
//#include <GLUT/glut.h>
//#include "include/GL/glut.h"
#include "include/glut.fwd.h"
#include "include/IAction.h"
#include "include/ICondition.h"
#include "include/Level.h"
#include "include/StateMachine.h"
#include "include/Util.h"
#include "include/Button.h"
#include "include/Image.h"
#include "include/UILayer.h"

using namespace std;

#define PI 3.14159265


Level* currentLevel;
int currentLevelNum = 1;
bool inLevel = false;

int mouseXPrev;
int mouseYPrev;

float lightPosition[3];
float diffuseColour[3];
float ambientColour[4];


// Declare club variables
float clubCharge;
bool clubCharging;

// Declare camera variables
Vector3f camP;
Vector3f camR; 
float camZoom;
int camUnlock;
bool overheadCam;
Vector3f overheadPos = Vector3f(0, 0, 0);
float near = .01;
float far = 10;

// Declare input variables
bool isCamW = false;
bool isCamA = false;
bool isCamS = false;
bool isCamD = false;
bool isCamX = false;
bool isCamZ = false;
bool isLightI = false;
bool isLightJ = false;
bool isLightK = false;
bool isLightL = false;
bool isLightO = false;
bool isLightU = false;

bool isMenuE = false;
bool isMenuQ = false;
bool isNotMenuE = true;
bool isNotMenuQ = true;

bool freezePhysics = false;


StateMachine* FSM = new StateMachine();

int numLevels;
char** levelNames;

int screenWidth = 500;
int screenHeight = 500;

int strokes = 0;

//**************** Test buttons. DELETE WHEN DONE ***********
Image* testImage = new Image((char*)"./assets/testbutton.spi");
Image* testImageHover = new Image((char*)"./assets/testbuttonhover.spi");
Image* testImageClick = new Image((char*)"./assets/testbuttonclick.spi");
Button* testButton = new Button(testImage, testImageHover, testImageClick);
Button* testButton2 = new Button(testImage, testImageHover, testImageClick);
Button* testButton3 = new Button(testImage, testImageHover, testImageClick);
Button* testButton4 = new Button(testImage, testImageHover, testImageClick);

Image* testChar = new Image((char*)"./assets/arialA.spi");

Image* powerBarUncharged = new Image((char*)"./assets/PowerBarUncharged.spi");
Image* powerBarCharged = new Image((char*)"./assets/PowerBarCharged.spi");
Image* arrow = new Image((char*)"./assets/Arrow.spi");


UILayer* UI;

UILayer* testUI = new UILayer(90.0);

Image* titleScreen = new Image((char*)"./assets/MiniGolfBig.spi");

Button* titleButton = new Button(titleScreen, titleScreen, titleScreen);

UILayer* titleUI = new UILayer(90.0);
//*****************


//***************** Test input ***************
InputManager* input = new InputManager();


//***************


// Set the camera settings
bool initializeCamera(){
	camP = Vector3f(0, 0, 0);
	camR = Vector3f(45, 0, 0);
	camZoom = 2;
	camUnlock = 0;
	overheadCam = false;
	return(true);
}

// Reset the controls (for when starting/ unpausing level)
void resetControls(){
	clubCharge = 0;
	clubCharging = false;

	isCamW = false;
	isCamA = false;
	isCamS = false;
	isCamD = false;
	isCamX = false;
	isCamZ = false;
	isLightI = false;
	isLightJ = false;
	isLightK = false;
	isLightL = false;
	isLightO = false;
	isLightU = false;

	return;
}



// When locked, slide the camera towards being centered on the ball
void slewCamera(){
	Vector3f ballPos = currentLevel->getBallPosition();
	Vector3f camPos = (ballPos - camP) / 7 + camP;
	camP = Vector3f(camPos.x, camPos.y, camPos.z);
}


// When camera movement buttons are held down, exponentially
//     increase the delay before the camera re-locks based on 
//     how long buttons were held down
void unlockCamera(){
	if(camUnlock == 0)
		camUnlock = 4;
	camUnlock *= 2;
	if(camUnlock > 60)
		camUnlock = 60; // Maximum frames before camera re-snaps
}

void moveCamUp() {
    unlockCamera();
    camP.x += .1*cos((camR.y)*PI/180);
    camP.z += .1*sin((camR.y)*PI/180);
}

void moveCamDown() {
    unlockCamera();
    camP.x += .1*cos((camR.y + 180)*PI/180);
    camP.z += .1*sin((camR.y + 180)*PI/180);
}

void moveCamLeft() {
    unlockCamera();
    camP.x += .1*cos((camR.y + 90)*PI/180);
    camP.z += .1*sin((camR.y + 90)*PI/180);
}

void moveCamRight() {
    unlockCamera();
    camP.x += .1*cos((camR.y - 90)*PI/180);
    camP.z += .1*sin((camR.y - 90)*PI/180);
}

void pressedE() {
    if (isNotMenuE) {
        isMenuE = true;
        isNotMenuE = false;
    }
}

void pressedQ() {
    if (isNotMenuQ) {
        isMenuQ = true;
        isNotMenuQ = false;
    }
}


// Move the camera position (called every frame)
void moveCamera(){
	if(isCamW){
		unlockCamera();
		camP.x += .1*cos((camR.y)*PI/180);
		camP.z += .1*sin((camR.y)*PI/180);
	}
	if(isCamS){
		unlockCamera();
		camP.x += .1*cos((camR.y + 180)*PI/180);
		camP.z += .1*sin((camR.y + 180)*PI/180);
	}
	if(isCamA){
		unlockCamera();
		camP.x += .1*cos((camR.y + 90)*PI/180);
		camP.z += .1*sin((camR.y + 90)*PI/180);
	}
	if(isCamD){
		unlockCamera();
		camP.x += .1*cos((camR.y - 90)*PI/180);
		camP.z += .1*sin((camR.y - 90)*PI/180);
	}
	if(isCamZ){
		// If camera has been unlocked already, maintain the unlocked state
		if(camUnlock > 0)
			unlockCamera();
		if(camZoom < 5)
			camZoom += 0.1;
	}
	if(isCamX){
		// If camera has been unlocked already, maintain the unlocked state
		if(camUnlock > 0)
			unlockCamera();
		if(camZoom > .3)
			camZoom -= 0.1;
	}
	
	if(camUnlock == 0)
		slewCamera();
	else camUnlock --;
}


// Move the light position (called every frame)
void moveLight(){
	if(isLightI){
		lightPosition[0] += .1*cos((camR.y)*PI/180);
		lightPosition[2] += .1*sin((camR.y)*PI/180);
	}
	if(isLightK){
		lightPosition[0] += .1*cos((camR.y + 180)*PI/180);
		lightPosition[2] += .1*sin((camR.y + 180)*PI/180);
	}
	if(isLightJ){
		lightPosition[0] += .1*cos((camR.y + 90)*PI/180);
		lightPosition[2] += .1*sin((camR.y + 90)*PI/180);
	}
	if(isLightL){
		lightPosition[0] += .1*cos((camR.y - 90)*PI/180);
		lightPosition[2] += .1*sin((camR.y - 90)*PI/180);
	}
	if(isLightO){
		lightPosition[1] += 0.1;
	}
	if(isLightU){
		lightPosition[1] -= 0.1;
	}
}


// Apply the club's charge level to affect the acceleration of the ball
void swingClub(){
	float power = clubCharge;
	Vector3f swingDir = Vector3f(cos(PI*camR.y/180), 0, sin(PI*camR.y/180));
	//cout << "camR.y: " << (int)(camR.y)%360 << "    Power: " << power << endl;
	swingDir *= power;
	currentLevel->addForceToBall(swingDir);
    ++strokes;
}


// Charge the club until it reaches the maximum value (arbitrarily 5)
void chargeClub(){
	if(clubCharge < 1.4)
		clubCharge += .04;
	else clubCharge = 1.4;
}

void setUp3D() {
    float aspect = UILayer::getWindowWidth()/UILayer::getWindowHeight();
	glViewport(0, 0, UILayer::getWindowWidth(), UILayer::getWindowHeight());
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90.0, aspect, 0.01, 10000); // necessary to preserve aspect ratio
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void setUp2D() {
    glMatrixMode(GL_PROJECTION);
    //glPushMatrix();
    glLoadIdentity();
    //glOrtho(0.0, screenWidth, 0.0, screenHeight, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glDisable(GL_CULL_FACE);
    
    glClear(GL_DEPTH_BUFFER_BIT);
    
    //glTranslatef(-screenWidth, -screenHeight, 0);
    //glScalef(1/screenWidth, 1/screenHeight, 1);
}



/* Game State actions */
// Draws the current level
void displayLevel() {
	// Set voidean so that ingame controls only work in the level
	inLevel = true;
    
    
	setUp3D();
    //glPushMatrix();
    
	cout << "Display Level" << endl;
    if(!freezePhysics)
		currentLevel->updateBall();
	moveLight();
	if(!overheadCam){
		moveCamera();
		
		if(clubCharging)
			chargeClub();
	}
    
    glScalef(-1, 1, 1);
    
    // Move the camera
    if (overheadCam) {
        glRotatef(90, 1, 0, 0);
        glTranslatef(-overheadPos.x, -overheadPos.y, -overheadPos.z);
    } else {
        glTranslatef(0, 0, -camZoom);
        
        glRotatef(camR.x, 1, 0, 0);
        glRotatef(camR.y + 90, 0, 1, 0);
        
        glTranslatef(-camP.x, -camP.y, -camP.z);
    }
	//draw_axis();
	
	// Draw an object to represent the source of the light
	currentLevel->drawLight(lightPosition[0], lightPosition[1], lightPosition[2]);
    
    
    //glLightfv(GL_LIGHT1, GL_AMBIENT, ambientColour);
    
	glEnable(GL_LIGHTING);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	
	currentLevel->drawLevel();
	
    glDisable(GL_LIGHTING);
	
    

	
	//setUp2D();
	
	
	// Load the identity matrix and clear the depth buffer in order to draw the HUD
    glLoadIdentity();
	glClear(GL_DEPTH_BUFFER_BIT);
    
	// HUD drawing area
    //testUI->draw();
	
	
    return;
}

void enterMainMenu() {
    UI = titleUI;
}

void enterLevel() {
    UI = testUI;
    resetControls();
}

void exitLevel(){
	// Use to disable ingame controls when not in the level
	inLevel = false;
    return;
}

void displayMainMenu() {
    cout << "Main Menu" << endl;
    //setUp2D();
    //testButton->draw();
    return;
}

void displayPauseMenu() {
    cout << "Pause Screen" << endl;
    return;
}

void displayWinScreen() {
    cout << "Win Screen" << endl;
    return;
}

void nullAction() {
    return;
}

void quit() {
    cout << "Exiting" << endl;
    exit(0);
    return;
}

void restartLevel() {
    initializeCamera();
    currentLevel->restartBall();
    strokes = 0;
    return;
}

void nextLevel() {
	cout << "NEXT LEVEL" << endl;
    if (currentLevelNum < numLevels - 1) {
        ++currentLevelNum;
    } else {
        currentLevelNum = 1;
    }
    //Level level = Level(levelNames[currentLevelNum], &overheadPos);
    currentLevel = new Level(levelNames[currentLevelNum], &overheadPos); //&level;
    restartLevel();
    return;
}



/* Game State conditions */

bool ePressed() {
    if (isMenuE) {
        isMenuE = false;
        return true;
    }
    return false;
}

bool qPressed() {
    if (isMenuQ) {
        isMenuQ = false;
        return true;
    }
    return false;
}

bool checkWin() {
    return currentLevel->hasWon();
}


void displayMain()
{
    
}


void cb_display() {
    static clock_t previous_clock = 0;
    static clock_t ms_since_last = 0;
    clock_t now = clock() / (CLOCKS_PER_SEC / 1000);
    ms_since_last = now - previous_clock;
    if (ms_since_last < 20) {
        return;
    }
    previous_clock = now;
	
	input->callInputUpdates();
		
	/* Test function for movement (replace with ball's physics function) */
    //currentLevel->applyPhysics();
	//currentLevel->applyAVP();
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_LIGHT0);
	glLoadIdentity();
    
    setUp3D();
    //glScalef(-1, 1, 1);
    
    
    
	// Run state machine update and call the state's actions
	
	IAction* actions = FSM->update();
    
	for (int i = 0; i < 4; ++i) {
        actions[i].doAction();
    }
	
    //displayLevel();
    
    
    glLoadIdentity();
	glClear(GL_DEPTH_BUFFER_BIT);
    
    glDisable(GL_LIGHTING);
    
    //glPushAttrib( GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_ENABLE_BIT );
    
    glEnable( GL_BLEND );
    //glEnable( GL_DEPTH_TEST );
    //glDepthMask( GL_FALSE );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    
    setUp2D();
    
    UI->draw();
    
    glClear(GL_DEPTH_BUFFER_BIT);
    
    //drawText(0, .5, GLUT_BITMAP_TIMES_ROMAN_24, 24, "test", Vector3f(1, 1, 1), SPS_ALIGNMENT_TOPLEFT);
	
    //glDepthRange(near, far);
    
    
    //glDepthMask( GL_TRUE );
    glDisable( GL_BLEND );
    
	glFlush();
	glutSwapBuffers(); // for smoother animation
}

void cb_idle() {
	glutPostRedisplay();
}

void cb_reshape(int w, int h) {
    screenWidth = w;
	screenHeight = h;
	UILayer::reshape(w,h);
	testUI->moveElements();
	titleUI->moveElements();
}

void cb_mouseclick(int button, int state, int x, int y) {
    mouseXPrev = x;
    mouseYPrev = y;
    
    
    UI->click(x, y);
}

void cb_mousemove(int x, int y) {
    if(inLevel) {
		camR.y -= x - mouseXPrev;
		camR.x += y - mouseYPrev;
	}
    mouseXPrev = x;
    mouseYPrev = y;
    UI->update(x, y);
}

void cb_mousemovepassive(int x, int y) {
    UI->update(x, y);
}

void cb_keyboard(unsigned char key, int x, int y) {
    switch(key) {
		case 'w': if(inLevel) isCamW = true; break;
		case 'a': if(inLevel) isCamA = true; break;
		case 's': if(inLevel) isCamS = true; break;
		case 'd': if(inLevel) isCamD = true; break;
		case 'x': if(inLevel) isCamX = true; break;
		case 'z': if(inLevel) isCamZ = true; break;
		
		case 'i': if(inLevel) isLightI = true; break;
		case 'j': if(inLevel) isLightJ = true; break;
		case 'k': if(inLevel) isLightK = true; break;
		case 'l': if(inLevel) isLightL = true; break;
		case 'o': if(inLevel) isLightO = true; break;
		case 'u': if(inLevel) isLightU = true; break;
            
        case 'v': if(inLevel) {
					  overheadCam = true;
					  clubCharge = 0;
					  clubCharging = false; 
				  } 
				  break;
		
		case 'c': if(inLevel) clubCharging = true; break;
		
		case 'f': freezePhysics = true; currentLevel->updateBall(); break;
		case 'r': freezePhysics = false; break;
		
        case 'e': if (isNotMenuE) {
                isMenuE = true;
                isNotMenuE = false;
            } break;
		case 'q': if (isNotMenuQ) {
            isMenuQ = true;
            isNotMenuQ = false;
        } break;
			break;
    }
}


// Key release callback function
void cb_keyboardUp(unsigned char key, int x, int y) {

	// Deactivate the keys as they are released
	switch(key) {
		case 'w': isCamW = false; break;
		case 'a': isCamA = false; break;
		case 's': isCamS = false; break;
		case 'd': isCamD = false; break;
		case 'x': isCamX = false; break;
		case 'z': isCamZ = false; break;
		
		case 'i': isLightI = false; break;
		case 'j': isLightJ = false; break;
		case 'k': isLightK = false; break;
		case 'l': isLightL = false; break;
		case 'o': isLightO = false; break;
		case 'u': isLightU = false; break;
            
        case 'v': overheadCam = false; break;
		
		case 'c': if(clubCharging && inLevel) swingClub();
				clubCharge = 0;
				clubCharging = false;
            break;
            
        case 'e': isNotMenuE = true; break;
        case 'q': isNotMenuQ = true; break;
	}
};





//int main(int argc, char * argv[])
{
	if(argc < 2){
		cout << "USAGE: MiniGolf levelFile" << endl;
		exit(1);
	}

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutCreateWindow("Minigolf");
	glutIgnoreKeyRepeat(false);
	glEnable(GL_TEXTURE_2D);
    
    //glewInit();
    //init();
    //glutDisplayFunc(displayMain);
    //glutKeyboardFunc(keyboard);
    
    numLevels = argc;
    
    levelNames = argv;
    
    //glutMainLoop();
    
    // insert code here...
    //for (int i = 0; i < argc; ++i) {
    Level level = Level(levelNames[currentLevelNum], &overheadPos);
    currentLevel = &level;
    
    //******* Test Button Setup ********
    //testUI->addElement(testButton, .1, .25, .1, SPS_ALIGNMENT_TOPLEFT, &clubCharge, &SPS_GLOBAL_1, 1.4, 1, SPS_ALIGNMENT_TOPLEFT);
    //testUI->addElement(testButton2, .1, .1, .1, SPS_ALIGNMENT_TOPLEFT);
    //testUI->addElement(testButton3, .8, .7, .05, SPS_ALIGNMENT_RIGHT);
    testUI->addElement(powerBarCharged, 0, 1, .3, SPS_ALIGNMENT_BOTTOMLEFT, &SPS_GLOBAL_1, &clubCharge, 1, 1.4, SPS_ALIGNMENT_BOTTOMLEFT);
    testUI->addElement(powerBarUncharged, 0, 1, .3, SPS_ALIGNMENT_BOTTOMLEFT);
    testUI->addElement(arrow, .5, .5, .1, SPS_ALIGNMENT_TOP);
    
    
    //testUI->addElement(testChar, .5, .5, 1, SPS_ALIGNMENT_FIXED);
    
    titleUI->addElement(titleScreen, 0.5, 0, 1, SPS_ALIGNMENT_TOP);
    
    testUI->addElement((char*)"Coursename: ", (char*)"Test", 0, 0.1, SPS_ALIGNMENT_TOPLEFT);
    testUI->addElement((char*)"Level: ", (char*)"Test", 0, 0.2, SPS_ALIGNMENT_TOPLEFT);
    testUI->addElement((char*)"Par: ", (char*)"4", 0, 0.3, SPS_ALIGNMENT_TOPLEFT);
    testUI->addElement((char*)"Strokes: ", &strokes, 0, 0.4, SPS_ALIGNMENT_TOPLEFT);
    
    UILayer::setWidth(screenWidth);
    UILayer::setHeight(screenHeight);
    
    UI = titleUI;
    
    testUI->setFont(GLUT_BITMAP_TIMES_ROMAN_24, 24, Vector3f(1, 1, 1));
    
    //******* Setting up key functions
    
    input->setKeyFunction(SPS_KEY_W, SPS_KEYSTATE_HELD, moveCamUp);
    input->setKeyFunction(SPS_KEY_A, SPS_KEYSTATE_HELD, moveCamLeft);
    input->setKeyFunction(SPS_KEY_S, SPS_KEYSTATE_HELD, moveCamDown);
    input->setKeyFunction(SPS_KEY_D, SPS_KEYSTATE_HELD, moveCamRight);
    input->setKeyFunction(SPS_KEY_Q, SPS_KEYSTATE_UP, pressedQ);
    input->setKeyFunction(SPS_KEY_E, SPS_KEYSTATE_DOWN, pressedE);
    
    
    
    
    /*int levelRet = currentLevel->run();
     if (levelRet) {
     return levelRet;
     }*/
    //}
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glEnable (GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_TEXTURE_2D);
    
    // Create OpenGL texture
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    
    lightPosition[0] = 1.5;
	lightPosition[1] = 3;
	lightPosition[2] = -1.5;
	//lightPosition[3] = 1;
    
    ambientColour[0] = .8;
	ambientColour[1] = .1;
	ambientColour[2] = .1;
	ambientColour[3] = 500;
    
    
    glutDisplayFunc(cb_display);
    glutKeyboardFunc(InputManager::cb_keyDown);
	glutKeyboardUpFunc(InputManager::cb_keyUp);
    glutMotionFunc(cb_mousemove);
    glutPassiveMotionFunc(cb_mousemovepassive);
    glutMouseFunc(cb_mouseclick);
	//glutReshapeFunc(cb_reshape);
    glutReshapeFunc(UILayer::reshape);
	glutIdleFunc(cb_idle);
    
    


    
    // Create State Machine
    IAction noAction;
    noAction.setAction(nullAction);
    
    IAction levelAction;
    levelAction.setAction(displayLevel);
	
	IAction exitLevelAction;
	exitLevelAction.setAction(exitLevel);
    
    IAction mainMenuAction;
    mainMenuAction.setAction(displayMainMenu);
    
    IAction pauseMenuAction;
    pauseMenuAction.setAction(displayPauseMenu);
	
	IAction enterMainAction;
	enterMainAction.setAction(enterMainMenu);
    
    IAction unPauseAction;
	unPauseAction.setAction(enterLevel);
    
    IAction winScreenAction;
    winScreenAction.setAction(displayWinScreen);
    
    IAction quitAction;
    quitAction.setAction(quit);
    
    IAction restartAction;
    restartAction.setAction(restartLevel);
    
    IAction nextLevelAction;
    nextLevelAction.setAction(nextLevel);
    
    ICondition eCondition;
    eCondition.setTest(ePressed);
    
    ICondition qCondition;
    qCondition.setTest(qPressed);
    
    ICondition winCondition;
    winCondition.setTest(checkWin);
    
    stateref mainMenuState = make_state(mainMenuAction, enterMainAction, noAction, 2, 0);
    stateref levelState = make_state(levelAction, unPauseAction, exitLevelAction, 3, 1);
    stateref pauseMenuState = make_state(pauseMenuAction, noAction, noAction, 2, 2);
    stateref winScreenState = make_state(winScreenAction, noAction, noAction, 2, 3);
    stateref quitState = make_state(quitAction, noAction, noAction, 0, 4);
    
    make_transition(levelState, noAction, eCondition, pauseMenuState);
    make_transition(levelState, noAction, qCondition, mainMenuState);
    make_transition(levelState, noAction, winCondition, winScreenState);
    make_transition(mainMenuState, restartAction, eCondition, levelState);
    make_transition(mainMenuState, noAction, qCondition, quitState);
    make_transition(pauseMenuState, noAction, eCondition, levelState);
    make_transition(pauseMenuState, noAction, qCondition, mainMenuState);
    make_transition(winScreenState, nextLevelAction, eCondition, levelState);
    make_transition(winScreenState, nextLevelAction, qCondition, mainMenuState);
    
    FSM->setState(mainMenuState);
    
    
    glClearColor(0,0,0,0);
    
    glutMainLoop();
    
    return 0;
}

 void setupGL() {
    
}