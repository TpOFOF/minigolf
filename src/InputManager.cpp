//
//  InputManager.cpp
//  Minigolf5
//
//  Created by Chase Bradbury on 5/29/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#include <cstdlib>
#include <iostream>
#include "include/InputManager.h"

using namespace std;

int InputManager::keysNum = 36;
bool InputManager::keyPressed[36] = {};
bool InputManager::keyHeld[36] = {};
bool InputManager::keyReleased[36] = {};
bool InputManager::mouseButtonPressed[3] = {false, false, false};


char InputManager::lastKeyPressed = SPS_KEY_NULL;

int InputManager::mouseX = 0;
int InputManager::mouseY = 0;

int InputManager::prevMouseX = 0;
int InputManager::prevMouseY = 0;

bool InputManager::mousePressed = false;

SPS_MOUSE InputManager::mouseButton = SPS_MOUSE_LEFT;





// Returns whether a certain mouse button is down
bool InputManager::isClicked(SPS_MOUSE mouseButton){
	return(mouseButtonPressed[mouseButton]);
}

// Returns the mouse's X position
int InputManager::getMouseX(){
	return(mouseX);
}

// Returns the mouse's Y position
int InputManager::getMouseY(){
	return(mouseY);
}

// Return the mouse's X position in the previous frame
int InputManager::getMouseXPrev(){
	return(prevMouseX);
}

// Return the mouse's Y position in the previous frame
int InputManager::getMouseYPrev(){
	return(prevMouseY);
}

// Returns the last key pressed
SPS_KEY InputManager::getLastKeyPressed() {
    return lastKeyPressed;
}

// Returns whether a certain key is pressed
bool InputManager::isPressed(SPS_KEY key) {
    key = toupper(key);
    return keyHeld[spsCharToInt(key)];
}







// Sets the key function of the given key to the function pointer.
void InputManager::setKeyFunction(SPS_KEY key, SPS_KEYSTATE state, void (*function)()) {
    key = toupper(key);
    switch (state) {
        case 0:
            switch(key) {
                case '0': function_down0 = function; break;
                case '1': function_down1 = function; break;
                case '2': function_down2 = function; break;
                case '3': function_down3 = function; break;
                case '4': function_down4 = function; break;
                case '5': function_down5 = function; break;
                case '6': function_down6 = function; break;
                case '7': function_down7 = function; break;
                case '8': function_down8 = function; break;
                case '9': function_down9 = function; break;
                    
                case 'A': function_downA = function; break;
                case 'B': function_downB = function; break;
                case 'C': function_downC = function; break;
                case 'D': function_downD = function; break;
                case 'E': function_downE = function; break;
                case 'F': function_downF = function; break;
                case 'G': function_downG = function; break;
                case 'H': function_downH = function; break;
                case 'I': function_downI = function; break;
                case 'J': function_downJ = function; break;
                case 'K': function_downK = function; break;
                case 'L': function_downL = function; break;
                case 'M': function_downM = function; break;
                case 'N': function_downN = function; break;
                case 'O': function_downO = function; break;
                case 'P': function_downP = function; break;
                case 'Q': function_downQ = function; break;
                case 'R': function_downR = function; break;
                case 'S': function_downS = function; break;
                case 'T': function_downT = function; break;
                case 'U': function_downU = function; break;
                case 'V': function_downV = function; break;
                case 'W': function_downW = function; break;
                case 'X': function_downX = function; break;
                case 'Y': function_downY = function; break;
                case 'Z': function_downZ = function; break;
            }
            break;
            
        case 1:
            switch(key) {
                case '0': function_up0 = function; break;
                case '1': function_up1 = function; break;
                case '2': function_up2 = function; break;
                case '3': function_up3 = function; break;
                case '4': function_up4 = function; break;
                case '5': function_up5 = function; break;
                case '6': function_up6 = function; break;
                case '7': function_up7 = function; break;
                case '8': function_up8 = function; break;
                case '9': function_up9 = function; break;
                    
                case 'A': function_upA = function; break;
                case 'B': function_upB = function; break;
                case 'C': function_upC = function; break;
                case 'D': function_upD = function; break;
                case 'E': function_upE = function; break;
                case 'F': function_upF = function; break;
                case 'G': function_upG = function; break;
                case 'H': function_upH = function; break;
                case 'I': function_upI = function; break;
                case 'J': function_upJ = function; break;
                case 'K': function_upK = function; break;
                case 'L': function_upL = function; break;
                case 'M': function_upM = function; break;
                case 'N': function_upN = function; break;
                case 'O': function_upO = function; break;
                case 'P': function_upP = function; break;
                case 'Q': function_upQ = function; break;
                case 'R': function_upR = function; break;
                case 'S': function_upS = function; break;
                case 'T': function_upT = function; break;
                case 'U': function_upU = function; break;
                case 'V': function_upV = function; break;
                case 'W': function_upW = function; break;
                case 'X': function_upX = function; break;
                case 'Y': function_upY = function; break;
                case 'Z': function_upZ = function; break;
            }
            break;
            
        case 2:
            switch(key) {
                case '0': function_0 = function; break;
                case '1': function_1 = function; break;
                case '2': function_2 = function; break;
                case '3': function_3 = function; break;
                case '4': function_4 = function; break;
                case '5': function_5 = function; break;
                case '6': function_6 = function; break;
                case '7': function_7 = function; break;
                case '8': function_8 = function; break;
                case '9': function_9 = function; break;
                    
                case 'A': function_A = function; break;
                case 'B': function_B = function; break;
                case 'C': function_C = function; break;
                case 'D': function_D = function; break;
                case 'E': function_E = function; break;
                case 'F': function_F = function; break;
                case 'G': function_G = function; break;
                case 'H': function_H = function; break;
                case 'I': function_I = function; break;
                case 'J': function_J = function; break;
                case 'K': function_K = function; break;
                case 'L': function_L = function; break;
                case 'M': function_M = function; break;
                case 'N': function_N = function; break;
                case 'O': function_O = function; break;
                case 'P': function_P = function; break;
                case 'Q': function_Q = function; break;
                case 'R': function_R = function; break;
                case 'S': function_S = function; break;
                case 'T': function_T = function; break;
                case 'U': function_U = function; break;
                case 'V': function_V = function; break;
                case 'W': function_W = function; break;
                case 'X': function_X = function; break;
                case 'Y': function_Y = function; break;
                case 'Z': function_Z = function; break;
            }
            break;
            
        default:
            break;
    }
    
}

// Runs the key function of the given key.
void InputManager::doKeyFunction(SPS_KEY key, SPS_KEYSTATE state) {
    key = toupper(key);
    switch (state) {
        case 0:
            switch(key) {
                case '0': function_down0(); keyPressed[0] = false; break;
                case '1': function_down1(); keyPressed[1] = false; break;
                case '2': function_down2(); keyPressed[2] = false; break;
                case '3': function_down3(); keyPressed[3] = false; break;
                case '4': function_down4(); keyPressed[4] = false; break;
                case '5': function_down5(); keyPressed[5] = false; break;
                case '6': function_down6(); keyPressed[6] = false; break;
                case '7': function_down7(); keyPressed[7] = false; break;
                case '8': function_down8(); keyPressed[8] = false; break;
                case '9': function_down9(); keyPressed[9] = false; break;
                    
                case 'A': function_downA(); keyPressed[10] = false; break;
                case 'B': function_downB(); keyPressed[11] = false; break;
                case 'C': function_downC(); keyPressed[12] = false; break;
                case 'D': function_downD(); keyPressed[13] = false; break;
                case 'E': function_downE(); keyPressed[14] = false; break;
                case 'F': function_downF(); keyPressed[15] = false; break;
                case 'G': function_downG(); keyPressed[16] = false; break;
                case 'H': function_downH(); keyPressed[17] = false; break;
                case 'I': function_downI(); keyPressed[18] = false; break;
                case 'J': function_downJ(); keyPressed[19] = false; break;
                case 'K': function_downK(); keyPressed[20] = false; break;
                case 'L': function_downL(); keyPressed[21] = false; break;
                case 'M': function_downM(); keyPressed[22] = false; break;
                case 'N': function_downN(); keyPressed[23] = false; break;
                case 'O': function_downO(); keyPressed[24] = false; break;
                case 'P': function_downP(); keyPressed[25] = false; break;
                case 'Q': function_downQ(); keyPressed[26] = false; break;
                case 'R': function_downR(); keyPressed[27] = false; break;
                case 'S': function_downS(); keyPressed[28] = false; break;
                case 'T': function_downT(); keyPressed[29] = false; break;
                case 'U': function_downU(); keyPressed[30] = false; break;
                case 'V': function_downV(); keyPressed[31] = false; break;
                case 'W': function_downW(); keyPressed[32] = false; break;
                case 'X': function_downX(); keyPressed[33] = false; break;
                case 'Y': function_downY(); keyPressed[34] = false; break;
                case 'Z': function_downZ(); keyPressed[35] = false; break;
            }
            break;
            
        case 1:
            switch(key) {
                case '0': function_up0(); break;
                case '1': function_up1(); break;
                case '2': function_up2(); break;
                case '3': function_up3(); break;
                case '4': function_up4(); break;
                case '5': function_up5(); break;
                case '6': function_up6(); break;
                case '7': function_up7(); break;
                case '8': function_up8(); break;
                case '9': function_up9(); break;
                    
                case 'A': function_upA(); break;
                case 'B': function_upB(); break;
                case 'C': function_upC(); break;
                case 'D': function_upD(); break;
                case 'E': function_upE(); break;
                case 'F': function_upF(); break;
                case 'G': function_upG(); break;
                case 'H': function_upH(); break;
                case 'I': function_upI(); break;
                case 'J': function_upJ(); break;
                case 'K': function_upK(); break;
                case 'L': function_upL(); break;
                case 'M': function_upM(); break;
                case 'N': function_upN(); break;
                case 'O': function_upO(); break;
                case 'P': function_upP(); break;
                case 'Q': function_upQ(); break;
                case 'R': function_upR(); break;
                case 'S': function_upS(); break;
                case 'T': function_upT(); break;
                case 'U': function_upU(); break;
                case 'V': function_upV(); break;
                case 'W': function_upW(); break;
                case 'X': function_upX(); break;
                case 'Y': function_upY(); break;
                case 'Z': function_upZ(); break;
            }
            break;
            
        case 2:
            switch(key) {
                case '0': function_0(); break;
                case '1': function_1(); break;
                case '2': function_2(); break;
                case '3': function_3(); break;
                case '4': function_4(); break;
                case '5': function_5(); break;
                case '6': function_6(); break;
                case '7': function_7(); break;
                case '8': function_8(); break;
                case '9': function_9(); break;
                    
                case 'A': function_A(); break;
                case 'B': function_B(); break;
                case 'C': function_C(); break;
                case 'D': function_D(); break;
                case 'E': function_E(); break;
                case 'F': function_F(); break;
                case 'G': function_G(); break;
                case 'H': function_H(); break;
                case 'I': function_I(); break;
                case 'J': function_J(); break;
                case 'K': function_K(); break;
                case 'L': function_L(); break;
                case 'M': function_M(); break;
                case 'N': function_N(); break;
                case 'O': function_O(); break;
                case 'P': function_P(); break;
                case 'Q': function_Q(); break;
                case 'R': function_R(); break;
                case 'S': function_S(); break;
                case 'T': function_T(); break;
                case 'U': function_U(); break;
                case 'V': function_V(); break;
                case 'W': function_W(); break;
                case 'X': function_X(); break;
                case 'Y': function_Y(); break;
                case 'Z': function_Z(); break;
            }
            break;
            
        default:
            break;
    }
}

void InputManager::callInputUpdates() {
    char tmpchar;
    for (int i = 0; i < keysNum; ++i) {
        tmpchar = spsIntToChar(i);
        if (keyPressed[i]) {
            doKeyFunction(tmpchar, 0);
            keyPressed[i] = false;
        }
        if (keyReleased[i]) {
            doKeyFunction(tmpchar, 1);
            keyReleased[i] = false;
        }
        if (keyHeld[i]) {
            doKeyFunction(tmpchar, 2);
        }
    }
    
}

//
void InputManager::cb_keyDown(unsigned char key, int x, int y) {
    key = toupper(key);
    switch (key) {
        case '0':
            keyPressed[0] = true;
            keyHeld[0] = true;
            break;
        case '1':
            keyPressed[1] = true;
            keyHeld[1] = true;
            break;
        case '2':
            keyPressed[2] = true;
            keyHeld[2] = true;
            break;
        case '3':
            keyPressed[3] = true;
            keyHeld[3] = true;
            break;
        case '4':
            keyPressed[4] = true;
            keyHeld[4] = true;
            break;
        case '5':
            keyPressed[5] = true;
            keyHeld[5] = true;
            break;
        case '6':
            keyPressed[6] = true;
            keyHeld[6] = true;
            break;
        case '7':
            keyPressed[7] = true;
            keyHeld[7] = true;
            break;
        case '8':
            keyPressed[8] = true;
            keyHeld[8] = true;
            break;
        case '9':
            keyPressed[9] = true;
            keyHeld[9] = true;
            break;
            
        case 'A':
            keyPressed[10] = true;
            keyHeld[10] = true;
            break;
        case 'B':
            keyPressed[11] = true;
            keyHeld[11] = true;
            break;
        case 'C':
            keyPressed[12] = true;
            keyHeld[12] = true;
            break;
        case 'D':
            keyPressed[13] = true;
            keyHeld[13] = true;
            break;
        case 'E':
            keyPressed[14] = true;
            keyHeld[14] = true;
            break;
        case 'F':
            keyPressed[15] = true;
            keyHeld[15] = true;
            break;
        case 'G':
            keyPressed[16] = true;
            keyHeld[16] = true;
            break;
        case 'H':
            keyPressed[17] = true;
            keyHeld[17] = true;
            break;
        case 'I':
            keyPressed[18] = true;
            keyHeld[18] = true;
            break;
        case 'J':
            keyPressed[19] = true;
            keyHeld[19] = true;
            break;
        case 'K':
            keyPressed[20] = true;
            keyHeld[20] = true;
            break;
        case 'L':
            keyPressed[21] = true;
            keyHeld[21] = true;
            break;
        case 'M':
            keyPressed[22] = true;
            keyHeld[22] = true;
            break;
        case 'N':
            keyPressed[23] = true;
            keyHeld[23] = true;
            break;
        case 'O':
            keyPressed[24] = true;
            keyHeld[24] = true;
            break;
        case 'P':
            keyPressed[25] = true;
            keyHeld[25] = true;
            break;
        case 'Q':
            keyPressed[26] = true;
            keyHeld[26] = true;
            break;
        case 'R':
            keyPressed[27] = true;
            keyHeld[27] = true;
            break;
        case 'S':
            keyPressed[28] = true;
            keyHeld[28] = true;
            break;
        case 'T':
            keyPressed[29] = true;
            keyHeld[29] = true;
            break;
        case 'U':
            keyPressed[30] = true;
            keyHeld[30] = true;
            break;
        case 'V':
            keyPressed[31] = true;
            keyHeld[31] = true;
            break;
        case 'W':
            keyPressed[32] = true;
            keyHeld[32] = true;
            break;
        case 'X':
            keyPressed[33] = true;
            keyHeld[33] = true;
            break;
        case 'Y':
            keyPressed[34] = true;
            keyHeld[34] = true;
            break;
        case 'Z':
            keyPressed[35] = true;
            keyHeld[35] = true;
            break;
            
        default:
            break;
    }
    lastKeyPressed = key;
}

//
void InputManager::cb_keyUp(unsigned char key, int x, int y) {
    key = toupper(key);
    switch (key) {
        case '0':
            keyReleased[0] = true;
            keyHeld[0] = false;
            break;
        case '1':
            keyReleased[1] = true;
            keyHeld[1] = false;
            break;
        case '2':
            keyReleased[2] = true;
            keyHeld[2] = false;
            break;
        case '3':
            keyReleased[3] = true;
            keyHeld[3] = false;
            break;
        case '4':
            keyReleased[4] = true;
            keyHeld[4] = false;
            break;
        case '5':
            keyReleased[5] = true;
            keyHeld[5] = false;
            break;
        case '6':
            keyReleased[6] = true;
            keyHeld[6] = false;
            break;
        case '7':
            keyReleased[7] = true;
            keyHeld[7] = false;
            break;
        case '8':
            keyReleased[8] = true;
            keyHeld[8] = false;
            break;
        case '9':
            keyReleased[9] = true;
            keyHeld[9] = false;
            break;
            
        case 'A':
            keyReleased[10] = true;
            keyHeld[10] = false;
            break;
        case 'B':
            keyReleased[11] = true;
            keyHeld[11] = false;
            break;
        case 'C':
            keyReleased[12] = true;
            keyHeld[12] = false;
            break;
        case 'D':
            keyReleased[13] = true;
            keyHeld[13] = false;
            break;
        case 'E':
            keyReleased[14] = true;
            keyHeld[14] = false;
            break;
        case 'F':
            keyReleased[15] = true;
            keyHeld[15] = false;
            break;
        case 'G':
            keyReleased[16] = true;
            keyHeld[16] = false;
            break;
        case 'H':
            keyReleased[17] = true;
            keyHeld[17] = false;
            break;
        case 'I':
            keyReleased[18] = true;
            keyHeld[18] = false;
            break;
        case 'J':
            keyReleased[19] = true;
            keyHeld[19] = false;
            break;
        case 'K':
            keyReleased[20] = true;
            keyHeld[20] = false;
            break;
        case 'L':
            keyReleased[21] = true;
            keyHeld[21] = false;
            break;
        case 'M':
            keyReleased[22] = true;
            keyHeld[22] = false;
            break;
        case 'N':
            keyReleased[23] = true;
            keyHeld[23] = false;
            break;
        case 'O':
            keyReleased[24] = true;
            keyHeld[24] = false;
            break;
        case 'P':
            keyReleased[25] = true;
            keyHeld[25] = false;
            break;
        case 'Q':
            keyReleased[26] = true;
            keyHeld[26] = false;
            break;
        case 'R':
            keyReleased[27] = true;
            keyHeld[27] = false;
            break;
        case 'S':
            keyReleased[28] = true;
            keyHeld[28] = false;
            break;
        case 'T':
            keyReleased[29] = true;
            keyHeld[29] = false;
            break;
        case 'U':
            keyReleased[30] = true;
            keyHeld[30] = false;
            break;
        case 'V':
            keyReleased[31] = true;
            keyHeld[31] = false;
            break;
        case 'W':
            keyReleased[32] = true;
            keyHeld[32] = false;
            break;
        case 'X':
            keyReleased[33] = true;
            keyHeld[33] = false;
            break;
        case 'Y':
            keyReleased[34] = true;
            keyHeld[34] = false;
            break;
        case 'Z':
            keyReleased[35] = true;
            keyHeld[35] = false;
            break;
            
        default:
            break;
    }
}


// Callback function for mouse movement while pressed
void InputManager::cb_mouseMove(int x, int y) {
    prevMouseX = mouseX;
    prevMouseY = mouseY;
    mouseX = x;
    mouseY = y;
}

// Callback function for mouse movement while not pressed
void InputManager::cb_mousePassive(int x, int y) {
    prevMouseX = mouseX;
    prevMouseY = mouseY;
    mouseX = x;
    mouseY = y;
}

// Callback function for mouse click
void InputManager::cb_mouseClick(int button, int state, int x, int y) {
    mousePressed = !state;
    mouseButton = button;
	
	mouseButtonPressed[button] = !state;
}