//
//  Ball.cpp
//  Minigolf3
//
//  Created by Chase Bradbury on 4/30/14.
//  Copyright (c) 2014 Chase Bradbury. All rights reserved.
//

#include "include/SphereCollider.h"


SphereCollider::SphereCollider(LevelData* data)
{
    LD = data;
}

SphereCollider::SphereCollider(LevelData* data, Vector3f initPosition)
{
	LD = data;
    position = initPosition;
}

SphereCollider::SphereCollider(LevelData* data, Vector3f initPosition, Tile* initTile)
{
	LD = data;
    position = initPosition;
    tile = initTile;
	tileID = tile->ID;
}

void SphereCollider::restart()
{
    position = LD->getTee()->position;
    tileID = LD->getTee()->tileID;
    tile = LD->getTile(tileID);
    velocity = Vector3f(0, 0, 0);
    force = Vector3f(0, 0, 0);
	inCup = false;
}

void SphereCollider::setTile(Tile* newTile)
{
    tile = newTile;
}

void SphereCollider::setTile(int newTile)
{
	tileID = newTile;
	tile = LD->getTile(tileID);
}

int SphereCollider::getTile(){
	return(tileID);
}

void SphereCollider::setRadius(float newRadius)
{
    radius = newRadius;
}

void SphereCollider::setMass(float newMass)
{
    mass = newMass;
}

void SphereCollider::addForce(Vector3f newForce)
{
    force += newForce;
}

Vector3f SphereCollider::getAcceleration()
{
    return force/mass;
}

bool SphereCollider::isInCup()
{
    return inCup;
}




void SphereCollider::update()
{
	if(tile == NULL){
		cout << "WARNING: SphereCollider::update(): tile is null" << endl;
		// exit(1);
	}
	
	
	/*
	// If the ball is moving slow enough no club force added, stop it
	if(force.length() == 0 && velocity.length() < radius / 15){
		// Additional check to attempt to make the ball stop
		if( abs(tile->floor.normal.x) < .0001 && abs(tile->floor.normal.z) < .0001 )
			return;
	} 
	*/
	
	// Add the force of gravity
	Vector3f g = Vector3f(0, -gravity, 0);
	//if(velocity.length() > radius / 25)
		addForce(g); 
	
	bool onGround = false;
	
	// If on or under the ground, add the normal force
	float planeHeight = planeYPos(tile->vertices[0], tile->floor.normal, position.x, position.z);
	if(position.y <= planeHeight + radius / 10){
		//cout << endl << "Below ground: " << position.y - (planeHeight + radius / 10) << endl;
		Vector3f n = tile->normal;
		Vector3f zero = Vector3f(0,0,0);
		// Apply the normal force (vector projection of -(gravity) onto the normal)
		Vector3f normalForce = n * (dot(n, zero - g) / n.length());
		if(velocity.length() > radius / 25)
			addForce(normalForce); // Add the force from the tile in response to gravity
		
		// If the velocity is close enough to the projection of the velocity 
		//     on the tile, consider the ball on the ground
		Vector3f projectedVelocity = velocity - project(velocity, tile->normal);
		float alignmentAngle = 180*asin( dot(normalize(velocity), normalize(tile->floor.normal)) )/PI;
		// Check the ball's velocity against an angular threshold of 5 degrees from the tile
		if(alignmentAngle < 10.0){
			if(velocity.length() > radius / 25){
				position.y = planeHeight + radius / 10;
				velocity = normalize(projectedVelocity) * velocity.length();
			}
			onGround = true;
		}
	}
	
	
	// Check if ball is in cup, inform player if they are victorious
    if ((position - LD->getCup()->position).length() < radius*2.5) {
        if ((position - LD->getCup()->position).length() < radius/1.2 && velocity.length() < .07) {
			inCup = true;
			return;
        } else {
			// If close to the cup, pull the ball towards it by a factor based on the angle distance
		//	if(velocity.length() < radius){
				Vector3f F = Vector3f(0,0,0);
				if(velocity.length() > 0)
					F = (LD->getCup()->position - position) * .01 / velocity.length();
				// Multiply based on a factor of how "not-towards the cup" the ball is moving
				//F *= (1 - abs(acos(dot(normalize(LD->getCup()->position - position), normalize(velocity))) - PI/2) / PI/2);
				//cout << "pullForce: " << F << endl;
				addForce(F);
		//	} else {
		/*
				// Make the ball bounce out of the cup in a random direction
				float theta = atan2(velocity.z, velocity.x);
				float alpha = (float)(rand()%10) * .06;
				float beta = (float)(rand()%10) * .06;
				Vector3f F = Vector3f(alpha * cos(theta + PI), -velocity.y + 6*radius, beta * .90 * sin(theta + PI));
				cout << "Adding force: " << F << endl;
				addForce(F);
			}
		*/
        
		}
    }

	
		// If the ball is moving slow enough no club force added, stop it
	if(force.length() == 0 && velocity.length() < radius / 15){
		velocity = Vector3f(0,0,0);
	} 
    
    velocity += getAcceleration();
    force.x = 0;
    force.y = 0;
    force.z = 0;

	// Recursively check for collisions with walls
	bool wallCollision = false;
	float distanceLeft = velocity.length();
	// Edge normals are inward-facing if tile vertices are CCW
	
	
	// Recursively check for intersection collisions with each wall, updating the current position
	while(distanceLeft > 0){
		// Break if the distance is small enough
		if(abs(distanceLeft) < 0.0001)
			break;
		
		
		float minDist = 1000000000;
		int minEdge = -1;
		int numWalls = tile->numEdges;
		Wall* walls = tile->walls;
		
		// Check each wall, find the closest one
		for(int i = 0; i < numWalls; i++){
			float tempDist = rayPlaneCollisionDist(position, velocity, walls[i].points[0], walls[i].normal);
			if(walls[i].solid){
				// Subtract the radius from the distance to prioritize solid walls
				//     (ball will collide with solid walls first, since the center
				//      collides with non-solid walls)
				tempDist -= radius;
			}
			// Find the minimum distance a wall is at (on this tile)
			if(tempDist < minDist && tempDist >= 0){
				// Store the index in the walls array (corresponds to neighbors array)
				minEdge = i; 
				minDist = tempDist;
			}
		}
		
		
		// If no collisions occur within the distance remaining, break the loop
		if(minDist > distanceLeft){
			// Move the ball's position if no collisions have occurred yet
			if(!wallCollision){
				position += velocity;
			}
			// Set the distance left to 0 and break the loop
			distanceLeft = 0;
			break;
		}
		wallCollision = true;
	
		// Calculate collision point with the wall
		Wall w = walls[minEdge];
        Vector3f pc;
        
        if (velocity.length() != 0){
            pc = rayPlaneCollisionPoint(position, velocity, w.points[0], w.normal);
        } else {
            pc = rayPlaneCollisionPoint(position, Vector3f(.001, .001, .001), w.points[0], w.normal);
        }
		float travelDist = (pc - position).length();
			
		// Move the ball to a new tile
		if(!w.solid){
			// Get the tile to move to
			Tile* newTile = LD->getTile(tile->neighbors[minEdge]);
			if(newTile == NULL){
				cout << "WARNING: SphereCollider::update(): Tile entered is null" << endl;
			} 

		
			// Attempt to keep better track of the tile the ball is currently on
			if(dot(pc - position, w.normal) > 0){
				newTile = tile;
			}
			
		
				// If projection of velocity onto new plane (normalized) - velocity (normalized) 
				//      has a positive y component, slow it down by a factor of the dot product
				
				Vector3f tempPos = position + velocity;
				float newTilePosY = planeYPos(newTile->vertices[0], newTile->normal, tempPos.x, tempPos.z);
				float oldTilePosY = planeYPos(tile->vertices[0], tile->normal, tempPos.x, tempPos.z);
				if(newTilePosY > oldTilePosY){
					// The ball would be travelling into the next tile
					// 		so slow the speed after adjusting the velocity
					// Calculate the redirected velocity (speed unchanged)
					Vector3f vTemp = velocity - project(velocity, newTile->normal);
					float vMag = velocity.length();
					vTemp = normalize(vTemp) * vMag;
					
					float rampAngle = 180*acos( dot(normalize(velocity), normalize(vTemp)) )/PI;
					float speedPercent = 1 - pow(rampAngle / 90, 2);
					
					
					// If the ball is not airborne, adjust its speed based on inclined plane collisions
					if(onGround){
						velocity = vTemp * speedPercent;
					}
				}
			
			distanceLeft -= travelDist;
			position = pc;
			position += normalize(velocity) * .001;
			
			// Update the current tile
			tile = newTile;
			tileID = tile->ID;
		} else {
		
			// Calculate the margin back from the wall to place the sphere
			float backDist = (radius * travelDist * (w.normal).length()) / dot(w.normal, position - pc);

			// Place the sphere a margin away from the collision point to avoid clipping
			Vector3f newPosition = pc - (normalize(velocity) * backDist);
			distanceLeft -= (newPosition - position).length();
			position = newPosition;
			velocity.reflect(w.normal);
			position += normalize(velocity) * .001;
		}
		
	}
	
	

	
	// Next check for overlap collisions with each solid wall after the distance 
	//      has been exhausted. If found, back the ball up and reflect velocity
	int numWalls = tile->numEdges;
	Wall* walls = tile->walls;
	for(int i = 0; i < numWalls; i++){
		float tempDist = pointPlaneDist(position, walls[i].points[0], walls[i].normal);
		if(walls[i].solid){
			if(tempDist >= 0 && tempDist < radius){
				position -= (normalize(walls[i].normal) * (radius - tempDist));
				velocity.reflect(walls[i].normal);
			}
			
			if(tempDist < 0 && abs(tempDist) < radius){
				position += (normalize(walls[i].normal) * (radius - abs(tempDist)));
				velocity.reflect(walls[i].normal);
			}
			
		}
	}
	
	
	
	// Apply friction to the ball's movement
	velocity *= .98;

		
    // Adhere to the plane
	
	
	// If below the plane or considered on the ground, place the ball on the plane
	planeHeight = planeHeight = planeYPos(tile->vertices[0], tile->floor.normal, position.x, position.z);
	
	if(position.y <= planeHeight + radius / 10){
		if(onGround){
			// If the ball is rolling, make sure it stays on the floor
			position.y = planeHeight + radius / 10; // Arbitrarily radius / 4 offset?
		} else {
			// If the ball was flying, make it bounce off the tile
			Vector3f bounceVelocity = reflect(velocity, tile->floor.normal);
			Vector3f normalComponent = project(bounceVelocity, tile->floor.normal);
			velocity = bounceVelocity - (normalComponent / 3);
			position.y = planeHeight + radius / 10;
		}
	}
}



void SphereCollider::draw()
{
    glColor3f(1,1,1);
	glPushMatrix();
	glTranslatef(position.x, position.y + radius, position.z);
	glutSolidSphere(radius, 20, 20);
	glPopMatrix();
}