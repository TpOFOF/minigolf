#include <cstdlib> 
#include <cstdio>
#include <ctime>
#include <iostream>
#include <math.h>
#include <string>
#include <cstring>

#include "include/glut.fwd.h"
#include "include/Game.h"

#include "include/IAction.h"
#include "include/ICondition.h"
#include "include/LevelNew.h"
#include "include/CourseParser.h"
#include "include/SphereCollider.h"
//#include "include/StateMachine.h"
#include "include/Util.h"
#include "include/Button.h"
#include "include/Image.h"
#include "include/InputManager.h"
#include "include/UILayer.h"

using namespace std;

#define PI 3.14159265


int levelState = -1;



// Static initializations of private Game variables

int Game::numStates = 0;
vector<stateref> Game::states = vector<stateref>();
StateMachine* Game::FSM = NULL;
Level** Game::levels = NULL;
Level* Game::currentLevel = NULL;
int Game::totalLevels = 0;
int Game::currentLevelNum = 0;
string Game::windowTitle = "";
UILayer* Game::UI = NULL;

Game* Game::gameObject = NULL;
bool Game::gameObjectBound = false;

Vector3f Game::camP = Vector3f(0,0,0);
Vector3f Game::camR = Vector3f(0,0,0);
Vector3f Game::overheadPos = Vector3f(0,0,0);
float Game::camZoom = 0;
bool Game::overheadCam = false;
bool Game::smoothOverheadMove = false;

int Game::screenWidth = 0;
int Game::screenHeight = 0;


IAction* Game::logicAction = new IAction();
IAction* Game::physicsAction = new IAction();
IAction* Game::renderAction = new IAction();

IAction* nullAction = new IAction();




// Static initializations of public Game variables

string Game::courseName = "Untitled_Course";
string Game::currentLevelName = "Untitled_Level";
int Game::currentLevelPar = 3;
bool Game::defaultCamera = true;









void bindGameObject(Game* G){
//	cout << "Binding game object." << endl;
	Game::gameObject = G;
	Game::gameObjectBound = true;
};


Game* Game::getGameObject(){
	if(Game::gameObject == NULL)
		cout << "WARNING: Game object is null" << endl;
	return(Game::gameObject);
};


void Game::cb_display() {
    static clock_t previous_clock = 0;
    static clock_t ms_since_last = 0;
    clock_t now = clock() / (CLOCKS_PER_SEC / 1000);
    ms_since_last = now - previous_clock;
    if (ms_since_last < 20) {
        return;
    }
    previous_clock = now;
	
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_LIGHT0);
	glLoadIdentity();
    
	// Handle input and UI
	UI = FSM->currentState->UI;
	FSM->currentState->inputManager->callInputUpdates();
	// Update the FSM and perform all relevent actions
	FSM->update();
	
	
	glFlush();
	glutSwapBuffers(); // for smoother animation
}


void Game::cb_idle() {
	glutPostRedisplay();
}


void Game::cb_reshape(int w, int h) {
    screenWidth = w;
	screenHeight = h;
	UILayer::reshape(w, h);
}


void Game::cb_mouseclick(int button, int state, int x, int y){
	InputManager::cb_mouseClick(button, state, x, y);
	FSM->currentState->UI->click(x,y);
}


void Game::cb_mousemove(int x, int y) {
	InputManager::cb_mouseMove(x, y);
	int mouseXPrev = InputManager::getMouseXPrev();
	int mouseYPrev = InputManager::getMouseYPrev();
	
	FSM->currentState->UI->update(x, y);
	
	if(defaultCamera && !overheadCam){
		if(FSM->currentState->isLevelState) {
			camR.y -= x - mouseXPrev;
			camR.x += y - mouseYPrev;
			
			// Constrain the camera's y and z values to the range [-180, 180} to help with transitions
			camR.y = fmod(camR.y, 360);
			if(camR.y > 180) camR.y -= 360;
			camR.z = fmod(camR.z, 360);
			if(camR.z > 180) camR.z -= 360;
		}
		
		// Constrain the camera's x rotation
		if(camR.x < -90) camR.x = -90;
		if(camR.x > 90) camR.x = 90;
	}
}


void Game::cb_mousepassive(int x, int y){
	InputManager::cb_mousePassive(x, y);
	FSM->currentState->UI->update(x, y);
}




void Game::addSceneControl( int sceneID, SPS_KEY key, SPS_KEYSTATE state, void (*keyFunction)() ){
	states[sceneID]->inputManager->setKeyFunction(key, state, keyFunction);
}






// Draw the UILayer object
void Game::drawUI(){
	glLoadIdentity();
	glClear(GL_DEPTH_BUFFER_BIT);
    glDisable(GL_LIGHTING);
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    //setUp3D();
    
	
	// UI is a pointer to currentState's UI object
    UI->draw();
    
    glClear(GL_DEPTH_BUFFER_BIT);
    glDisable( GL_BLEND );
}


void Game::initializeLight(){
	lightPosition[0] = 1.5;
	lightPosition[1] = 3;
	lightPosition[2] = -1.5;
	lightPosition[3] = 0;
}


void Game::initializeCamera(){
	camP = Vector3f(0, 0, 0);
	camR = Vector3f(45, 0, 0);
	overheadPos = Vector3f(0, 0, 0);
	camZoom = 2;
	overheadCam = false;
}


void Game::drawLightObject(){
	currentLevel->drawLight(lightPosition[0], lightPosition[1], lightPosition[2]);
};



void doPhysics(){
	Game::currentLevel->updateBall();
}


void doRender(){
	Game* G = Game::getGameObject();
	
	setUp3D();
	
	
	glLoadIdentity();
	glScalef(-1,1,1);

	float* lightPosition = G->lightPosition;
	Vector3f camP = Game::camP;
	Vector3f camR = Game::camR;
	Vector3f overheadPos = Game::overheadPos;
	float camZoom = Game::camZoom;
	bool overheadCam = Game::overheadCam;
	bool smoothOverheadMove = Game::smoothOverheadMove;
	bool drawLight = G->drawLight;
	
	// Move the camera to an overhead position
	if(overheadCam){
		// If set to perform the overhead movement smoothly, do so
		if(smoothOverheadMove){
			Vector3f overRot = Vector3f(90, 0, 0);
			
			G->trackCameraToPosition(overheadPos, (1.0f/4.0f));
			G->trackCameraToRotation(overRot, (1.0f/4.0f));
			G->trackCameraToZoom(3, (1.0f/3.0f));
			
			glTranslatef(0, 0, -camZoom);
			glRotatef(camR.x, 1, 0, 0);
			glRotatef(camR.y + 90, 0, 1, 0);
			glRotatef(camR.z, 0, 0, 1);
			glTranslatef(-camP.x, -camP.y, -camP.z);
		} else {
			glTranslatef(0, 0, -2);
			glRotatef(90, 1, 0, 0);
			glRotatef(camR.z, 0, 0, 1);
			glTranslatef(-overheadPos.x, -overheadPos.y, -overheadPos.z);
		}
        
	} else {
		glTranslatef(0, 0, -camZoom);
        
        glRotatef(camR.x, 1, 0, 0);
        glRotatef(camR.y + 90, 0, 1, 0);
        glRotatef(camR.z, 0, 0, 1);
		
        glTranslatef(-camP.x, -camP.y, -camP.z);
	}
	
	// Draw an object to represent the source of the light
	if(G->drawLight)
		G->drawLightObject();
	
	// Draw level
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	
	//Vector3f tempVec = Vector3f(lightPosition[0], lightPosition[1], lightPosition[2]);
	//cout << "Light position: " << tempVec << endl;
	
	//glDisable(GL_LIGHTING);
	
	Game::currentLevel->drawLevel();
	//Game::drawCurrentLevel();
	
    glDisable(GL_LIGHTING);
	
	glLoadIdentity();
	glClear(GL_DEPTH_BUFFER_BIT);
}


Game::Game(string gameName, char* courseFile){
	// Set up the FSM
	numStates = 0;
	totalLevels = 0;
	currentLevelNum = 0;
	defaultCamera = false;
	
	states = vector<stateref>();
	FSM = new StateMachine();
	
	nullAction->setAction(nullFunction);

	logicAction->setAction(nullFunction);
	physicsAction->setAction(nullFunction);
	renderAction->setAction(nullFunction);
	
	
	
	// Set the window title (and do not allow another instance of Game)
	if(windowTitle.compare("") == 0)
		windowTitle = gameName;
	else {
		cout << "ERROR: Cannot create another instance of Game class" << endl;
		exit(1);
	}
	
	// Create a CourseParser to return a list of levels
	CourseParser parser = CourseParser(courseFile);
	
	levels = parser.getLevels();
	totalLevels = parser.getNumLevels();
	
	courseName = parser.courseName;

	// Load the fist level available by default
	setLevel(0);
	
	physicsAction->setAction(doPhysics);
	renderAction->setAction(doRender);
	
	initializeCamera();
	initializeLight();
}
	

Game::~Game(){
	// Free the states
	for(int i = 0; i < numStates; i++){
		free(states[i]->transitions);
		free(states[i]);
	}
}




void Game::start(int argc, char** argv, int sWidth, int sHeight){
	if(!(Game::gameObjectBound)){
		cout << "ERROR: Game object has not been bound." << endl << "( Call bindGameObject(Game* G) before calling Game::start() )" << endl;
		exit(1);
	}
	
	char* windowName = new char[windowTitle.length() + 1];
	strcpy(windowName, windowTitle.c_str());
	
	screenWidth = sWidth;
	screenHeight = sHeight;
	
	// Set up the window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(screenWidth, screenHeight);
    glutCreateWindow(windowName);
	glutIgnoreKeyRepeat(true);
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glEnable (GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_TEXTURE_2D);
	
	// Create OpenGL texture
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	glutDisplayFunc(cb_display);
    glutKeyboardFunc(InputManager::cb_keyDown);
	glutKeyboardUpFunc(InputManager::cb_keyUp);
	// Don't call InputManager's callback immediately (for handling camera movement)
    glutMotionFunc(cb_mousemove); 
    glutPassiveMotionFunc(cb_mousepassive);
    glutMouseFunc(cb_mouseclick);
    glutReshapeFunc(cb_reshape);
	glutIdleFunc(cb_idle);
	
	glClearColor(0,0,0,0);
	
	// Set up the camera
	setUp3D();
	
	// Start the main loop.
	glutMainLoop();
}



// End the game
void Game::end(){
	exit(0);
}




void Game::addFrameLogic(void (*logicFunction)()){
	if(logicAction == NULL)
		logicAction = new IAction();
	logicAction->setAction(logicFunction);
}




// Add a state to the FSM, passing in function pointers for general game logic, state logic, and hud logic
int Game::addScene(void (*stateFunction)(), bool isLevelState){
	
	// Create the scene update to initialize the state with
	IAction* sceneAction = new IAction();
	sceneAction->setAction(stateFunction);
	
	// Create the hud action for this scene
	IAction* hudAction = new IAction();
	hudAction->setAction(drawUI);
	
	stateref tempState = make_state(sceneAction, nullAction, nullAction, numStates, isLevelState);
	
	// Account for the remaining update actions after the state has been created
	tempState->logicAction = logicAction;
	tempState->physicsAction = physicsAction;
	tempState->renderAction = renderAction;
	tempState->hudAction = hudAction;
	
	// Push the state into a vector belonging to Game
	states.push_back(tempState);
	numStates ++;
	if(numStates == 1){
		FSM->setState(tempState);
		UI = FSM->currentState->UI;
	}
	return(numStates - 1);
}


// Add a transition to scene1 that causes the FSM to move to scene2
void Game::addTransition(int scene1, int scene2, bool (*condFunction)(), void (*actFunction)()){
	if(scene1 >= numStates || scene2 >= numStates){
		cout << "ERROR: Game::addTransition(): Scene does not exist" << endl;
		exit(1);
	}
	
	IAction* act = new IAction();
	act->setAction(actFunction);
	ICondition* cond = new ICondition();
	cond->setTest(condFunction);
	
	make_transition(states[scene1], act, cond, states[scene2]);
}


// Add an action to be called upon entering a scene
void Game::addSceneEnterAction(int scene, void (*entryFunction)()){
	IAction* act = new IAction();
	act->setAction(entryFunction);
	states[scene]->entryAction = act;
}


// Add an action to be called upon exiting a scene
void Game::addSceneExitAction(int scene, void (*exitFunction)()){
	if(scene >= numStates){
		cout << "ERROR: Game::addSceneExitAction(): Scene does not exist" << endl;
		exit(1);
	}
	IAction* act = new IAction();
	act->setAction(exitFunction);
	states[scene]->exitAction = act;
}


// Return the current scene id
int Game::getCurrentScene(){
	return(FSM->currentID);
}


// Set the current scene to the scene with id s
void Game::setCurrentScene(int s){
	FSM->setState(states[s]);
}


// Return whether the current scene is set to a level scene
bool Game::isLevelScene(){
	return(FSM->currentState->isLevelState);
}


int Game::getLevel(){
	return(currentLevelNum);
}
	

void Game::setLevel(int l){
	if(l >= totalLevels){
		cout << "ERROR: Game::setLevel(int l): Level does not exist" << endl;
		exit(1);
	}
	currentLevelNum = l;
	currentLevel = levels[l];
	currentLevel->restartBall();
	
	currentLevelPar = currentLevel->par;
	currentLevelName = currentLevel->levelName;
	
	overheadCam = false;
	overheadPos = currentLevel->overheadCamPos;
}


void Game::nextLevel(){
	int nL = (getLevel() + 1) % totalLevels;
	setLevel(nL);
}


Vector3f Game::getBallPosition(){
	return(currentLevel->getBallPosition());
}


void Game::setBallPosition(Vector3f pos){
	currentLevel->setBallPosition(pos);
}


// Get the velocity of the ball
Vector3f Game::getBallVelocity(){
	return(currentLevel->getBallVelocity());
}
	
// Set the velocity of the ball directly
void Game::setBallVelocity(Vector3f vel){
	currentLevel->setBallVelocity(vel);
}


void Game::addForceToBall(Vector3f f){
	if(FSM->currentState->isLevelState)
		currentLevel->addForceToBall(f);
}


bool Game::hasWonLevel(){
	return(currentLevel->hasWon());
}



// Move the camera a specific ratio of the distance between its 
//     current position and the desired position
void Game::trackCameraToPosition(Vector3f pos, float ratio){
	// Clamp the ratio
	ratio = min(max(ratio, 0.0f), 1.0f);
	Vector3f camPos = (pos - camP) * ratio + camP;
	camP = Vector3f(camPos.x, camPos.y, camPos.z);
}


// Move the camera relative to dir
void Game::moveCamera(Vector3f dir){
	Game::camP += dir;
}


void Game::moveCameraHorizontallyRelative(float localX, float localZ){
	// Move the camera with respect to its Z axis (which it is always looking down)
	camP.x += localZ*cos((camR.y)*PI/180);
	camP.z += localZ*sin((camR.y)*PI/180);
	
	// Move the camera with respect to its local X axis
	camP.x += localX*cos((camR.y - 90)*PI/180);
	camP.z += localX*sin((camR.y - 90)*PI/180);
}


// Return the position of the camera
Vector3f Game::getCameraPosition(){
	return(camP);
}


// Rotate the camera relative a certain amount in the x, y, and z directions
void Game::rotateCamera(float xRot, float yRot, float zRot){
	camR.x += xRot;
	camR.y += yRot;
	camR.z += zRot;
	
	while(camR.x < 0) camR.x += 360;
	while(camR.y < 0) camR.y += 360;
	while(camR.z < 0) camR.z += 360;
	
	// Constrain the camera's y and z value to the range [-180, 180} to help with transitions
	camR.y = fmod(camR.y, 360);
	if(camR.y > 180) camR.y -= 360;
	camR.z = fmod(camR.z, 360);
	if(camR.z > 180) camR.z -= 360;
	
	// Constrain the camera's x rotation so it can't be upside down
	if(camR.x < -90) camR.x = -90;
	if(camR.x > 90) camR.x = 90;
}


// Change the camera's orientation to a certain amount in the x, y, and z directions
void Game::rotateCameraTo(float xRot, float yRot, float zRot){
	camR = Vector3f(xRot, yRot, zRot);
	
	while(camR.x < 0) camR.x += 360;
	while(camR.y < 0) camR.y += 360;
	while(camR.z < 0) camR.z += 360;
	
	// Constrain the camera's y value to the range [-180, 180} to help with transitions
	camR.y = fmod(camR.y, 360);
	if(camR.y > 180) camR.y -= 360;
	camR.z = fmod(camR.z, 360);
	if(camR.z > 180) camR.z -= 360;
	
	// Constrain the camera's x rotation
	if(camR.x < -90) camR.x = -90;
	if(camR.x > 90) camR.x = 90;
}


// Adjust the rotation of the camera by a certain ratio
void Game::trackCameraToRotation(Vector3f rot, float ratio){
	// Clamp the ratio
	ratio = min(max(ratio, 0.0f), 1.0f);
	
	// Constrain the inputs so the transition is smooth
	while(rot.x < 0) rot.x += 360;
	while(rot.y < 0) rot.y += 360;
	while(rot.z < 0) rot.z += 360;
	rot.x = fmod(rot.x, 360);
	rot.y = fmod(rot.y, 360);
	rot.z = fmod(rot.z, 360);
	if(abs(camR.x - rot.x) > abs(camR.x - (rot.x - 360))) rot.x -= 360;
	if(abs(camR.y - rot.y) > abs(camR.y - (rot.y - 360))) rot.y -= 360;
	if(abs(camR.z - rot.z) > abs(camR.z - (rot.z - 360))) rot.z -= 360;
	
	Vector3f camRot = (rot - camR) * ratio + camR;
	rotateCameraTo(camRot.x, camRot.y, camRot.z);
}


// Return the rotation values of the camera (represented misleadingly by a Vector3f)
Vector3f Game::getCameraRotation(){
	return(camR);
}




// Zoom the camera to a certain amount
void Game::setCameraZoom(float zoom){
	camZoom = zoom;
	// Restrict the zoom from being negative
	if(camZoom < 0) camZoom = 0;
}


// Zoom the camera relative to a certain amount
void Game::zoomCamera(float zoomAmount){
	camZoom += zoomAmount;
	// Restrict the zoom from being negative
	if(camZoom < 0) camZoom = 0;
}


// Adjust the zoom of the camera by a certain ratio (default is 2)
void Game::trackCameraToZoom(float zoom, float ratio){
	// Clamp the ratio
	ratio = min(max(ratio, 0.0f), 1.0f);
	camZoom = (zoom - camZoom) * ratio + camZoom;
	// Restrict the zoom from being negative
	if(camZoom < 0) camZoom = 0;
}


// Return the level of zoom the camera's at
float Game::getCameraZoom(){
	return(camZoom);
}


// Set the camera to a top-down perspective on the level (with optional smooth transition)
void Game::setCameraOverhead(bool isOverhead, bool smoothTransition){
	overheadCam = isOverhead;
	smoothOverheadMove = smoothTransition;
}


// Get whether the camera is overhead
bool Game::getCameraOverhead(){
	return(overheadCam);
}
	



// Set the light's position
void Game::setLightPosition(Vector3f pos){
	float* lp = gameObject->lightPosition;
	lp[0] = pos.x;
	lp[1] = pos.y;
	lp[2] = pos.z;
}

// Move the light in a direction
void Game::moveLight(Vector3f dir){
	float* lp = gameObject->lightPosition;
	lp[0] += dir.x;
	lp[1] += dir.y;
	lp[2] += dir.z;
}

void Game::moveLightHorizontallyRelative(float localX, float localZ){
	float* lp = gameObject->lightPosition;

	// Move the light with respect to the camera's local Z axis
	lp[0] += localZ*cos((camR.y)*PI/180);
	lp[2] += localZ*sin((camR.y)*PI/180);
	
	// Move the light with respect to the camera's local X axis
	lp[0] += localX*cos((camR.y - 90)*PI/180);
	lp[2] += localX*sin((camR.y - 90)*PI/180);
}

// Return the light's position
Vector3f Game::getLightPosition(){
	float* lp = gameObject->lightPosition;
	return(Vector3f(lp[0], lp[1], lp[2]));
}





// Set the font for a UILayer text
void Game::setFont(int sceneID, void* font, int size, Vector3f color) {
    states[sceneID]->UI->setFont(font, size, color);
}

// Add a Button element with no UI variance
int Game::addElement(int sceneID, Button* element, float screenPosX, float screenPosY, float scale, SPS_ALIGNMENT alignment) {
    return(states[sceneID]->UI->addElement(element, screenPosX, screenPosY, scale, alignment));
}

// Add a Button element with UI variance
int Game::addElement(int sceneID, Button* element, float x, float y, float scale, SPS_ALIGNMENT alignment, float* refU, float* refV, float maxU, float maxV, SPS_ALIGNMENT alignmentUV) {
    return(states[sceneID]->UI->addElement(element, x, y, scale, alignment, refU, refV, maxU, maxV, alignmentUV));
}

// Add an Image element with no UI variance
int Game::addElement(int sceneID, Image* element, float screenPosX, float screenPosY, float scale, SPS_ALIGNMENT alignment) {
    return(states[sceneID]->UI->addElement(element, screenPosX, screenPosY, scale, alignment));
}

// Add an Image element with no UI variance
int Game::addElement(int sceneID, Image* element, float x, float y, float scale, SPS_ALIGNMENT alignment, float* refU, float* refV, float maxU, float maxV, SPS_ALIGNMENT alignmentUV) {
    return(states[sceneID]->UI->addElement(element, x, y, scale, alignment, refU, refV, maxU, maxV, alignmentUV));
}

// Add a text element
int Game::addElement(int sceneID, char* text1, char* text2, float screenPosX, float screenPosY, SPS_ALIGNMENT alignment) {
    return(states[sceneID]->UI->addElement(text1, text2, screenPosX, screenPosY, alignment));
}

// Add a text element with an int variable
int Game::addElement(int sceneID, char* text, int* var, float screenPosX, float screenPosY, SPS_ALIGNMENT alignment) {
    return(states[sceneID]->UI->addElement(text, var, screenPosX, screenPosY, alignment));
}



void Game::hideElement(int element){
	FSM->currentState->UI->hideElement(element);
}

void Game::showElement(int element){
	FSM->currentState->UI->showElement(element);
}

void Game::hideText(int text){
	FSM->currentState->UI->hideText(text);
}

void Game::showText(int text){
	FSM->currentState->UI->showText(text);
}







int Game::getScreenWidth(){
	return(screenWidth);
}

int Game::getScreenHeight(){
	return(screenHeight);
}

// Get the x position of the mouse on the screen
int Game::getMouseX(){
	return(InputManager::getMouseX());
}

// Get the y position of the mouse on the screen
int Game::getMouseY(){
	return(InputManager::getMouseX());
}

// Returns whether a certain mouse button is down
bool Game::isClicked(SPS_MOUSE mouseButton){
	return(FSM->currentState->inputManager->isClicked(mouseButton));
}

// Returns the last key pressed
SPS_KEY Game::getLastKeyPressed(){
	return(FSM->currentState->inputManager->getLastKeyPressed());
}

// Returns whether a certain key is pressed
bool Game::isPressed(SPS_KEY key){
	return(FSM->currentState->inputManager->isPressed(key));
}

