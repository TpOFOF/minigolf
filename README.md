# Spring Plant Sail Minigolf Engine/ Game
This is a minigolf engine designed by team Spring Plant Sail (Daniel Finch and Chase Bradbury).
This project was created in Spring quarter of 2014 at UCSC for CMPS 164 (Game Engines).

Commits made by "Unknown Name" were submitted by Chase Bradbury.

Compilation instructions are available in README.txt