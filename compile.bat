:: Call g++, pass the source files, and link the local libraries
:: source/MiniGolf.cpp source/Parser.cpp source/LevelData.cpp source/LevelObjects.cpp
:: LevelObjects > src/main.cpp > Parser
:: ... MiniGolf.cpp
g++ -D_STDCALL_SUPPORTED -std=c++11 -o ./bin/MiniGolf.exe src/LevelNew.cpp src/LevelData.cpp src/LevelObjects.cpp src/CourseParser.cpp src/Vector3f.cpp src/SphereCollider.cpp src/Util.cpp src/StateMachine.cpp src/Button.cpp src/Image.cpp src/InputManager.cpp src/UILayer.cpp src/Game.cpp src/MiniGolf.cpp -L ./lib -lglut32win -lopengl32 -lglu32

:: Pause to read the compile output
pause
