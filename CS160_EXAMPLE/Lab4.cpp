// Lab3.cpp : Terrain Mapping
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <vector>
#include <assert.h>

#include "ppm_canvas.h"
#include "util.h"
#include "penguin.h"

using namespace std;


#define PI 3.14159265

float xrot, yrot = 0;
bool keyState[256] = { false };
int windowWidth, windowHeight;

canvas_t terrain;
canvas_t skin;
GLuint texture;
bool vertexNormals = false;
bool cached = false;
bool makeCache = true;

float lightPosition[3];
float diffuseColour[3];
float ambientColour[3];

// Control the level of detail of the model
int lod = 4;
int minLod = 7;

// Control the position of the camera and the light
float cX = 0, cY = 0, cZ = 10, cTh = 225, cPh = -30;
int trackP = 0;


// Control keyboard input
bool isQ = false, isW = false, isE = false, isA = false, isS = false, isD = false; //, isN = false, isM = false;
float nearTh = 0;
float farTh = 0;

int btn;

int xMPrev, yMPrev;
//bool camControl = true;
bool spin;
float spinRadius;
float FOV = 90.0;


// gS is how large the terrain should be in the x and y directions
int gS = 20;
// gHM inversely affects how tall the terrain should be
float gHM = 128; // 40


float wI, hI;


float theta = 0;

vector<float> target1;
vector<float> target2;
vector<float> target3;


// Display Lists
/****
*	Upon looking into Vertex Buffer objects and their implementation, I realized that through a combination
*	of using OpenGL version 2.1, and my graphics card's architectural limitations, I could not use VBO objects.
*	As I was attempting to code a VBO, I found that I could not only not generate any type of buffer object
*	(All related functions were not defined), but I also could not bind a buffer (because of the same issue).
*	The only "bind" function available was glBindTexture, which would serve no purpose without a buffer in the first place.
*****/
int DL = -1;
bool isList = false;
bool useList = true;


// Penguins
Penguin *p1, *p2, *p3;

bool moveEverything = true;

int debugColor = 0;
bool debug = false;
float dbx, dby, dbz;
vector<float> dbn;



void doMoves(){
	// Move the camera or light depending on which keys are being held

	if(isW){
			cX += .1*cos((cTh)*PI/180);
			cY += .1*sin((cTh)*PI/180);
	}

	if(isA){
			cX += .1*cos((cTh+90)*PI/180);
			cY += .1*sin((cTh+90)*PI/180);
	}

	if(isS){
			cX += .1*cos((cTh+180)*PI/180);
			cY += .1*sin((cTh+180)*PI/180);
	}

	if(isD){
			cX += .1*cos((cTh-90)*PI/180);
			cY += .1*sin((cTh-90)*PI/180);
	}

	if(isQ){
			cZ -= .1;
	}

	if(isE){
			cZ += .1;
	}


	// Spin the camera
	if(spin){
		lightPosition[0] = spinRadius*cos(theta*PI/180);
		lightPosition[1] = spinRadius*sin(theta*PI/180);
		theta ++;
	}

};




float vDot(vector<float> A, vector<float> B){
	int s = A.size();
	float sum = 0;
	for(int i = 0; i < s; i++){
		sum += A[i]*B[i];
	}
	return(sum);
};



vector<float> vCross(vector<float> A, vector<float> B){
	// Return the cross product of two vectors
	assert(A.size() == B.size());
	vector<float> C;
	
	float iComp = A[1]*B[2] - B[1]*A[2];
	float jComp = A[0]*B[2] - B[0]*A[2];
	float kComp = A[0]*B[1] - B[0]*A[1];

	C.push_back(iComp);
	C.push_back(-(jComp));
	C.push_back(kComp);
	return(C);
};



vector<float> vScale(vector<float> A, float s){
	vector<float> B;
	B.push_back(A[0]*s);
	B.push_back(A[1]*s);
	B.push_back(A[2]*s);
	return(B);
};



vector<float> vAdd(vector<float> A, vector<float> B){
	// Return the sum of two vectors
	assert(A.size() == B.size());
	int s = A.size();
	vector<float> C;
	for(int i = 0; i < s; i++){
		C.push_back(A[i]+B[i]);
	}
	return(C);
};


vector<float> vSubtract(vector<float> A, vector<float> B){
	// Return the difference between two vectors
	assert(A.size() == B.size());
	int s = A.size();
	vector<float> C;
	for(int i = 0; i < s; i++){
		C.push_back(A[i]-B[i]);
	}
	return(C);
};


float vMagnitude(vector<float> A){
	int s = A.size();
	float sqrTotal = 0.0;
	for(int i = 0; i < s; i++){
		sqrTotal += pow(A[i],2);
	}
	return(sqrtf(sqrTotal));
}



vector<float> vNormalize(vector<float> A){
	// Return the normalized version of a vector
	vector<float> B;
	int s = A.size();
	float mag = vMagnitude(A);
	for(int i = 0; i < s; i++){
		B.push_back(A[i]/mag);
	}
	return(B);
};



float triArea(vector<float> A, vector<float> B){
	// Return the area of a triangle defined by two vectors
	vector<float> C = vCross(A,B);
	return(.5*vMagnitude(C));
};



void showVector(float x1,float y1, float z1, float i1, float j1, float k1){
	glDisable(GL_LIGHTING);
	// Debug to display normals
	if(debugColor == 0)
		glColor3f(1,0,0);
	if(debugColor == 1)
		glColor3f(0,1,0);
	if(debugColor == 2)
		glColor3f(0,0,1);
	if(++debugColor > 2) debugColor -= 3;

	glLineWidth(2);
	glBegin(GL_LINES);
	glVertex3f(x1, y1, z1);
	glVertex3f(x1 + i1, y1 + j1, z1 + k1);
	glEnd();
	glEnable(GL_LIGHTING);
};



/*
vector<int> getIndices(float x, float y){
	int wIncr, hIncr;
	wIncr = hIncr = (int)(pow(2.0f,(float)(minLod-lod)));

	//printf("(x,y):  (%f, %f)\n", x, y);

	// for lower left vertex:   i*wI - gS/2,      j*hI - gS/2,      hAA
	x += gS/2;
	y += gS/2;
	bool upper = false;

	int i = (int)floor(x/wI) / wIncr;
	int j = (int)floor(y/hI) / hIncr;

	vector<int> ind;
	ind.push_back(i);
	ind.push_back(j);
	return(ind);
};
*/


vector<float> getNormal(float x, float y){


	int wIncr, hIncr;
	wIncr = hIncr = (int)(pow(2.0f,(float)(minLod-lod)));

	x += gS/2;
	y += gS/2;
	bool upper = false;

	int i = (int)floor(x/wI); // / wIncr;
	int j = (int)floor(y/hI); // / hIncr;

	float xC = x - i*wI;
	float yC = y - j*hI;

	// Determine whether the point lies in the upper left or lower right triangle of the grid square
	if( yC > xC ) // y-y0 > m(x-x0)   (offset accounted for)
		upper = true;

	float hAA = RED(PIXEL(terrain, i, j))/gHM;
	float hBA = RED(PIXEL(terrain, i+wIncr, j))/gHM;
	float hAB = RED(PIXEL(terrain, i, j+hIncr))/gHM;
	float hBB = RED(PIXEL(terrain, i+wIncr, j+hIncr))/gHM;

	vector<float> A; A.push_back(wI*wIncr); A.push_back(0.0f); A.push_back(hBB - hAB);
	vector<float> B; B.push_back(0.0f); B.push_back(hI*hIncr); B.push_back(hAB - hAA);
	vector<float> C; C.push_back(0.0f); C.push_back(-hI*hIncr); C.push_back(hBA-hBB);
	vector<float> D; D.push_back(-wI*wIncr); D.push_back(0.0f); D.push_back(hAA-hBA);


	// Return the normal vectors through the cross products
	if(upper)
		return(vNormalize(vCross(A,B)));
	else
		return(vNormalize(vCross(D,C)));
};




float findHeight(float x, float y){
	int wIncr, hIncr;
	wIncr = hIncr = (int)(pow(2.0f,(float)(minLod-lod)));

	//printf("(x,y):  (%f, %f)\n", x, y);

	// for lower left vertex:   i*wI - gS/2,      j*hI - gS/2,      hAA
	x += gS/2;
	y += gS/2;
	bool upper = false;

	int i = (int)floor(x/wI); // / wIncr;
	int j = (int)floor(y/hI); // / hIncr;

	//printf("(xa,ya):  (%f, %f)\n", x, y);
	
	/*

	// Determine the grid coordinates i and j
	int i = 0;
	int j = 0;
	while( (i+wIncr)*wI < x )
		i += wIncr;
	while( (j+hIncr)*hI < y )
		j += hIncr;
	
	*/
	
	//printf("i = %d, j = %d\n", i, j);

	float xC = x - i*wI;
	float yC = y - j*hI;

//	printf("(xC,yC):  (%.2f, %.2f)\n", xC, yC);

	// Determine whether the point lies in the upper left or lower right triangle of the grid square
	if( yC > xC ) // y-y0 > m(x-x0)   (offset accounted for)
		upper = true;



	vector<float> a1o;
	vector<float> a2o; 
	vector<float> a3o;

	if(upper){
		// Vectors from the point to the top-left triangle's corners
		a1o.push_back(0 - xC); a1o.push_back(0 - yC); a1o.push_back(0);
		a2o.push_back(0 - xC); a2o.push_back(hI*hIncr - yC); a2o.push_back(0);
		a3o.push_back(wI*wIncr - xC); a3o.push_back(hI*hIncr - yC); a3o.push_back(0);
	} else {
		// Vectors from the point to the bottom-right triangle's corners
		a1o.push_back(0 - xC); a1o.push_back(0 - yC); a1o.push_back(0);
		a2o.push_back(wI*wIncr - xC); a2o.push_back(0 - yC); a2o.push_back(0);
		a3o.push_back(wI*wIncr - xC); a3o.push_back(hI*hIncr - yC); a3o.push_back(0);
	}

	float tA12 = triArea(a1o, a2o);
	float tA23 = triArea(a2o, a3o);
	float tA13 = triArea(a1o, a3o);


	float tTotalA = tA12 + tA23 + tA13;



	// Retrieve all the heights of the four corners of this "quad"
	float hAA = RED(PIXEL(terrain, i, j))/gHM;
	float hBA = RED(PIXEL(terrain, i+wIncr, j))/gHM;
	float hAB = RED(PIXEL(terrain, i, j+hIncr))/gHM;
	float hBB = RED(PIXEL(terrain, i+wIncr, j+hIncr))/gHM;


	// Heights
	float hA;
	if(upper)  
		hA = (tA12 * hBB + tA23 * hAA + tA13 * hAB) / tTotalA;
	else
		hA = (tA12 * hBB + tA23 * hAA + tA13 * hBA) / tTotalA;

	//printf("Height: %f\n", hA);

	return(hA);
};



void moveP1(){
	vector<float> p1Pos; p1Pos.push_back(p1->x); p1Pos.push_back(p1->y);

	if(vMagnitude(vSubtract(p1Pos, target1)) < .04){
		//printf("reached target\n");
		// If close enough to reach the target, do so and set a new target
		p1->setPosition(target1[0], target1[1], findHeight(target1[0], target1[1]));
		target1.clear();
		float p1Angle = atan2(p1->y - p1->yPrev, p1->x - p1->xPrev);
		target1.push_back(p1Pos[0] + .25*cos(p1Angle + PI/48));
		target1.push_back(p1Pos[1] + .25*sin(p1Angle + PI/48));
	} else {
		//printf("finding height\n");
		// Move towards the target
		float tAngle = atan2(target1[1]-p1Pos[1], target1[0]-p1Pos[0]);
		p1Pos[0] = p1->x + .01*cos(tAngle);
		p1Pos[1] = p1->y + .01*sin(tAngle);
		p1->setPosition(p1Pos[0], p1Pos[1], findHeight(p1Pos[0],p1Pos[1]) );
	}
};




void moveP2(){
	vector<float> p2Pos; p2Pos.push_back(p2->x); p2Pos.push_back(p2->y);

	if(vMagnitude(vSubtract(p2Pos, target2)) < .03){
		//printf("reached target\n");
		// If close enough to reach the target, do so and set a new target
		p2->setPosition(target2[0], target2[1], findHeight(target2[0], target2[1]));
		target2.clear();
		float p2Angle = atan2(p2->y - p2->yPrev, p2->x - p2->xPrev);
		target2.push_back(p2Pos[0] + .25*cos(p2Angle - PI/36));
		target2.push_back(p2Pos[1] + .25*sin(p2Angle - PI/36));
	} else {
		//printf("finding height\n");
		// Move towards the target
		float tAngle = atan2(target2[1]-p2Pos[1], target2[0]-p2Pos[0]);
		p2Pos[0] = p2->x + .01*cos(tAngle);
		p2Pos[1] = p2->y + .01*sin(tAngle);
		p2->setPosition(p2Pos[0], p2Pos[1], findHeight(p2Pos[0],p2Pos[1]) );
	}
};



void moveP3(){
	vector<float> p3Pos; p3Pos.push_back(p3->x); p3Pos.push_back(p3->y);

	if(vMagnitude(vSubtract(p3Pos, target3)) < .03){
		// If close enough to reach the target, do so and set a new target
		p3->setPosition(target3[0], target3[1], findHeight(target3[0], target3[1]));
		target3.clear();
		target3.push_back(rand() % (int)(gS * .8) - (gS*.8)/2);
		target3.push_back(rand() % (int)(gS * .8) - (gS*.8)/2);
	} else {
		//printf("finding height\n");
		// Move towards the target
		float tAngle = atan2(target3[1]-p3Pos[1], target3[0]-p3Pos[0]);
		p3Pos[0] = p3->x + .01*cos(tAngle);
		p3Pos[1] = p3->y + .01*sin(tAngle);
		p3->setPosition(p3Pos[0], p3Pos[1], findHeight(p3Pos[0],p3Pos[1]) );
	}
};



void rightPenguin(Penguin* p){
	vector<float> norm = vNormalize(getNormal(p->x, p->y));
	vector<float> up; up.push_back(0); up.push_back(0); up.push_back(1);
	vector<float> crs = vNormalize(vCross(up, norm));
	float pAngle = acosf(vDot(up, norm));
	
	vector<float> crsPrev; crsPrev.push_back(p->rX); crsPrev.push_back(p->rY); crsPrev.push_back(p->rZ);
	
	// If the cross vector is not too different from the previous one (avoid twitching)
	if(abs(vDot(crs, crsPrev)) < .5){
		p->rX = crs[0];
		p->rY = crs[1];
		p->rZ = crs[2];
	}

	p->angle = pAngle * 180/PI;
};




void trackCamera(){

	// Rotate the camer around the current penguin
	Penguin* currTrack = NULL;
	if(trackP == 1) currTrack = p1;
	if(trackP == 2) currTrack = p2;
	if(trackP == 3) currTrack = p3;

	float cosT = cos((cTh)*PI/180);
	float sinT = sin((cTh)*PI/180);
	float cosPh = cos((cPh)*PI/180);
	float sinPh = sin((cPh)*PI/180);
	
	if(trackP != 0){
		// cX + 10*cosT*cosPh, cY + 10*sinT*cosPh, cZ + 10*sinPh
		cX = currTrack->x - 2*cosT*cosPh;
		cY = currTrack->y - 2*sinT*cosPh;
		cZ = currTrack->z - 2*sinPh;
	}
}




void movePenguins(){
	
	//	vector<float> p2Pos; p2Pos.push_back(p2->x); p2Pos.push_back(p2->y);
	//	vector<float> p3Pos; p3Pos.push_back(p3->x); p3Pos.push_back(p3->y);

	// Move each individual penguin across the terrain
	moveP1();
	moveP2();
	moveP3();

	// Animate the penguins
	p1->moveAll();
	p2->moveAll();
	p3->moveAll();

	rightPenguin(p1);
	rightPenguin(p2);
	rightPenguin(p3);
}




void drawTerrain(){
	// Draw objects
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);

	int wIncr, hIncr;
	wIncr = hIncr = (int)(pow(2.0f,(float)(minLod-lod)));

	float tw = terrain.width;
	float th = terrain.height;

	for(int j = 0; j < terrain.height - hIncr; j += hIncr){
		for(int i = 0; i < terrain.width - wIncr; i += wIncr){
			// Calculate the cross product, use it for glNormal calls

			// Retrieve all the heights of the four corners of this "quad"
			float hAA = RED(PIXEL(terrain, i, j))/gHM;
			float hBA = RED(PIXEL(terrain, i+wIncr, j))/gHM;
			float hAB = RED(PIXEL(terrain, i, j+hIncr))/gHM;
			float hBB = RED(PIXEL(terrain, i+wIncr, j+hIncr))/gHM;

			vector<float> A; A.push_back(wI*wIncr); A.push_back(0.0f); A.push_back(hBB - hAB);
			vector<float> B; B.push_back(0.0f); B.push_back(hI*hIncr); B.push_back(hAB - hAA);
			vector<float> C; C.push_back(0.0f); C.push_back(-hI*hIncr); C.push_back(hBA-hBB);
			vector<float> D; D.push_back(-wI*wIncr); D.push_back(0.0f); D.push_back(hAA-hBA);


			// Ensure the vectors are the appropriate size
			assert(A.size() == B.size() && B.size() == C.size() && C.size() == D.size() && D.size() == 3);

			// Find the normal vectors through the cross products
			vector<float> NA = vNormalize(vCross(A,B));
			vector<float> NB = vNormalize(vCross(D,C));


			glBegin(GL_TRIANGLES);


			glNormal3f(NA[0],NA[1],NA[2]);
			glTexCoord2f( i/tw, j/th);	
			glVertex3f( i*wI - gS/2, j*hI - gS/2, hAA );
		
			glNormal3f(NA[0],NA[1],NA[2]);
			glTexCoord2f( i/tw, (j+hIncr)/th);
			glVertex3f( i*wI - gS/2, (j+hIncr)*hI - gS/2, hAB );
		
			glNormal3f(NA[0],NA[1],NA[2]);
			glTexCoord2f( (i+wIncr)/tw, (j+hIncr)/th);
			glVertex3f( (i+wIncr)*wI - gS/2, (j+hIncr)*hI - gS/2, hBB );
		


			glNormal3f(NB[0],NB[1],NB[2]);
			glTexCoord2f( (i+wIncr)/tw, (j+hIncr)/th );
			glVertex3f( (i+wIncr)*wI - gS/2, (j+hIncr)*hI - gS/2, hBB );

			glNormal3f(NB[0],NB[1],NB[2]);
			glTexCoord2f( (i+wIncr)/tw, j/th );
			glVertex3f( (i+wIncr)*wI - gS/2, j*hI - gS/2, hBA );

			glNormal3f(NB[0],NB[1],NB[2]);
			glTexCoord2f( i/tw, j/th);
			glVertex3f( i*wI - gS/2, j*hI - gS/2, hAA );

			glEnd();


			// If debug is enabled, show the normals of the faces
			if(debug){
				if(i < terrain.width - 6*wIncr && j < terrain.height - 6*hIncr){
					float hA = findHeight(.3 + i*wI - gS/2, .7 + j*hI - gS/2);
					float hB = findHeight(.7 + i*wI - gS/2, .3 + j*hI - gS/2);
			
					showVector(i*wI-gS/2 + .3,  j*hI - gS/2 + .7,  hA,  NA[0], NA[1], NA[2] );
					showVector(i*wI-gS/2 + .7,  j*hI - gS/2 + .3,  hB,  NB[0], NB[1], NB[2] );
				}
			}

		}
	}


	glDisable(GL_TEXTURE_2D);
}



void drawLight(){
	// Draw a cube to represent the position of the light
	float lx = lightPosition[0];
	float ly = lightPosition[1];
	float lz = lightPosition[2];

	glDisable(GL_LIGHTING);

	glColor3f(1,1,1);
	glBegin(GL_QUADS);

	glNormal3f(0,0,lz+1);
	glVertex3f(lx-.5,-.5+ly,-.5+lz);
	glVertex3f(lx-.5,.5+ly,-.5+lz);
	glVertex3f(lx+.5,.5+ly,-.5+lz);
	glVertex3f(lx+.5,-.5+ly,-.5+lz);

	glNormal3f(0,0,lz-1);
	glVertex3f(lx-.5,-.5+ly,.5+lz);
	glVertex3f(lx-.5,.5+ly,.5+lz);
	glVertex3f(lx+.5,.5+ly,.5+lz);
	glVertex3f(lx+.5,-.5+ly,.5+lz);

	glNormal3f(1,0,0);
	glVertex3f(lx-.5,-.5+ly,-.5+lz);
	glVertex3f(lx-.5,.5+ly,-.5+lz);
	glVertex3f(lx-.5,.5+ly,.5+lz);
	glVertex3f(lx-.5,-.5+ly,.5+lz);

	glNormal3f(-1,0,0);
	glVertex3f(lx+.5,-.5+ly,-.5+lz);
	glVertex3f(lx+.5,.5+ly,-.5+lz);
	glVertex3f(lx+.5,.5+ly,.5+lz);
	glVertex3f(lx+.5,-.5+ly,.5+lz);

	glNormal3f(0,1,0);
	glVertex3f(lx-.5,-.5+ly,-.5+lz);
	glVertex3f(lx+.5,-.5+ly,-.5+lz);
	glVertex3f(lx+.5,-.5+ly,.5+lz);
	glVertex3f(lx-.5,-.5+ly,.5+lz);

	glNormal3f(0,-1,0);
	glVertex3f(lx-.5,.5+ly,-.5+lz);
	glVertex3f(lx+.5,.5+ly,-.5+lz);
	glVertex3f(lx+.5,.5+ly,.5+lz);
	glVertex3f(lx-.5,.5+ly,.5+lz);

	glEnd();
};



void drawTexture() {
	// Depending on the list setting, use, make, or ignore the display list
	if(useList){
		if( !isList){
			DL = glGenLists(1);
			glNewList(DL, GL_COMPILE);

			// Call the function to add gl calls to the display list
			drawTerrain();

			// End the list
			glEndList();
			isList = true;
		}
		
		// Draw the display list
		glCallList(DL);
	} else
		// If we should not use the display list, just draw the terrain normally
		drawTerrain();

	// Draw the light object
	drawLight();
	
};




void cb_display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_LIGHT0);

	// Every display call, move as necessary
	doMoves();

	glLoadIdentity();

	// Spin the camera to the correct angle
	float cosT = cos((cTh)*PI/180);
	float sinT = sin((cTh)*PI/180);
	float cosPh = cos((cPh)*PI/180);
	float sinPh = sin((cPh)*PI/180);

	gluLookAt(cX, cY, cZ,   cX + 10*cosT*cosPh, cY + 10*sinT*cosPh, cZ + 10*sinPh,    0, 0, 1);

	glEnable(GL_LIGHTING);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	drawTexture();
	glDisable(GL_LIGHTING);


	// Draw the axis
	glPushMatrix();
	glTranslatef(-gS/2, -gS/2, 3);
	draw_axis(4.0);
	glPopMatrix();


	// Move and draw all penguins
	if(moveEverything)
		movePenguins();

	trackCamera();

	p1->drawPenguin();
	p2->drawPenguin();
	p3->drawPenguin();

	glFlush();
	glutSwapBuffers(); // for smoother animation
};




void loadTextureMap(canvas_t tex) {
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tex.width, tex.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex.pixels);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
};




void cb_idle() {
	// Also draw the texture and post another display call to the queue
	drawTexture();
	glutPostRedisplay();
};



void cb_reshape(int w, int h) {
	windowWidth = w;
	windowHeight = h;
	int aspect = w/h;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90.0, aspect, 0.1, 200); // necessary to preserve aspect ratio
	glMatrixMode(GL_MODELVIEW);
};




void cb_mouseclick(int button, int state, int x, int y) {
	xMPrev = x;
	yMPrev = y;
	btn = button;
};




void cb_mousemove(int x, int y) {
	if(btn == 0){ //&& camControl){
		cTh -= (x-xMPrev)/3;
		if(cTh >= 360) cTh -= 360;
		if(cTh < 0) cTh += 360;

		// Constrain the vertical angle to 89 degrees either way to keep the up vector well-defined
		cPh -= (y-yMPrev)/3;
		if(cPh < -89) cPh = -89;
		if(cPh > 89) cPh = 89;
	}


	xMPrev = x;
	yMPrev = y;
};




void showHelp(){
	printf("\n___________ CONTROLS ____________\n");
	printf("To move the camera, use WASD and drag the left mouse button.\n");
	printf("To control the light, use WASD\n");
	printf("To raise or lower the camera, use E or Q\n");
	printf("To lower the level of detail of the terrain, press Z\n");
	printf("To raise the level of detail, press X\n");
	printf("To toggle spin on the light, press J\n");
	printf("To switch the camera between penguins, press C\n");
	printf("To freeze all motion except the light, press F\n");
	printf("To display help, press H\n");
	printf("To exit, press P\n");
};




void cb_keyboard(unsigned char key, int x, int y) {

	// Activate the keys as they are pressed
	switch(key) {
		case 'w': isW = true; break;
		case 'a': isA = true; break;
		case 's': isS = true; break;
		case 'd': isD = true; break;
		case 'q': isQ = true; break;
		case 'e': isE = true; break;
		case 'f': moveEverything = !moveEverything; break;
		case 'z': if(lod > 0){ lod --; isList = false;} break;
		case 'x': if(lod < minLod) {lod++; isList = false;} break;
		case 'c': trackP ++; if(trackP > 3) trackP -= 4; printf("%d\n", trackP); break;
		case 'j': spin = !spin;
					spinRadius = sqrt(pow(lightPosition[0],2) + pow(lightPosition[2],2));
					theta = 180*atan2(lightPosition[1],lightPosition[0])/PI;
					break;
		case 'h': showHelp(); break;
		case 'p': exit(0); break;
	}
};




void cb_keyboardUp(unsigned char key, int x, int y) {

	// Deactivate the keys as they are released
	switch(key) {
		case 'w': isW = false; break;
		case 'a': isA = false; break;
		case 's': isS = false; break;
		case 'd': isD = false; break;
		case 'q': isQ = false; break;
		case 'e': isE = false; break;
//		case 'n': isN = false; break;
//		case 'm': isM = false; break;
	}
};




int main(int argc, char** argv) {

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(1000, 1000);
	glutCreateWindow("Lab 3: Terrain Mapping");
	glutIgnoreKeyRepeat(true);

	glEnable(GL_TEXTURE_2D);
	if (argc == 3) {
		printf("Loading file '%s'... ", argv[1]);
		ppmLoadCanvas(argv[1], &terrain);
		printf("Done.\n");
		printf("Loading file '%s'... ", argv[2]);
		ppmLoadCanvas(argv[2], &skin);
		printf("Done.\n");
		loadTextureMap(skin);
		
		wI = gS/(float)(terrain.width);
		hI = gS/(float)(terrain.height);

	} else {
		printf("Usage: %s terrain.ppm texture.ppm\n", argv[0]);
		return 1;
	}


	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glEnable (GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glutDisplayFunc(cb_display);
	glutReshapeFunc(cb_reshape);
	glutIdleFunc(cb_idle);

	glutKeyboardFunc(cb_keyboard);
	glutKeyboardUpFunc(cb_keyboardUp);
	glutMouseFunc(cb_mouseclick);
	glutMotionFunc(cb_mousemove);

	glClearColor(.2,.3,.8,0); // set background color

	lightPosition[0] = -24;
	lightPosition[1] = 4;
	lightPosition[2] = 20;

	printf("\n----- Texture rendering -----\n\n\n");
	showHelp();

	p1 = new Penguin(1,0,.5);
	p2 = new Penguin(2,-4.5,.5);
	p3 = new Penguin(-8,7,.5);

	target1.push_back(2); target1.push_back(2);
	target2.push_back(3); target2.push_back(-4);
	target3.push_back(-7); target3.push_back(7);

	////
	dbx = dby = dbz = 0;
	////

	glutMainLoop();

	return 0;
};
