//#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include "util.h"
#include "drawing.h"
#include "penguin.h"

const double PI = 3.14159265;



Penguin::Penguin(double X, double Y, double Z){
	x = X;
	y = Y;
	z = Z;
	xPrev = X;
	yPrev = Y;
	rX = 1;
	rY = 0;
	rZ = 0;
	angle = 0;
	tBody = tBeak = tArmsA = tArmsB = tArmsC= tTail = tFeetA = 0;
	// Makes the walking animation look better
	tFeetB = 120;
};

Penguin::~Penguin(){
	fprintf(stderr, "penguin destroyed");
};

	

void Penguin::movePart(int p){
	// Increase the animation value for the requested body part
	switch(p){
		case 0: tBody += 6; constrain(&tBody); break;
		case 1: tBeak += 10; constrain(&tBeak); break;
		case 2: tArmsA += 6; constrain(&tArmsA); break;
		case 3: tArmsB += 2; constrain(&tArmsB); break;
		case 4: tArmsC += 2; constrain(&tArmsC); break;
		case 5: tFeetA += 6; constrain(&tFeetA); break;
		case 6: tFeetB += 6; constrain(&tFeetB); break;
		case 7: tTail += 2; constrain(&tTail); break;
		
		default: fprintf(stderr, "ERROR: movePart(int p): Invalid part id\n");
			exit(1); break;
	}
};



void Penguin::moveAll(){
	for(int i = 0; i < 8; i++)
		movePart(i);
};


/*
//private:
double x, z;
bool visible;
// Theta measures for various body components
double tBody, tBeak, tArmsA, tArmsB, tArmsC, tTail, tFeetA, tFeetB;
*/


void Penguin::constrain(double* in){
	// A method to constrain angles to 360 degrees
	if(*in > 359) *in -= 360;
};


void Penguin::setVisible(bool v){
	visible = v;
};


bool Penguin::getVisible(){
	return(visible);
};


void Penguin::clearMoves(){
	tBody = tBeak = tArmsA = tArmsB = tArmsC = tFeetA = tTail = 0;
	// For a smoother walking animation
	tFeetB = 120;
};


void Penguin::setPosition(double X, double Y, double Z){
	// Set the previous positions
	xPrev = x;
	yPrev = y;

	x = X;
	y = Y;
	z = Z;
};


void Penguin::drawPenguin(){
	// Call methods to draw the various components of the penguin from the reference frame of the body
	glPushMatrix();

	glTranslatef(x,y,z);
	glTranslatef(0,0,.31);
	glScalef(.1,.1,.1);
	
	

	if(xPrev != x && yPrev != y)
		glRotatef(180*atan2(y-yPrev, x-xPrev)/PI,0,0,1);

	glRotatef(angle, rX, rY, rZ);
	glRotatef(90,1,0,0);
	
	// Rotate and move the entire body by an amount (for running animation)
	glRotatef(15*sin(PI*tBody/180), 0, 1, 0); // Rotate about the spine
	glRotatef(15*sin(PI*tBody/180), 1, 0, 0); // Bob left and right
	glTranslatef(0,0.3*cos(PI*tBody/90), 0.3*sin(PI*(tBody-30)/180)); // Slide left and right, Bob up and down


	glScalef(0.5,0.5,0.5);

	drawBody();
	
	glPushMatrix();
	glTranslatef(8,0,0);
	glRotatef(-5*sin(PI*tBody/180), 0, 1, 0);
	glTranslatef(-8,0,0);
	glRotatef(-5*sin(PI*tBody/180), 1, 0, 0);
	drawHead();
	glPopMatrix();
		
	glPushMatrix();
	drawArmL();
	glPopMatrix();

	glPushMatrix();
	drawArmR();
	glPopMatrix();

	glPushMatrix();
	drawFootL();
	glPopMatrix();

	glPushMatrix();
	drawFootR();
	glPopMatrix();

	glPushMatrix();
	drawTail();
	glPopMatrix();

	glPopMatrix();
};


//drawBox(float l, float w, float h, float wStr, float hStr, float lSk, float wSk, float hSk, int ends)

void Penguin::drawBody(){
	// Draw the main part of the body
	glColor3f(.15,.15,.15);
	glRotatef(90, 0, 0, 1);
	glTranslatef(2.5,0,0);
	drawBox(1,3.8,3.8,.7,.7,0,0,0,2);
	glTranslatef(-1,0,0);
	drawBox(1,4.4,4.4,.863636,.863636,0,0,0,0);
	glTranslatef(-1.5,0,0);
	drawBox(1.5,4.8,4.8,.916667,.916667,0,0,0,0);
	glTranslatef(-2.5,0,0);
	drawBox(2.5,4.55,4.55,1.05495,1.05495,0,0,0,0);
	glTranslatef(-1,0,0);
	drawBox(1,4,4,1.1375,1.1375,0,0,0,0);
	glTranslatef(-.5,0,0);
	drawBox(.5,3.3,3.3,1.21212,1.21212,0,0,0,0);
	glTranslatef(-.25,0,0);
	drawBox(.255,2.31,2.31,1.45857,1.42857,0,0,0,1);

	// Draw the light highlight on the chest
	glPushMatrix();
	glColor3f(.6,.6,.6);
	glScalef(.7,.6,.7);
	glTranslatef(8,-2,0);
	drawBox(1,3.8,3.8,.7,.7,0,0,0,2);
	glTranslatef(-1,0,0);
	drawBox(1,4.4,4.4,.863636,.863636,0,0,0,0);
	glTranslatef(-1.5,0,0);
	drawBox(1.5,4.8,4.8,.916667,.916667,0,0,0,0);
	glTranslatef(-2.5,0,0);
	drawBox(2.5,4.55,4.55,1.05495,1.05495,0,0,0,0);
	glTranslatef(-1,0,0);
	drawBox(1,4,4,1.1375,1.1375,0,0,0,0);
	glTranslatef(-.5,0,0);
	drawBox(.5,3.3,3.3,1.21212,1.21212,0,0,0,1);
	glPopMatrix();
};

void Penguin::drawHead(){
	// Draw the main portion of the head
	glColor3f(.18,.18,.18);
	glTranslatef(11,0,0);
	glScalef(.6,.6,.6);
	drawBox(.5,2.66,2.66,.6,.6,0,0,0,2);
	glTranslatef(-1,0,0);
	drawBox(1,3.8,3.8,.7,.7,0,0,0,0);
	glTranslatef(-1,0,0);
	drawBox(1,4.4,4.4,.863636,.863636,0,0,0,0);
	glTranslatef(-1.5,0,0);
	drawBox(1.5,4.8,4.8,.916667,.916667,0,0,0,0);
	glTranslatef(-2.5,0,0);
	drawBox(2.5,4.55,4.55,1.05495,1.05495,0,0,0,0);
	glTranslatef(-1,0,0);
	drawBox(1,4,4,1.1375,1.1375,0,0,0,0);
	glTranslatef(-.5,0,0);
	drawBox(.5,3.3,3.3,1.21212,1.21212,0,0,0,1);
	glPushMatrix();


	// Draw the eyes
	glPushMatrix();

	// Eyeballs
	glColor3f(0,0,0);
	glTranslatef(1,1,3);
	glPushMatrix();

	glTranslatef(0,4,-1);

	glPushMatrix();
	glTranslatef(4,-6.8,-3);
	glRotatef(45,0,1,0);
	glRotatef(-90,0,0,1);
	drawBox(.8,1.3,1.3,.6,.6,0,0,0,2);

	glPopMatrix();

	glPushMatrix();
	glTranslatef(4,-6.8,-1);
	glRotatef(45,0,1,0);
	glRotatef(-90,0,0,1);
	drawBox(.8,1.3,1.3,.6,.6,0,0,0,2);

	glPopMatrix();

	glPopMatrix();

	// Highlights
	glColor3f(1,1,1);
	glTranslatef(4.2,-3.55,-2.2);

	glPushMatrix();
	glRotatef(-90,0,0,1);
	drawBox(.15,.25,.25,.6,.6,0,0,0,2);
	glPopMatrix();

	glTranslatef(0,0,-2);
	glPushMatrix();
	glRotatef(-90,0,0,1);
	drawBox(.15,.25,.25,.6,.6,0,0,0,2);
	glPopMatrix();

	glTranslatef(-.4,0,2.3);

	glPushMatrix();
	glRotatef(-90,0,0,1);
	drawBox(.1,.15,.15,.6,.6,0,0,0,2);
	glPopMatrix();

	glTranslatef(0,0,-2);
	glPushMatrix();
	glRotatef(-90,0,0,1);
	drawBox(.1,.15,.15,.6,.6,0,0,0,2);
	glPopMatrix();

	glPopMatrix();


	// Draw the beak
	glPopMatrix();
	glTranslatef(3.3,-2.4,0);
	glRotatef(-90,0,0,1);
	glColor3f(.8,.8,0);

	glPushMatrix();
	glRotatef(-10+10*sin(tBeak*PI/180),0,0,1);

	// Lower left
	glPushMatrix();
	glTranslatef(0,0,.4);
	glRotatef(-30,1,0,0);
	drawBox(2,1,.3,0,0,0,-.4,.15,2);
	glPopMatrix();

	glColor3f(.8,.8,.4);

	// Lower right
	glPushMatrix();
	glTranslatef(0,0,-.4);
	glRotatef(30,1,0,0);
	drawBox(2,1,.3,0,0,0,.4,.15,2);
	glPopMatrix();

	glPopMatrix();
	glPushMatrix();

	glRotatef(10-10*sin(tBeak*PI/180),0,0,1);

	// Upper left
	glPushMatrix();
	glTranslatef(0,.4,.4);
	glRotatef(30,1,0,0);
	drawBox(2,1,.3,0,0,0,-.4,-.15,2);
	glPopMatrix();

	glColor3f(.8,.8,.6);

	// Upper right
	glPushMatrix();
	glTranslatef(0,.4,-.4);
	glRotatef(-30,1,0,0);
	drawBox(2,1,.3,0,0,0,.4,-.15,2);
	glPopMatrix();

	glPopMatrix();

	/*
	//////////
	// Hat
	glPopMatrix();
	glColor3f(.75,.75,.55);

	//glTranslatef(0,8.4,0);
		
	glTranslatef(7.3,0,-.1);
	glRotatef(10,1,0,0);
	glRotatef(10,0,1,0);
	//glRotatef(90,0,0,1);
	//glTranslatef(0,3.7,0);
	drawBox(.6,5,5,.5,.5,0,0,0,1);
	//glTranslatef(.3,0,0);
	drawBox(1,3,3,1,1,0,0,0,2);
	*/
};


void Penguin::drawArmL(){
	glColor3f(.12,.12,.12);
	// Draw the first joint of the arm
	glTranslatef(6.7,0,-1.3);
	glRotatef(90,1,0,0);
	glRotatef(-90,0,0,1);

	// Rotate the "shoulder" based on the orientation of the body
	glRotatef(-30+20*sin((tArmsA-75)*PI/180),0,0,1);

	drawBox(2.6,3,.8,.8,.5,0,.2,0,3);

	//Draw the second joint, maintaining the hierarchy of transformations
	glTranslatef(2.6,0,0+.2);
	glRotatef(20*sin((tArmsB-60)*PI/60),0,0,1);
	drawBox(2,2.4,.4,.6,.4,0,.15,0,3);
	
	//Draw the third joint, still maintaining the hierarchy
	glTranslatef(2,0,0+.15);
	glRotatef(15*sin((tArmsC-45)*PI/60),0,0,1);
	drawBox(1.2,1.44,.1,.3,.4,0,.15,0,3);
};


void Penguin::drawArmR(){
	glColor3f(.12,.12,.12);
	// Draw the first joint of the arm
	glTranslatef(6.7,0,1.3);
	glRotatef(-90,1,0,0);
	glRotatef(-90,0,0,1);

	// Rotate the "shoulder" based on the orientation of the body
	glRotatef(-30+20*sin((tArmsA-75+180)*PI/180),0,0,1);

	drawBox(2.6,3,.8,.8,.5,0,-.2,0,3);

	//Draw the second joint, maintaining the hierarchy of transformations
	glTranslatef(2.6,0,0-.2);
	glRotatef(20*sin((tArmsB-60+180)*PI/60),0,0,1);
	drawBox(2,2.4,.4,.6,.4,0,-.15,0,3);
	
	//Draw the third joint, still maintaining the hierarchy
	glTranslatef(2,0,0-.15);
	glRotatef(15*sin((tArmsC-45+180)*PI/60),0,0,1);
	drawBox(1.2,1.44,.1,.3,.4,0,-.15,0,3);
};


	void Penguin::drawFootL(){
	// Draw the first part of the foot
	glColor3f(.12,.12,.12);
	glTranslatef(.6,0,-.75);
	glRotatef(180,0,0,1);
	glRotatef(20*sin((tFeetA + 60 +180)*PI/180), 0,0,1);
	drawBox(1.5,1.2,2,.666666,.5,0,0,0,3);
		
	// Draw the second part of the foot
	glTranslatef(1.5,0,0);
	glColor3f(.5,.35,0);
	drawBox(.4,.4,.4,.7,.7,0,0,.2,3);

	// Draw the third part of the foot
	glColor3f(.7,.5,0);
	glTranslatef(.3,.2,0);
	glRotatef(180,1,0,0);
	glRotatef(5+20*sin((tFeetB + 60)*PI/180), 0,0,1); // Angle feet to push off ground
	glRotatef(15-15*cos((tFeetB - 30 + 180)*PI/180), -1,0,0); // Angle ankles to not overlap feet

	glPushMatrix();
	glRotatef(25,1,0,0);
	glTranslatef(0,-1,0);
	drawBox(.3,.6,2,1,1,-.2,0,0,3);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0,-1,0);
	drawBox(.3,.6,2,1,1,-.2,0,0,3);
	glPopMatrix();
		
	glPushMatrix();
	glRotatef(-25,1,0,0);
	glTranslatef(0,-1,0);
	drawBox(.3,.6,2,1,1,-.2,0,0,3);
	glPopMatrix();
};


void Penguin::drawFootR(){
	// Draw the first part of the foot
	glColor3f(.12,.12,.12);
	glTranslatef(.6,0,.75);
	glRotatef(180,0,0,1);
	glRotatef(20*sin((tFeetA + 60)*PI/180), 0,0,1);
	drawBox(1.5,1.2,2,.666666,.5,0,0,0,3);
		
	// Draw the second part of the foot
	glTranslatef(1.5,0,0);
	glColor3f(.5,.35,0);
	drawBox(.4,.4,.4,.7,.7,0,0,.2,3);

	// Draw the third part of the foot
	glColor3f(.7,.5,0);
	glTranslatef(.3,.2,0);
	glRotatef(180,1,0,0);
	glRotatef(5+20*sin((tFeetB - 120)*PI/180), 0,0,1); // Angle feet to push off ground
	glRotatef(15-15*cos((tFeetB -30)*PI/180), 1,0,0); // Angle ankles to not overlap feet

	glPushMatrix();
	glRotatef(25,1,0,0);
	glTranslatef(0,-1,0);
	drawBox(.3,.6,2,1,1,-.2,0,0,3);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0,-1,0);
	drawBox(.3,.6,2,1,1,-.2,0,0,3);
	glPopMatrix();
		
	glPushMatrix();
	glRotatef(-25,1,0,0);
	glTranslatef(0,-1,0);
	drawBox(.3,.6,2,1,1,-.2,0,0,3);
	glPopMatrix();
};


void Penguin::drawTail(){
	glColor3f(.15,.15,.15);
	glTranslatef(1,1.7,0);
	glRotatef(180,0,0,1);
	drawBox(1,3,.8,.7,.6,0,.3*sin(PI*tTail/60),-.5,3);
	glTranslatef(1,-.5,.3*sin(PI*tTail/60));
	drawBox(.2,2.1,.48,.3,.4,0,.4*sin(PI*(tTail-30)/60),-.6,3);
};
