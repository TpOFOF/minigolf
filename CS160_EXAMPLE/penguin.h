#ifndef PENGUIN_H
#define PENGUIN_H


class Penguin{
	
public:
	Penguin(double X, double Y, double Z);
	~Penguin();
	void movePart(int p);
	void moveAll();
	void setVisible(bool v);
	bool getVisible();
	void clearMoves();
	void drawPenguin();
	void setPosition(double X, double Y, double Z);
	float x, y, z, xPrev, yPrev, rX, rY, rZ, angle;

private:
	bool visible;
	double tBody, tBeak, tArmsA, tArmsB, tArmsC, tTail, tFeetA, tFeetB;

	void constrain(double* in);
	void drawBody();
	void drawHead();
	void drawArmL();
	void drawArmR();
	void drawFootL();
	void drawFootR();
	void drawTail();
	
};

#endif