:: Set PATH variable to the environment path variable, then append the location of gcc, the GL headers, and the dlls to be linked

set PATH=%PATH%; C:\MinGW\bin; 
::C:\MinGW\include\GL; C:\MinGW\lib\; .\GL; .\lib;



:: Call g++, pass the source files, then 

g++ -D_STDCALL_SUPPORTED -o ./bin/Lab4.exe Lab4.cpp drawing.cpp penguin.cpp ppm_canvas.cpp util.cpp -I .\GL -L .\lib -lMinGW32 -lglut32win -lopengl32 -lglu32




:: Pause to read the compile output

pause
