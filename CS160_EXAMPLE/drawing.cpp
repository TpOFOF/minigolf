#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "util.h"

//#include "GL/glut.h"
#include <GL/glu.h>


void drawBox(float l, float w, float h, float wStr, float hStr, float lSk, float wSk, float hSk, int ends){
	// Arguments in order:
	// length, width, height, width Stretch, height Stretch, length Skew, width Skew, height Skew, end faces

	// This function draws a box with between 4 and 6 quads, allowing for skewing and stretching of the faces
	

//	if(debug)
//		glColor3f(0,0,.9);
	
	glBegin(GL_QUADS);
	glVertex3f(0,h/2,w/2);
	glVertex3f(0,-h/2,w/2);
	glVertex3f(l+lSk, -(h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
	glVertex3f(l, (h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
	
//	if(debug)
//		glColor3f(0,.9,0);

	glVertex3f(l, (h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
	glVertex3f(l, (h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
	glVertex3f(0,h/2,-w/2);
	glVertex3f(0,h/2,w/2);
	
//	if(debug)
//		glColor3f(0,0,.3);

	glVertex3f(0,h/2,-w/2);
	glVertex3f(l, (h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
	glVertex3f(l+lSk, -(h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
	glVertex3f(0,-h/2,-w/2);
	
//	if(debug)
//		glColor3f(0,.3,0);

	glVertex3f(l+lSk, -(h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
	glVertex3f(l+lSk, -(h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
	glVertex3f(0,-h/2,w/2);
	glVertex3f(0,-h/2,-w/2);

	glEnd();

	// If selected, draw the nearest end face
	if(ends == 1 || ends == 3){
//		if(debug)
//			glColor3f(.3,0,0);

		glBegin(GL_QUADS);
		glVertex3f(0,-h/2,w/2);
		glVertex3f(0,h/2,w/2);
		glVertex3f(0,h/2,-w/2);
		glVertex3f(0,-h/2,-w/2);
		glEnd();
	}

	// If selected, draw the farthest end face
	if(ends == 2 || ends == 3){
//		if(debug)
//			glColor3f(.9,0,0);

		glBegin(GL_QUADS);
		glVertex3f(l+lSk,-(h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
		glVertex3f(l+lSk,-(h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
		glVertex3f(l,(h*hStr)/2 + hSk, -(w*wStr)/2 + wSk);
		glVertex3f(l,(h*hStr)/2 + hSk, (w*wStr)/2 + wSk);
		glEnd();
	}

}
