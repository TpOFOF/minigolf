/**
* CMPS 160L (Fall 2011)
* Lab 4
*/


// Util makes sure OpenGL and glut are included only once

#ifndef UTIL_H
#define UTIL_H

#include <GL/glut.h>
#include <GL/glu.h>

void draw_string( float x, float y, float z, char *txt );

void draw_axis(float len);

#endif