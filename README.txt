=== MiniGolf ===

Daniel Finch
(William) Chase Bradbury


___ Changes ___

- Added 3D physics (flying off ramps, bouncing)
- Added state machine for handling game rooms 
	(e.g. main menu, ingame, pause, etc)


___ Files ___
src/include/GL/gl.h
src/include/GL/glext.h
src/include/GL/glu.h
src/include/GL/glut.h

src/include/Button.h
src/include/Game.fwd.h
src/include/Game.h
src/include/glut.fwd.h
src/include/IAction.h
src/include/ICondition.h
src/include/Image.h
src/include/InputManager.h
src/include/Level.fwd.h
src/include/Level.h
src/include/LevelData.h
src/include/LevelObjects.h
src/include/Parser.h
src/include/SphereCollider.h
src/include/StateMachine.h
src/include/UILayer.h
src/include/Util.h
src/include/Vector3f.h

src/Button.cpp
src/Game.cpp
src/Image.cpp
src/InputManager.cpp
src/Level.cpp
src/LevelData.cpp
src/LevelObjects.cpp
src/MiniGolf.cpp
src/Parser.cpp
src/SphereCollider.cpp
src/StateMachine.cpp
src/UILayer.cpp
src/Util.cpp
src/Vector3f.cpp




___ Compiling ___

On Windows:
If you don't have g++ (possibly through MinGW), you will either need to get g++ 
(command-line enabled by setting the environment path variable), or move the
source files into an IDE in the same directory structure.
If you do have g++, simply run compile.bat, then navigate to the bin directory.

On Mac:
You will probably need to move the source files into a project in an IDE, but
the usage will be the same


___ Usage ___

In the same directory as the executable, input the following in command line:

MiniGolf.exe levelFile

where MiniGolf.exe is the executable, and levelFile is the filename 
(including extension) of the level to load


___ Controls ___

WASD - Move the camera around
ZX - Zoom the camera in or out
V  - Hold for top-down camera view
Mouse - Click and drag to rotate the camera around a point in front of the camera
IJKL, UO - Move the light position around in space (represented by the white cube)
C - Hold to charge the club, release to swing in the direction the camera is facing
Q - Close the program

